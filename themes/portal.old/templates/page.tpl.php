<?php


if(preg_match('/home/i', $head_title)){
	$ptl_breadcrumb = '';
}

if(preg_match('/documentos/i', $_SERVER['REQUEST_URI'])){
	$ptl_breadcrumb = 'Documentos';
}

if(preg_match('/relatorios/i', $_SERVER['REQUEST_URI'])){
	$ptl_breadcrumb = 'Relatórios';
}

if(preg_match('/sistemas/i', $_SERVER['REQUEST_URI'])){
	$ptl_breadcrumb = 'Sistemas';
}

if(preg_match('/solicitacoes/i', $_SERVER['REQUEST_URI'])){
	$ptl_breadcrumb = 'Solicitações';
}


?>

<!-- CONTAINER PRINCIPAL  -->
<div class="container-principal">
	<div class="barra-usuario">
		<div class="interno">
			<?php if(user_is_logged_in()){ ?>
			<div class="flutua-dir">
				<a href="<?php echo $base_root;?>/mismobile/user/logout" class="botao25 bt-sair" ><span class="flutua-esq">Sair</span></a>
			</div>
			<div class="flutua-dir nome-usuario">Bem vindo, <?php echo $user->name;?></div>
			<?php } ?>
		</div>
	</div>
	<div class="banner">
		<div class="cabecalho">
			<div class="interno">
				<div class="flutua-esq logo-cabecalho">
					<a href="<?php echo $base_root;?>/mismobile/" title="Página Inicial">
						<img src="<?php echo $base_root;?>/mismobile/themes/portal/images/logo.png" alt="Logo Portal" />
					</a>
				</div>
				<?php if(user_is_logged_in()){?>
				<div class="flutua-dir menu-principal">
					<ul>
						<li class="flutua-dir"><a href="solicitacoes?acao=home" title="Solicitações" class="botao40 bt-solicitacoes" onmouseover="exibe_breadcrumb('Solicitações');" onmouseout="exibe_breadcrumb('<?php echo $ptl_breadcrumb;?>');" ></a></li>
						<li class="flutua-dir"><a href="sistemas?acao=todos" title="Sistemas" class="botao40 bt-sistemas" onmouseover="exibe_breadcrumb('Sistemas');" onmouseout="exibe_breadcrumb('<?php echo $ptl_breadcrumb;?>');" ></a></li>
						<li class="flutua-dir"><a href="documentos?acao=todos" title="Documentos" class="botao40 bt-documentos" onmouseover="exibe_breadcrumb('Documentos');" onmouseout="exibe_breadcrumb('<?php echo $ptl_breadcrumb;?>');" ></a></li>
						<li class="flutua-dir"><a href="relatorios?acao=todos" title="Relatorios" class="botao40 bt-relatorios" onmouseover="exibe_breadcrumb('Relatórios');" onmouseout="exibe_breadcrumb('<?php echo $ptl_breadcrumb;?>');" ></a></li>
						<li class="flutua-dir"><a href="#" title="Busca" class="botao40 bt-busca" onmouseover="exibe_breadcrumb('Busca');" onmouseout="exibe_breadcrumb('<?php echo $ptl_breadcrumb;?>');" ></a></li>
					</ul>
					<div class="clearfix"></div>
				</div>
				<div class="flutua-dir breadcrumb" id="breadcrumb"><?php echo $ptl_breadcrumb;?></div>
				<?php }?>
			</div>
		</div>
		<div class="baseline"></div>
	</div>
	<div class="conteudo">
		<?php print render($page['content']); ?><div class="clearfix"></div>
	</div>
	<div class="rodape">
		<div class="interno">
			<div class="direitos">© <?php echo date('Y')?> Porto Seguro - Todos os direitos reservados.</div>
		</div>
	</div>
</div>
<!-- / CONTAINER PRINCIPAL  -->



<!-- LOGIN CONTAINER -->
<div class="container-login">
	<div class="barra-usuario"></div>
	<div class="conteudo">
		<div class="caixa-login">
			<div class="cabecalho-caixa">
				<a href="<?php echo $base_root;?>/mismobile/user/login"><img src="<?php echo $base_root;?>/mismobile/themes/portal/images/logo.png" /></a>
			</div>
			<div class="conteudo-caixa" >
				<div class="flutua-dir cc-direita">
					<form action="<?php echo $base_root;?>/mismobile/user/login?destination=node/3" method="POST" accept-charset="UTF-8">
						<input type="text" name="name" id="username" placeholder="Matricula" class="text" /><br/>
						<input type="password" name="pass" id="password" placeholder="Senha" class="text" /><br/>
						<input type="submit" value="ENTRAR" class="submit" />
						<input type="hidden" name="form_id" value="user_login" />
					</form>
				</div>
				<div class="flutua-dir cc-esquerda">
					<img src="<?php echo $base_root;?>/mismobile/themes/portal/images/cracha.png" id="cracha" />
					<p id="mensagem"></p>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div><!-- /fim conteudo -->

	<div class="rodape">
		<div class="interno">
			<div class="navegacao">
				<ul >
					<li class="flutua-esq">
						<a href="#" title="Sobre o portal" class="botao40 botao-sobre" onclick="exibe_item_rodape('sobre');" ><span class="flutua-esq">Sobre o<br/>portal</span></a>
					</li>
					<li class="flutua-esq">
						<a href="#" title="Perdeu sua senha" class="botao40 botao-senha" onclick="exibe_item_rodape('perdeu-senha');" ><span class="flutua-esq">Perdeu sua<br/>senha?</span></a>
					</li>
					<li class="flutua-esq">
						<a href="#" title="Fale Conosco" class="botao40 botao-fale" onclick="exibe_item_rodape('fale-conosco');" ><span class="flutua-esq">Fale<br/>Conosco</span></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>

			<div class="conteudo-rodape" id="footer-content"></div>

			<div class="secao-rodape" id="sobre">
				<p>Sobre: Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra lá , depois divoltis porris, paradis. Paisis, filhis, espiritis santis. Mé faiz elementum girarzis, nisi eros vermeio, in elementis mé pra quem é amistosis quis leo. Manduma pindureta quium dia nois paga. Sapien in monti palavris qui num significa nadis i pareci latim. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis.</p>
			</div><!-- /fim sobre -->
			<div class="secao-rodape" id="perdeu-senha">
				<p>Senha: Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra lá , depois divoltis porris, paradis. Paisis, filhis, espiritis santis. Mé faiz elementum girarzis, nisi eros vermeio, in elementis mé pra quem é amistosis quis leo. Manduma pindureta quium dia nois paga. Sapien in monti palavris qui num significa nadis i pareci latim. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis.</p>
			</div><!-- /fim perdeusenha -->
			<div class="secao-rodape" id="fale-conosco">
				<p>Fale: Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra lá , depois divoltis porris, paradis. Paisis, filhis, espiritis santis. Mé faiz elementum girarzis, nisi eros vermeio, in elementis mé pra quem é amistosis quis leo. Manduma pindureta quium dia nois paga. Sapien in monti palavris qui num significa nadis i pareci latim. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis.</p>
			</div><!-- /fim fale conosco -->

			<div class="clearfix"></div>
		</div>
	</div><!-- /fim footer -->


<?php 

if(preg_match('/login/i', $_SERVER['REQUEST_URI'])){
	echo '<script type="text/javascript">document.title = \'Login | Portal de Atendimento\';</script>';
}

if(preg_match('/field is required/', $messages)){
	echo "<script type=\"text/javascript\">erro_login('Os campos não podem estar vazios.','".$base_root."/mismobile/themes/portal/images/dados_incorretos.png');</script>";
}

if(preg_match('/unrecognized username or password/', $messages)){
	echo "<script type=\"text/javascript\">erro_login('Usuário/senha incorretos');</script>";
}

if(preg_match('/Sorry, there have been more than 5 failed login attempts for this account./', $messages)){
	echo "<script type=\"text/javascript\">alert('Foram realizadas mais de 5 tentativas de login para esta conta que foi temporariamente bloqueada. Tente novamente mais tarde.');</script>";
}

//var_dump($messages);
?>
</div>
<!-- LOGIN CONTAINER -->
