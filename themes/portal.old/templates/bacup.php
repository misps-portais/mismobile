<iframe width="640" height="360" src="//www.youtube.com/embed/95JaFaWwZLY?feature=player_embedded" frameborder="0" allowfullscreen></iframe>

http://www.youtube.com/watch?feature=player_embedded&v=95JaFaWwZLY#t=0





<style>.home{ display:none; }</style>
<div id="conteudo" class="home">
	<div class="flutua-esq esquerda">
		<div class="blogs">	
			<ul>
				<li>
					<div class="flutua-esq avatar fabio-luchetti"></div>
					<div class="descricao-avatar">
						<a href="<?php echo $url['blog_luchetti'];?>" title="Blog Fabio Luchetti" target="_blank">
							<h1>Blog</br>Corporativo</h1>
							<h2>Fabio Luchetti</h2>
						</a>
					</div>
					<div class="clearfix"></div>
				</li>
				<li>
					<div class="flutua-esq avatar sonia-rica"></div>
					<div class="descricao-avatar">
						<a href="<?php echo $url['blog_soniarica'];?>" title="Fórum de Atendimento" target="_blank">
							<h1>Fórum de</br>Atendimento</h1>
							<h2>Sônia Rica</h2>
						</a>
					</div>
					<div class="clearfix"></div>
				</li>
				<li>
					<div class="flutua-esq avatar organograma"></div>
					<div class="descricao-avatar">
						<a href="#SemConteudoAinda" title="">
							<h1>Organograma</h1>
							<h2>Estrutura da Diretoria</br> de Atendimento</h2>
						</a>
					</div>
					<div class="clearfix"></div>
				</li>
				<li>
					<div class="flutua-esq avatar boletim-fluir"></div>
					<div class="descricao-avatar">
						<a href="#SemConteudoAinda" title="">
							<h1>Boletim</br>FLUIR</h1>
							<h2>Último status</h2>
						</a>
					</div>
					<div class="clearfix"></div>
				</li>
				<li>
					<div class="flutua-esq avatar boletim-pomar"></div>
					<div class="descricao-avatar">
						<a href="#SemConteudoAinda" title="">
							<h1>Boletim</br>POMAR</h1>
							<h2>Último status</h2>
						</a>
					</div>
					<div class="clearfix"></div>
				</li>
			</ul>
		</div>
	</div><!-- /fim esquerda-->

	<div id="centro" class="flutua-esq centro">

		<div class="destaques">
			<h2>O que acontece:</h2>
			<ul>
				<li>
					<h4>Estratégia e Gorvenança</h4><!-- Depto noticia -->
					<h1><a href="#SemConteudoAinda" title="Novo WFM">Novo WFM</a></h1><!-- Titulo noticia -->
					<p>Sistema de planejamento e gestão de escalas.</p><!-- Descrição noticia -->
				</li>
				<li>
					<h4>Estratégia e Gorvenança</h4>
					<h1><a href="#SemConteudoAinda" title="Programa Fluir">Programa Fluir</a></h1>
					<p>Programa de melhoria contínua dos processos de planejamento, controle e MIS.</p>
				</li>
				<li>
					<h4>Estratégia e Gorvenança</h4>
					<h1><a href="#SemConteudoAinda" title="POMAR">POMAR</a></h1>
					<p>Programa de otimização do modelo de atendimento e relacionamento.</p>
				</li>
				<li>
					<h4>Desenvolvimento de Atendimento</h4>
					<h1><a href="#SemConteudoAinda" title="Destaques Nota 11">Destaques Nota 11</a></h1>
					<p>Confira os destques das centrais de atendimento.</p>
				</li>
			</ul>
		</div>

	</div><!-- /fim centro-->

	<div id="direita" class="flutua-esq direita">
		<h2 style="">Links rápidos:</h2>
		<h1>Estratégia e Gorvenança</h1>

		<p style="color:#274b6d;font-size:1em;margin:2% 0% 0% 4%;">Diretoria de Atendimento</p>
    	<div class="carrossel">
			<div id="diretoria" class="diretoria" >
				<div id="rec_gra_dir" class="graph"></div>
				<div id="abd_gra_dir" class="graph"></div>
				<div id="ns_gra_dir" class="graph"></div>
			</div>
		</div>

		<p style="color:#274b6d;font-size:1em;margin:2% 0% 0% 4%;">Demais diretorias</p>
		<div class="carrossel">
			<div id="demais" class="demais">
				<div id="rec_gra_dem" class="graph"></div>
				<div id="abd_gra_dem" class="graph"></div>
				<div id="ns_gra_dem" class="graph"></div>
			</div>
		</div>

	</div><!-- /fim direita -->

	<div class="clearfix"></div>
</div><!-- /fim conteudo -->

<script src="<?php echo $base_root;?>/mismobile/node/js/highcharts.js" type="text/javascript"></script>
<script src="<?php echo $base_root;?>/mismobile/node/js/performance.js" type="text/javascript"></script>
