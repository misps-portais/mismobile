<?php

$path_parts = explode('theme',getcwd());
$url_parts = explode('?', $_SERVER['REQUEST_URI']);

define('DRUPAL_ROOT', $path_parts[0]);

require(DRUPAL_ROOT.'\includes\lessc.inc');

$filename = $url_parts[1].'.less';
$input = getcwd().'\less\\'.$filename;

try{
	header("Content-Type: text/css");
	$less = new lessc();
	#$less->setFormatter('compressed');
	print $less->compileFile($input);
}
catch (exception $ex){
	print $ex->getMessage();
}