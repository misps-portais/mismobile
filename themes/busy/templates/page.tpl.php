<?php

function menu_departamentos(){

	global $user;
	//$regras_usuario = $user->roles;
	//$regras_permitidas = array('administrator','docs_rw');
	
	$conn = odbc_connect('MISPG','',''); 
	$sql = "
      SELECT id, nome FROM docs.tbl_departamentos WHERE status=1 AND disponivel_menu=1 AND pai=1 ORDER By nome ASC";
	$exec = odbc_exec($conn,$sql);
	$linhas = odbc_num_rows($exec);
	
	if($linhas > 0){
	
		$li = null;
		
		while($resultado = odbc_fetch_array($exec)){
			$li .= '<li>';
			$li .= '<a href="http://172.23.14.155/mismobile/documentos?acao=indice&area='.$resultado['id'].'">'.$resultado['nome'].'</a>';
			$li .= '</li>';
		}
	}
	else{
		$li = '';
	}
	
	return $li;
}


?>
    <div id="container">
      <div id="header-wrapper">
        <div id="header-top">
          <div id="logo-floater">
            <?php if ($logo || $site_title): ?>
            <div id="branding" class="clearfix">
              <a href="<?php print $front_page ?>" title="<?php print $site_name_and_slogan ?>">
                <?php if ($logo): ?>
                <img src="http://172.23.14.155/mismobile/node/img/logo_portal_final.png" alt="<?php print $site_name_and_slogan ?>" id="logo" />
                <?php endif; ?>
              </a>
            </div>
            <?php else: /* Use h1 when the content title is empty */ ?>
            <h1 id="branding">
              <a href="<?php print $front_page ?>" title="<?php print $site_name_and_slogan ?>">
                <?php if ($logo): ?>
                <img src="<?php print $logo ?>" alt="<?php print $site_name_and_slogan ?>" id="logo" />
                <?php endif; ?>
              </a>
            </h1>
            <?php endif; ?>
          </div>            

          <?php if ($main_menu): ?>
<div class = 'menu-mis' id='menu-edit'>

  <ul id="main-menu" class="links clearfix">
    <li id = 'menu0' style="background: transparent url(http://172.23.14.155/mismobile/themes/busy/images/main-menu-separator-2.jpg) no-repeat right -1px;">
      <a  href="home" style='font-weight:bold'>Home</a>
    </li>
    <li id = 'menu1' style="background: transparent url(http://172.23.14.155/mismobile/themes/busy/images/main-menu-separator-2.jpg) no-repeat right -1px;">
		<a style='font-weight:'>Relatórios</a>
	<ul>
        <li >
          <a href="perf">Performance</a>
        </li>
        <li >
          <a href="portomaps">Porto Maps</a>
        </li>
        <li >
          <a >PPR 2013</a>
        </li>
        <li >
          <a >Escola de Atendimento</a>
        </li>
        <li >
          <a >Ofensores de quadro</a>
        </li>
        <li >
          <a href="boletim" >Boletins MIS</a>
        </li>
      </ul>
    </li>
    <li id = 'menu2' style="background: transparent url(http://172.23.14.155/mismobile/themes/busy/images/main-menu-separator-2.jpg) no-repeat right -1px;">
      <a style='font-weight:'>Documentos</a>
	  <ul>
		  <li><a href="http://172.23.14.155/mismobile/documentos?acao=indice" title="">Indice de Documentos</a></li>
      <?php echo menu_departamentos(); ?>
      <li><a href="http://172.23.14.155/mismobile/documentos?acao=novo" title="">Enviar Documento</a></li>
	  </ul>
	  
<!--
      <ul>
		<li><a href="envia-documento" title="">Enviar Documento</a></li>
		<li><a href="indice-documentos" title="">Indice de Documentos</a></li>
        <li><a href="indice-documentos?area=" title="">Planejamento e Controle</a></li>
        <li><a href="indice-documentos?area=" title="">NPC 1</a></li>
        <li><a href="indice-documentos?area=" title="">NPC 2</a></li>
        <li><a href="indice-documentos?area=" title="">NPC 3</a></li>
        <li><a href="indice-documentos?area=" title="">Porto Conecta</a></li>
      </ul>
-->
    </li>
    <li id = 'menu3'>
      <a href="solicitacoes">
        Solicitações
      </a>
    </li>
  </ul>

</div>
            <?php endif; ?>
          <?php if ($page['header_top_right']): ?>
          <div id="header-top-right" class="clearfix">
            <?php print render($page['header_top_right']); ?>
          </div>
          <?php endif; ?>
        </div>
        <div id="header" class="clearfix">
          <center></center>
          <?php if ($page['header_left']): ?>
          <div id="header-left">
            <?php print render($page['header_left']); ?>
          </div>
          <?php endif; ?>
          <?php if ($page['header_right'] || $site_slogan): ?>
          <div id="header-right">
            <div id="site-slogan">
              <?php print $site_slogan ?>
            </div>
            <?php print render($page['header_right']); ?>
          </div>
          <?php endif; ?>
        </div>
      </div>
      <div id="main-wrapper">
        <div id="main" class="clearfix">
          <div id="content"<?php print ($main_menu ? ' class="has-main-menu"' : ''); ?>>

            <div id="content-area">

              <?php if ($page['highlight']): ?>
              <div id="highlight"><?php print render($page['highlight']); ?></div>
              <?php endif; ?>
              <a id="main-content"></a>
              

             
              <?php print $messages; ?>
              <?php print render($page['help']); ?>
              <?php if ($action_links): ?><ul class="action-links"><?php print render($action_links); ?></ul><?php endif; ?>
              <div class="clearfix">
                <?php print render($page['content']); ?><!-- -->
              </div>
              <?php print $feed_icons ?>
            </div>
          </div>
          <?php if ($page['sidebar_first']): ?>
          <div class="sidebar-first sidebar">
            <?php print render($page['sidebar_first']); ?>
            <?php print theme('links__system_secondary_menu', array('links' => $secondary_menu, 'attributes' => array('id' => 'secondary-menu', 'class' => array('links', 'clearfix')))); ?>
          </div>
          <?php endif; ?>
        </div>
      </div>
<div id="page-footer" class="clearfix">
          <div class="region region-footer">
    <div id="block-system-powered-by" class="block block-system">

    
  <div class="content">
      </div>
</div>
  </div>
      </div>
    </div>

<!-- ALOMAUA <?php print $logo; print render($page['content']); ?>-->