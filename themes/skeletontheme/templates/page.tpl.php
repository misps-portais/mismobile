<div id="wrap">
    <div class="container">

	<!-- #header -->
        <div id="header" class="sixteen columns clearfix" style="background-color:rgb(16,181,240)">
            <div class="inner">
    
                    <img style="float:left;max-width: 42px;max-height: 50px;width:10%;height:12%" src="http://172.23.14.155/mis/node/img/head7.png" alt="<?php print t('Home'); ?>" />

               
                <div id="name-and-slogan">
                
                    <div id="site-name" style="padding-left: 50px;padding-top:5px">
                        <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" style="color:#ffffff;font-size:.8em;letter-spacing:0px;" rel="home">ATENDIMENTO</a>
<b style="font-size: 12px;display: block;color: white;line-height: 20px;margin-top: 0px;margin-left: -10px;">
                            <img src="http://172.23.14.155/mis/sites/default/files/color/garland-eaa03c42/logo.png" style="width: 20px;height: 20px;padding: 0 10px;" alt="ATENDIMENTO " title="ATENDIMENTO " id="logo">
                        by MIS
                        </b>
                    </div>
               
                </div>
            </div>
        </div><!-- /#header -->
        
        <!-- #navigation -->
        <div id="navigation" class="sixteen columns clearfix">
        
            <div class="menu-header">
                <?php print theme('links__system_main_menu', array(
                'links' => $main_menu,
                'attributes' => array(
                    'id' => 'main-menu-links',
                    'class' => array('menu', 'clearfix'),
                ),
                'heading' => array(
                    'text' => t('Main menu'),
                    'level' => 'h2',
                    'class' => array('element-invisible'),
                ),
                )); ?>
            </div>
            
        </div><!-- /#navigation -->
        
        <?php if ($page['sidebar_first']) { ?>
        <div id="content" class="eleven columns">
        <?php } else { ?>
        <div id="content" class="sixteen columns clearfix">
        <?php } ?>
        
            <?php if ($messages): ?>
                <div id="messages">
                  <?php print $messages; ?>
                </div><!-- /#messages -->
            <?php endif; ?>
        
            <?php if ($breadcrumb): ?>
                <div id="breadcrumb"><?php print $breadcrumb; ?></div>
            <?php endif; ?>
            
            <div id="main">
            
                <?php if ($page['highlighted']): ?><div id="highlighted"><?php print render($page['highlighted']); ?></div><?php endif; ?>
                
                <?php print render($title_prefix); ?>
                
                <?php if ($title): ?>
                <h1 class="title" id="page-title">
                  <?php print $title; ?>
                </h1>
                <?php endif; ?>
                
                <?php print render($title_suffix); ?>
                
                <?php if ($tabs): ?>
                <div class="tabs">
                  <?php print render($tabs); ?>
                </div>
                <?php endif; ?>
                
                <?php print render($page['help']); ?>
                
                <?php if ($action_links): ?>
                <ul class="action-links">
                  <?php print render($action_links); ?>
                </ul>
                <?php endif; ?>
                
                <?php print render($page['content']); ?>
                <?php print $feed_icons; ?>
                
            </div>
        
        </div><!-- /#content -->
        
        <?php if ($page['sidebar_first']): ?>
        <!-- #sidebar-first -->
        <div id="sidebar" class="five columns">
            <?php print render($page['sidebar_first']); ?>
        </div><!-- /#sidebar-first -->
        <?php endif; ?>
        
        <div class="clear"></div>
        
        <?php if ($page['featured_left'] || $page['featured_right']): ?>
        <!-- #featured -->
        <div id="featured" class="sixteen columns clearfix">
            
            <?php if ($page['featured_left'] && $page['featured_right']) { ?>
            <div class="one_half">
            <?php print render($page['featured_left']); ?>
            </div>
            
            <div class="one_half last">
            <?php print render($page['featured_right']); ?>
            </div>
            <?php } else { ?>
                
            <?php print render($page['featured_left']); ?>
            <?php print render($page['featured_right']); ?>
            
            <?php } ?>
            
        </div><!-- /#featured -->
        <?php endif; ?>
        
        <div class="clear"></div>
        <center><a href='/mis/user/logout'>Sair</a></center>
    </div>
</div> <!-- /#wrap -->