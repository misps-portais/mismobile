<?php
global $base_root;

# Não esteja logado, redireciona!
if(preg_match('/Access denied/', $head_title)){
	header('Location: '.$base_root.'/mismobile/user/login');
}

# Está logado e quer sair, redireciona para o login
if($_SERVER['REQUEST_URI'] == '/mismobile/user/logout'){
	header('Location: '.$base_root.'/mismobile/user/login');
}

# GAMBI PARA HABILIDADES, SEMPRE REDIRECIONA PARA /HABILIDADES
#if($_SERVER['REQUEST_URI'] == '/mismobile/'){
	#header('Location: '.$base_root.'/mismobile/habilidades');
#}


?>

<html>
	<head>
		<title><?php print $head_title; ?></title>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">  
		<link rel="shortcut icon" href="<?php echo $GLOBALS['base_url'];?>/favicon.ico" />
		
		<?php if(preg_match('/login/i', $_SERVER['REQUEST_URI'])){ ?>		
		<link href="<?php echo $GLOBALS['base_url'];?>/themes/portal/style?login" rel="stylesheet" type="text/css" />
		<?php }else{?>
		<link href="<?php echo $GLOBALS['base_url'];?>/themes/portal/style?portal" rel="stylesheet" type="text/css" />
		<?php }?>

		<?php
			if(preg_match('/boletim/i', $_SERVER['REQUEST_URI'])){
				echo '<meta http-equiv="refresh" content="600" />';
				echo '<link href="../mis/node/css/myTheme.css" rel="stylesheet" type="text/css" />'; 
			}

			if(preg_match('/documentos/i', $_SERVER['REQUEST_URI'])){
				echo '<link href="'.$GLOBALS['base_url'].'/themes/portal/style?documentos" rel="stylesheet" type="text/css" />'; 
			}
		?>
		<script src="<?php echo $GLOBALS['base_url'];?>/themes/portal/js/portal.js" type="text/javascript"></script>
		<script src="<?php echo $GLOBALS['base_url'];?>/themes/portal/js/jquery-1.10.2.min.js" type="text/javascript"></script>
	</head>

	<body>
		<?php print $page;?>
		<!--[if IE]><script type="text/javascript">go('<?php echo $GLOBALS['base_url'];?>/blocked.html');</script><![endif]-->
	</body>
</html>