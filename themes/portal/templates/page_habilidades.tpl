<?php

if(preg_match('/login/i',$_SERVER['REQUEST_URI'])){
	echo "<script type=\"text/javascript\">document.title = 'Login | Portal de Atendimento';</script>";

	if(preg_match('/field is required/', $messages)){
		echo "<script type=\"text/javascript\">erro_login('Os campos não podem estar vazios.','".$GLOBALS['base_url']."/themes/portal/images/dados_incorretos.png');</script>";
	}

	if(preg_match('/unrecognized username or password/', $messages)){
		echo "<script type=\"text/javascript\">erro_login('Usuário/senha incorretos','".$GLOBALS['base_url']."/themes/portal/images/dados_incorretos.png');</script>";
	}

	if(preg_match('/Sorry, there have been more than 5 failed login attempts for this account./', $messages)){
		echo "<script type=\"text/javascript\">alert('Foram realizadas mais de 5 tentativas de login para esta conta que foi temporariamente bloqueada. Tente novamente mais tarde.');</script>";
	}
}

if(preg_match('/home/i', $head_title)){
	$ptl_breadcrumb = '';
}

if(preg_match('/documentos/i', $_SERVER['REQUEST_URI'])){
	$ptl_breadcrumb = 'Documentos';
}

if(preg_match('/relatorios/i', $_SERVER['REQUEST_URI'])){
	$ptl_breadcrumb = 'Relatórios';
}

if(preg_match('/sistemas/i', $_SERVER['REQUEST_URI'])){
	$ptl_breadcrumb = 'Sistemas';
}

if(preg_match('/solicitacoes/i', $_SERVER['REQUEST_URI'])){
	$ptl_breadcrumb = 'Solicitações';
}

###  GAMBI PARA HABILIDADES
##
if(preg_match('/habilidades/i',$_SERVER['REQUEST_URI'])){ 
	setcookie('habilidades', true);
}

if(preg_match('/habilidades/i',$_SERVER['HTTP_REFERER'])){ 
	setcookie('habilidades', true);
}

?>

<!-- CONTAINER PRINCIPAL  -->
<div class="container-principal">
	<div class="barra-usuario">
		<div class="interno">
			<?php if(user_is_logged_in()){ ?>
			<div class="flutua-dir">
				<a href="<?php echo $GLOBALS['base_url'];?>/user/logout" class="botao25 bt-sair" ><span class="flutua-esq">Sair</span></a>
			</div>
			<div class="flutua-dir nome-usuario">Bem vindo, <?php echo $user->name;?></div>
			<?php } ?>
		</div>
	</div>
	<div class="banner">
		<div class="cabecalho">
			<div class="interno">
				<div class="flutua-esq logo-cabecalho">
					<img src="<?php echo $GLOBALS['base_url'];?>/themes/portal/images/logo.png" alt="Logo Portal" />
					<!--
					<a href="<?php echo $GLOBALS['base_url'];?>" title="Página Inicial">
						<img src="<?php echo $GLOBALS['base_url'];?>/themes/portal/images/logo.png" alt="Logo Portal" />
					</a>
					-->
				</div>
				<?php if(user_is_logged_in()){?>
				<div class="flutua-dir menu-principal">
					<ul>
						<li class="flutua-dir"><span class="botao40 bt-solicitacoes" onmouseover="exibe_breadcrumb('Solicitações');" onmouseout="exibe_breadcrumb('<?php echo $ptl_breadcrumb;?>');" ></span></li>
						<li class="flutua-dir"><span class="botao40 bt-sistemas" onmouseover="exibe_breadcrumb('Sistemas');" onmouseout="exibe_breadcrumb('<?php echo $ptl_breadcrumb;?>');" ></span></li>
						<li class="flutua-dir"><span class="botao40 bt-documentos" onmouseover="exibe_breadcrumb('Documentos');" onmouseout="exibe_breadcrumb('<?php echo $ptl_breadcrumb;?>');" ></span></li>
						<li class="flutua-dir"><span class="botao40 bt-relatorios" onmouseover="exibe_breadcrumb('Relatórios');" onmouseout="exibe_breadcrumb('<?php echo $ptl_breadcrumb;?>');" ></span></li>
						<li class="flutua-dir"><span class="botao40 bt-busca" onmouseover="exibe_breadcrumb('Busca');" onmouseout="exibe_breadcrumb('<?php echo $ptl_breadcrumb;?>');" ></span></li>
					</ul>
					<!--
					<ul>
						<li class="flutua-dir"><a href="solicitacoes?acao=home" title="Solicitações" class="botao40 bt-solicitacoes" onmouseover="exibe_breadcrumb('Solicitações');" onmouseout="exibe_breadcrumb('<?php echo $ptl_breadcrumb;?>');" ></a></li>
						<li class="flutua-dir"><a href="sistemas?acao=todos" title="Sistemas" class="botao40 bt-sistemas" onmouseover="exibe_breadcrumb('Sistemas');" onmouseout="exibe_breadcrumb('<?php echo $ptl_breadcrumb;?>');" ></a></li>
						<li class="flutua-dir"><a href="documentos?acao=todos" title="Documentos" class="botao40 bt-documentos" onmouseover="exibe_breadcrumb('Documentos');" onmouseout="exibe_breadcrumb('<?php echo $ptl_breadcrumb;?>');" ></a></li>
						<li class="flutua-dir"><a href="relatorios?acao=todos" title="Relatorios" class="botao40 bt-relatorios" onmouseover="exibe_breadcrumb('Relatórios');" onmouseout="exibe_breadcrumb('<?php echo $ptl_breadcrumb;?>');" ></a></li>
						<li class="flutua-dir"><a href="#" title="Busca" class="botao40 bt-busca" onmouseover="exibe_breadcrumb('Busca');" onmouseout="exibe_breadcrumb('<?php echo $ptl_breadcrumb;?>');" ></a></li>
					</ul>
					-->
					<div class="clearfix"></div>
				</div>
				<div class="flutua-dir breadcrumb" id="breadcrumb"><?php echo $ptl_breadcrumb;?></div>
				<?php }?>
			</div>
		</div>
		<div class="baseline"></div>
	</div>
	<div class="conteudo">
		<?php print render($page['content']); ?><div class="clearfix"></div>
	</div>
	<div class="rodape">
		<div class="interno">
			<div class="direitos">© <?php echo date('Y')?> Porto Seguro - Todos os direitos reservados.</div>
		</div>
	</div>
</div>
<!-- / CONTAINER PRINCIPAL  -->

<!-- LOGIN CONTAINER -->

<?php 
if(!isset($_COOKIE['habilidades'])){ ?>

<div class="container-login">
	<div class="barra-usuario"></div>
	<div class="conteudo">
		<div class="caixa-login">
			<div class="cabecalho-caixa">
				<a href="<?php echo $GLOBALS['base_url'];?>/user/login"><img src="<?php echo $GLOBALS['base_url'];?>/themes/portal/images/logo.png" /></a>
			</div>
			<div class="conteudo-caixa" >
				<div class="flutua-dir cc-direita">
					<form action="/mismobile/user/login?destination=node/3" method="POST" name="login" accept-charset="UTF-8" >
						<input type="text" name="name" id="username" placeholder="Matricula" class="text" /><br/>
						<input type="password" name="pass" id="password" placeholder="Senha" class="text" /><br/>
						<input type="submit" value="ENTRAR" class="submit" />
						<input type="hidden" name="form_id" value="user_login" />
					</form>
				</div>
				<div class="flutua-dir cc-esquerda">
					<img src="<?php echo $GLOBALS['base_url'];?>/themes/portal/images/cracha.png" id="cracha" />
					<p id="mensagem"></p>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div><!-- /fim conteudo -->

	<div class="rodape">
		<div class="interno">
			<div class="navegacao">
				<ul >
					<li class="flutua-esq">
						<a href="#" title="Sobre o portal" class="botao40 botao-sobre" onclick="exibe_item_rodape('sobre');" ><span class="flutua-esq">Sobre o<br/>portal</span></a>
					</li>
					<li class="flutua-esq">
						<a href="#" title="Perdeu sua senha" class="botao40 botao-senha" onclick="exibe_item_rodape('perdeu-senha');" ><span class="flutua-esq">Perdeu sua<br/>senha?</span></a>
					</li>
					<li class="flutua-esq">
						<a href="#" title="Fale Conosco" class="botao40 botao-fale" onclick="exibe_item_rodape('fale-conosco');" ><span class="flutua-esq">Fale<br/>Conosco</span></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>

			<div class="conteudo-rodape" id="footer-content"></div>

			<div class="secao-rodape" id="sobre">
				<p>Sobre: Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra lá , depois divoltis porris, paradis. Paisis, filhis, espiritis santis. Mé faiz elementum girarzis, nisi eros vermeio, in elementis mé pra quem é amistosis quis leo. Manduma pindureta quium dia nois paga. Sapien in monti palavris qui num significa nadis i pareci latim. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis.</p>
			</div><!-- /fim sobre -->
			<div class="secao-rodape" id="perdeu-senha">
				<p>Senha: Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra lá , depois divoltis porris, paradis. Paisis, filhis, espiritis santis. Mé faiz elementum girarzis, nisi eros vermeio, in elementis mé pra quem é amistosis quis leo. Manduma pindureta quium dia nois paga. Sapien in monti palavris qui num significa nadis i pareci latim. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis.</p>
			</div><!-- /fim perdeusenha -->
			<div class="secao-rodape" id="fale-conosco">
				<p>Fale: Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra lá , depois divoltis porris, paradis. Paisis, filhis, espiritis santis. Mé faiz elementum girarzis, nisi eros vermeio, in elementis mé pra quem é amistosis quis leo. Manduma pindureta quium dia nois paga. Sapien in monti palavris qui num significa nadis i pareci latim. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis.</p>
			</div><!-- /fim fale conosco -->

			<div class="clearfix"></div>
		</div>
	</div><!-- /fim footer -->

<?php }else{ ?>
<style>

/* Selections */

::selection {
 	color: #fff;
 	text-shadow: none;
 	background: #444;
}

::-moz-selection {
 	color: #fff;
 	text-shadow: none;
 	background: #444;
}

/* Basics */

.container-login{
	width: 100%;
	height: 100%;
	font-family: "Helvetica Neue", Helvetica, sans-serif;
	color: #444;
	-webkit-font-smoothing: antialiased;
	background: #f0f0f0;
	
}

.container-login h2{
	font-weight:normal;
	margin-top: 30px;
	text-align: center;
}

.caixa-login{
	position: fixed;
	width: 340px;
	height: 340px;
	top: 40%;
	left: 50%;
	margin-top: -140px;
	margin-left: -170px;
	background: #fff;
	border-radius: 3px;
	border: 1px solid #ccc;
	box-shadow: 0 1px 2px rgba(0, 0, 0, .1);
	-webkit-animation-name: bounceIn;
	-webkit-animation-fill-mode: both;
	-webkit-animation-duration: 1s;
	-webkit-animation-iteration-count: 1;
	-webkit-animation-timing-function: linear;
	-moz-animation-name: bounceIn;
	-moz-animation-fill-mode: both;
	-moz-animation-duration: 1s;
	-moz-animation-iteration-count: 1;
	-moz-animation-timing-function: linear;
	animation-name: bounceIn;
	animation-fill-mode: both;
	animation-duration: 1s;
	animation-iteration-count: 1;
	animation-timing-function: linear;
}

.caixa-login form {
	margin: 0 auto;
	margin-top: 20px;
}

.caixa-login label {
	color: #555;
	display: inline-block;
	margin-left: 18px;
	padding-top: 10px;
	font-size: 14px;
}

.caixa-login p a {
	font-size: 11px;
	color: #aaa;
	float: right;
	margin-top: -13px;
	margin-right: 20px;
	-webkit-transition: all .4s ease;
	-moz-transition: all .4s ease;
	transition: all .4s ease;
}

.caixa-login p a:hover {
	color: #555;
}

.caixa-login input {
	font-family: "Helvetica Neue", Helvetica, sans-serif;
	font-size: 12px;
	outline: none;
}

.caixa-login input[type=text],
.caixa-login input[type=password] {
	color: #777;
	padding-left: 10px;
	margin: 10px;
	margin-top: 12px;
	margin-left: 18px;
	width: 290px;
	height: 35px;
	border: 1px solid #c7d0d2;
	border-radius: 2px;
	box-shadow: inset 0 1.5px 3px rgba(190, 190, 190, .4), 0 0 0 5px #f5f7f8;
	-webkit-transition: all .4s ease;
	-moz-transition: all .4s ease;
	transition: all .4s ease;
}

.caixa-login input[type=text]:hover,
.caixa-login input[type=password]:hover {
	border: 1px solid #b6bfc0;
	box-shadow: inset 0 1.5px 3px rgba(190, 190, 190, .7), 0 0 0 5px #f5f7f8;
}

.caixa-login input[type=text]:focus,
.caixa-login input[type=password]:focus {
	border: 1px solid #a8c9e4;
	box-shadow: inset 0 1.5px 3px rgba(190, 190, 190, .4), 0 0 0 5px #e6f2f9;
}

.caixa-login #lower {
	background: #ecf2f5;
	width: 100%;
	height: 69px;
	margin-top: 20px;
	box-shadow: inset 0 1px 1px #fff;
	border-top: 1px solid #ccc;
	border-bottom-right-radius: 3px;
	border-bottom-left-radius: 3px;
}

.caixa-login input[type=checkbox] {
	margin-left: 20px;
	margin-top: 30px;
}

.caixa-login .check {
	margin-left: 3px;
	font-size: 11px;
	color: #444;
	text-shadow: 0 1px 0 #fff;
}

.caixa-login input[type=submit] {
	float: right;
	margin-right: 20px;
	margin-top: 20px;
	width: 80px;
	height: 30px;
	font-size: 14px;
	font-weight: bold;
	color: #fff;
	background-color: #acd6ef; /*IE fallback*/
	background-image: -webkit-gradient(linear, left top, left bottom, from(#acd6ef), to(#6ec2e8));
	background-image: -moz-linear-gradient(top left 90deg, #acd6ef 0%, #6ec2e8 100%);
	background-image: linear-gradient(top left 90deg, #acd6ef 0%, #6ec2e8 100%);
	border-radius: 30px;
	border: 1px solid #66add6;
	box-shadow: 0 1px 2px rgba(0, 0, 0, .3), inset 0 1px 0 rgba(255, 255, 255, .5);
	cursor: pointer;
}

.caixa-login input[type=submit]:hover {
	background-image: -webkit-gradient(linear, left top, left bottom, from(#b6e2ff), to(#6ec2e8));
	background-image: -moz-linear-gradient(top left 90deg, #b6e2ff 0%, #6ec2e8 100%);
	background-image: linear-gradient(top left 90deg, #b6e2ff 0%, #6ec2e8 100%);
}

.caixa-login input[type=submit]:active {
	background-image: -webkit-gradient(linear, left top, left bottom, from(#6ec2e8), to(#b6e2ff));
	background-image: -moz-linear-gradient(top left 90deg, #6ec2e8 0%, #b6e2ff 100%);
	background-image: linear-gradient(top left 90deg, #6ec2e8 0%, #b6e2ff 100%);
}

</style>
<div class="container-login">
	
	<div class="caixa-login">
		<h2>Banco de Habilidades</h2>
		<form action="/mismobile/user/login?destination=node/33" method="POST" name="login" accept-charset="UTF-8" >
			<label for="username">Username:</label>
			<input type="text" id="username" name="name" placeholder="Matricula" />

			<label for="password">Password:</label>
			<input type="password" id="password" name="pass" placeholder="Senha" />

			<div id="lower">
				<input type="submit" value="Logar">
			</div><!--/ lower-->

			<input type="hidden" name="form_id" value="user_login" />
		</form>
	</div>
<?php
}
?>
</div>
<!-- LOGIN CONTAINER -->
