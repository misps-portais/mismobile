<?php


function portal_theme() {
  $items = array();
  $items['user_login'] = array(
    'render element' => 'form',
    'path' => drupal_get_path('theme', 'portal') . '/templates',
    'template' => 'user_login',
    'preprocess functions' => array(
    'portal_preprocess_user_login'
    ),
  );

  $items['user_register_form'] = array(
    'render element' => 'form',
    'path' => drupal_get_path('theme', 'portal') . '/templates',
    'template' => 'user-register-form',
    'preprocess functions' => array(
    'portal_preprocess_user_register_form'
    ),
  );
  return $items;
}

/*function portal_preprocess_user_login(&$vars) {
  $vars['intro_text'] = t('This is my awesome login form');
}
function portal_preprocess_user_register_form(&$vars) {
  $vars['intro_text'] = t('This is my super awesome reg form');
}*/

function hook_user_login(&$edit, $account) {
}





?>