<?php
	$acao = $_GET['acao'];
	$tabela = $_GET['tabela'];
	
	$conn=odbc_connect('MISPG','','');
	
	//Inserir novo registro
	if ($acao=='Inserir') {
		$inserir = $_GET['strinsert'];
		$strsql = utf8_decode("INSERT INTO skills.$tabela VALUES $inserir");
		odbc_exec($conn,$strsql);
	}
	
	if ($acao=='editar') {
		$inserir = $_GET['strinsert'];
		$id = $_GET['id'];
		$strsql = utf8_decode("UPDATE skills.$tabela SET $inserir WHERE id=$id;");
		odbc_exec($conn,$strsql);
	}
	
	if ($tabela=='tbl_skill') {
		$strsql = "SELECT 
						tsk.servidor,
						tsk.dac,
						ts.servidor,
						td.dac,
						qr.skill,
						tt.tipo,
						th.calchc,
						tn.npc,
						tc.canal,
						tm.midia,
						tsk.sacdec,
						tsk.emergencial,
						tsk.vip,
						tsk.slaabd,
						tsk.slans,
						tsk.acceptable,
						tsk.".chr(34)."24horas".chr(34).",
						tas1.assunto_1,
						tas2.assunto_2,
						tas3.assunto_3,
						tdir.diretor,
						tdira.diretoria,
						tf.fila,
						tsk.ativo
					FROM skills.tbl_skill tsk
					INNER JOIN (
							SELECT
								max(id) as id,
								servidor,
								dac,
								skill
							FROM skills.tbl_skill
							GROUP BY 
								servidor,
								dac,
								skill
						) as qr
					ON qr.id = tsk.id
					LEFT JOIN skills.tbl_servidor ts
						ON (ts.id = tsk.servidor)
					LEFT JOIN skills.tbl_dac td
						ON (td.id = tsk.dac)
					LEFT JOIN skills.tbl_tipo tt
						ON (tt.id = tsk.tipo)
					LEFT JOIN skills.tbl_calchc th
						ON (th.id = tsk.calchc)
					LEFT JOIN skills.tbl_npc tn
						ON (tn.id = tsk.npc)
					LEFT JOIN skills.tbl_canal tc
						ON (tc.id = tsk.canal)
					LEFT JOIN skills.tbl_midia tm
						ON (tm.id = tsk.midia)
					LEFT JOIN skills.tbl_assunto_1 tas1
						ON (tas1.id = tsk.assunto_1)
					LEFT JOIN skills.tbl_assunto_2 tas2
						ON (tas2.id = tsk.assunto_2)
					LEFT JOIN skills.tbl_assunto_3 tas3
						ON (tas3.id = tsk.assunto_3)
					LEFT JOIN skills.tbl_diretor tdir
						ON (tdir.id = tsk.diretor)						
					LEFT JOIN skills.tbl_diretoria tdira
						ON (tdira.id = tsk.diretoria)
					LEFT JOIN skills.tbl_fila tf
						ON (tf.id = tsk.fila)							
					ORDER BY 1, 2, 3, 4, 5, 6";
		
		$rs=odbc_exec($conn,$strsql);
		
		echo "<div align='right'><input type='button'  class='botao' style='background-color:#96c8ff;' value='Novo Skilll' onclick='novoskill()'></div>";
		
		echo "<table id='mytable' name=$tabela style='position:relative;left:50px;width:450px'>"; //inicia tabela
		
		$mwidth = array(50, 140, 50, 50, 150, 150, 150, 150, 150, 150, 150, 150, 150, 150, 150, 150, 150, 150, 150, 150, 150, 150, 150, 150, 150);
		
		//Cabe�alho da tabela
		echo "<thead>";
		echo "<tr>";
		echo "<th>";
		echo "<input type='button' value='remover filtro' class='botao' style='position:relative' onclick='removefiltro()'>";
		echo "</th>";
		
		$mcampo = array('servidor'=>array('algoritmo'=>1, 'width'=>140),
						'dac'=>array('algoritmo'=>1, 'width'=>50),
						'skill'=>array('algoritmo'=>2, 'width'=>50),
						'tipo'=>array('algoritmo'=>1, 'width'=>100),
						'calchc'=>array('algoritmo'=>1, 'width'=>100),
						'npc'=>array('algoritmo'=>1, 'width'=>100),
						'canal'=>array('algoritmo'=>1, 'width'=>100),
						'midia'=>array('algoritmo'=>1, 'width'=>100),
						'sacdec'=>array('algoritmo'=>3, 'width'=>40),
						'emergencial'=>array('algoritmo'=>3, 'width'=>40),
						'vip'=>array('algoritmo'=>3, 'width'=>40),
						'slaabd'=>array('algoritmo'=>4, 'width'=>40),
						'slans'=>array('algoritmo'=>4, 'width'=>40),
						'acceptable'=>array('algoritmo'=>4, 'width'=>40),
						'24horas'=>array('algoritmo'=>3, 'width'=>40),
						'assunto_1'=>array('algoritmo'=>1, 'width'=>150),
						'assunto_2'=>array('algoritmo'=>1, 'width'=>150),
						'assunto_3'=>array('algoritmo'=>1, 'width'=>150),
						'diretor'=>array('algoritmo'=>1, 'width'=>140),
						'diretoria'=>array('algoritmo'=>1, 'width'=>140),
						'fila'=>array('algoritmo'=>1, 'width'=>140),
						'ativo'=>array('algoritmo'=>3, 'width'=>40),
						);
		$cont = 0;
		foreach ($mcampo as $i=> $valor) {
			$algoritmo = $mcampo[$i]['algoritmo'];
			echo "<th>";
			switch ($algoritmo) {
				case 1:
					$tabela = $i;
					$strsql = "SELECT * FROM skills.tbl_$tabela ORDER BY id";
					$lista=odbc_exec($conn,$strsql);
					$width = $mcampo[$i]['width'];
					echo "<select name='tbl_$tabela' id='filtro_tbl_$tabela' style='width:".$width."px' onchange='filtro(this.value, ".($cont+1).")'>";
					while (odbc_fetch_row($lista)) {
						$id = odbc_result($lista,1);
						$nome = odbc_result($lista,2);
						echo utf8_encode("<option value='$nome'>$nome</option>");
					}
					echo "</select>";
					break;
				case 2:
					$tabela = $i;
					$strsql = "SELECT DISTINCT skill FROM skills.tbl_$tabela ORDER BY skill";
					$lista=odbc_exec($conn,$strsql);
					$width = $mcampo[$i]['width'];
					echo "<select name='tbl_$tabela' id='tbl_$tabela' style='width:".$width."px' onchange='filtro(this.value, $cont+1)'>";
					while (odbc_fetch_row($lista)) {
						$nome = odbc_result($lista,1);
						echo utf8_encode("<option value='$nome'>$nome</option>");
					}
					echo "</select>";
					break;
				case 3:
					$tabela = $i;
					$width = $mcampo[$i]['width'];
					echo "<select name='tbl_$tabela' id='tbl_$tabela' style='width:".$width."px' onchange='filtro(this.value, $cont+1)'>";
					echo utf8_encode("<option value=1>Sim</option>");
					echo utf8_encode("<option value=0>N�o</option>");
					echo "</select>";
					break;
			}
			echo "</th>";
			$cont = $cont + 1;
		}
					
		echo "</tr>";
		echo "<tr>";
		echo "<th></th>";
		for ($cont=3;$cont<=odbc_num_fields($rs);$cont++) {
			$width = $mwidth[$cont-3];
			echo "<th>".odbc_field_name($rs,$cont)."</th>";
		}
		echo "</tr>";
		echo "</thead>";
		
		//Dados do banco
		echo "<tbody>";
		while (odbc_fetch_row($rs)) {
			echo "<tr style='display:'>";
			$servidor = odbc_result($rs,1);
			$dac = odbc_result($rs,2);
			$skill = odbc_result($rs,5);			
			echo "<td><input type='button' value='Editar'  class='botao' onclick=".chr(34)."editar_skill('consulta', '$servidor', '$dac','$skill')".chr(34)."></td>";
			for ($cont=3;$cont<=odbc_num_fields($rs);$cont++) {
			$texto = odbc_result($rs,$cont);
			$nome = odbc_field_name($rs,$cont);
			$width = $mwidth[$cont-3];
				echo utf8_encode("<td style='width:".$width."px'>$texto</td>");
			}
			echo "</tr>";
		}
		echo "</tbody>";		
		echo "</table>"; //Fecha tabela
		echo "</div>";
	}
	else {
		$strsql = "SELECT * FROM skills.$tabela ORDER BY 2";
		$rs=odbc_exec($conn,$strsql);
		echo "<table id='mytable' name='$tabela' style='position:relative;left:50px;width:450px'>"; //inicia tabela
		
		//Cabe�alho da tabela
		echo "<thead>";
		for ($cont=1;$cont<=odbc_num_fields($rs);$cont++) {
			echo "<th>".odbc_field_name($rs,$cont)."</th>";
		}
		echo "<th></th>";
		echo "</thead>";
		
		//Dados do banco
		echo "<tbody>";
		while (odbc_fetch_row($rs)) {
			echo "<tr>";
			for ($cont=1;$cont<=odbc_num_fields($rs);$cont++) {
			$texto = odbc_result($rs,$cont);
			$nome = odbc_field_name($rs,$cont);
				echo utf8_encode("<td headers='$nome'>$texto</td>");
			}
			echo "<td><input type='button' value='Editar' class='botao' onclick=".chr(34)."editar(this.value,this.parentNode.parentNode.rowIndex,this)".chr(34)."></td>";
			echo "</tr>";
		}
		echo "</tbody>";
		
		//Campo para novos inputs
		echo "<tfoot>";
		echo "<tr>";
		echo "<td></td>";
		for ($cont=1;$cont<=odbc_num_fields($rs);$cont++) {
			if (odbc_field_name($rs,$cont)<>'id') {
				echo "<td><input type='text'></td>";
			}
		}
		echo "<td><input type='button' value='Inserir' class='botao' onclick=".chr(34)."alterar(this.value,'".$tabela."')".chr(34)."></td>";
		echo "</tr>";
		echo "</tfoot>";
		
		echo "</table>"; //Fecha tabela
		echo "</div>";
	}
	odbc_close($conn);
?>
