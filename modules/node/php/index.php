<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//PT_BR" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
  <head>
    <title>Performance</title>

    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-1.9.1.js" type="text/javascript"></script>
    <script src="js/highcharts.js"></script>
    <script src="js/modules/exporting.js"></script>

  </head>
  <body>
<?php
header("Content-Type: text/html; charset=UTF-8",true);


    $conn = mysql_connect("localhost", "root", "135701");
    mysql_select_db("n_hdc",$conn);

$perf = mysql_query("
    SELECT 
        CASE `meses`.`mes`
                WHEN 1 THEN 'Janeiro'
                WHEN 2 THEN 'Fevereiro'
                WHEN 3 THEN 'Março'
                WHEN 4 THEN 'Abril'
                WHEN 5 THEN 'Maio'
                WHEN 6 THEN 'Junho'
                WHEN 7 THEN 'Julho'
                WHEN 8 THEN 'Agosto'
                WHEN 9 THEN 'Setembro'
                WHEN 10 THEN 'Outubro'
                WHEN 11 THEN 'Novembro'
                WHEN 12 THEN 'Dezembro'
        END as meses,
        SUM(`meses`.`som_not`)/SUM(`meses`.`qtd_not`) AS med
        FROM(
            SELECT 
            mes,
            SUM(nota) AS som_not,
            COUNT(NOTA) AS qtd_not
            FROM `monitoria_2013` 
            WHERE status = 'of'
            AND nota <> ''
            GROUP BY mes
        ) AS meses
        GROUP BY 
        `meses`.`mes");

//$series_str = "[";
//  while ($med_value = mysql_fetch_array($perf))
//  {
//      $series_str = $series_str."{name: '".$med_value['meses']."', data : [".number_format($med_value['med'],2,'.',',')."]},";
//  }
//  $series_str = substr($series_str,0,strlen($series_str)-1)."]";

//$seriest_str = "[";
    while ($todas_value = mysql_fetch_array($perf))
    {
        $seriest_str = $seriest_str.$todas_value['med'].",";
    }
    $seriest_str = substr($seriest_str,0,strlen($seriest_str)-1)."]";

//echo "Hello World!";

?>
<script type="text/javascript">
$(function () {
        $('#container').highcharts({
            chart: {
                zoomType: 'xy'
            },
            title: {
                text: 'Porto Seguro Performance'
            },
            subtitle: {
                text: ''
            },
            xAxis: [{
                categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
            }],
            yAxis: [{ // Primary yAxis
                labels: {
                    formatter: function() {
                        return this.value;
                    },
                    style: {
                        color: '#89A54E'
                    }
                },
                title: {
                    text: 'Mensal',
                    style: {
                        color: '#89A54E'
                    }
                },
                opposite: true
    
            }, { // Secondary yAxis
                gridLineWidth: 0,
                title: {
                    text: 'Conecta',
                    style: {
                        color: '#4572A7'
                    }
                },
                labels: {
                    formatter: function() {
                        return this.value +' mm';
                    },
                    style: {
                        color: '#4572A7'
                    }
                }
    
            }, { // Tertiary yAxis
                gridLineWidth: 0,
                title: {
                    text: 'Amostragem',
                    style: {
                        color: '#AA4643'
                    }
                },
                labels: {
                    formatter: function() {
                        return this.value +' mb';
                    },
                    style: {
                        color: '#AA4643'
                    }
                },
                opposite: true
            }],
            tooltip: {
                shared: true
            },
            legend: {
                layout: 'vertical',
                align: 'left',
                x: 120,
                verticalAlign: 'top',
                y: 80,
                floating: true,
                backgroundColor: '#FFFFFF'
            },
            series:[{
            name :'asd',
            color: '#4682B4',
            type: 'column',
            data:[<?php echo $seriest_str; ?>,
         },{    
                
                name: 'Média Mês',
                color: '#CD2626',
                type: 'line',
                data: [9.30,7.0,7.78,7.1,7.8,9.18,8.41,10],
        }]
});
});

</script>

<div id="container" style="min-width: 100%; height: 100%; margin: 0 auto"></div>

</body>
</html>