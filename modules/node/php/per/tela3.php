<style type="text/css">
#primeira{	
	width: 200px;
	height: 115px;
	z-index:1;
	float: left;
	top: 9px;
}
#segunda {	
	width:200px;
	height:115px;
	z-index:2;
	float: left;	
	left: 205px;
	top: 9px;
}
#terceira {	
	width:200px;
	height:115px;
	z-index:3;
	float: right;
	top: 9px;
}
</style>
<body>
<title>Performance - TMA / Hold / ACW </title>
    
    <script src="js/jquery-1.9.1.js" type="text/javascript"></script>
    <script src="js/highcharts.js"></script>

<?php
error_reporting(0);
$conn=odbc_connect('MISPG','','');	
			
			$sqlm = "
							SELECT * FROM proc_performance_tela3(1,39,8,'2013-08-28')
					";
			
		//Laço Mensal
			
			$rs=odbc_exec($conn,$sqlm);
			
			while(odbc_fetch_row($rs)){

					$tmam = odbc_result($rs,'acdtime');
					$abdm = odbc_result($rs,'hold_time');
					$acwm = odbc_result($rs,'acw');
					$recd = odbc_result($rs,'periodo');
					
			$seriestmam_str = $seriestmam_str.$tmam.",";
			$seriesabdm_str = $seriesabdm_str.$abdm.",";
			$seriesacwm_str = $seriesacwm_str.$acwm.",";
			$seriestabdmd_str = $seriestabdmd_str."'".$recd."'".",";
			}	
			$seriestmam_str = substr($seriestmam_str,0,strlen($seriestmam_str)-1)."]";
			$seriesabdm_str = substr($seriesabdm_str,0,strlen($seriesabdm_str)-1)."]";
			$seriesacwm_str = substr($seriesacwm_str,0,strlen($seriesacwm_str)-1)."]";
			$seriestabdmd_str = substr($seriestabdmd_str,0,strlen($seriestabdmd_str)-1)."]";
			
			
		//Laço diario
		
		$sqld = "
							SELECT * FROM proc_performance_tela3(2,39,8,'2013-08-28')
					";
			$rs=odbc_exec($conn,$sqld);
		
			while(odbc_fetch_row($rs)){

					$tmad = odbc_result($rs,'acdtime');
					$abdd = odbc_result($rs,'hold_time');
					$acwd = odbc_result($rs,'acw');
					$recd = odbc_result($rs,'periodo');
					
			$seriestmad_str = $seriestmad_str.$tmad.",";
			$seriesabdd_str = $seriesabdd_str.$abdd.",";
			$seriesacwd_str = $seriesacwd_str.$acwd.",";
			$seriestabddd_str = $seriestabddd_str."'".$recd."'".",";
			}	
			$seriestmad_str = substr($seriestmad_str,0,strlen($seriestmad_str)-1)."]";
			$seriesabdd_str = substr($seriesabdd_str,0,strlen($seriesabdd_str)-1)."]";
			$seriesacwd_str = substr($seriesacwd_str,0,strlen($seriesacwd_str)-1)."]";
			$seriestabddd_str = substr($seriestabddd_str,0,strlen($seriestabddd_str)-1)."]";
			
		//Laço Intra hora
		
		$sqlh = "
							SELECT * FROM proc_performance_tela3(3,39,8,'2013-08-28')
					";
			$rs=odbc_exec($conn,$sqlh);
		
			while(odbc_fetch_row($rs)){

					$tmah = odbc_result($rs,'acdtime');
					$abdh = odbc_result($rs,'hold_time');
					$acwh = odbc_result($rs,'acw');
					$rech = odbc_result($rs,'periodo');
					
			$seriestmah_str = $seriestmah_str.$tmah.",";
			$seriesabdh_str = $seriesabdh_str.$abdh.",";
			$seriesacwh_str = $seriesacwh_str.$acwh.",";
			$seriestabddh_str = $seriestabddh_str."'".$rech."'".",";
			}	
			$seriestmah_str = substr($seriestmah_str,0,strlen($seriestmah_str)-1)."]";
			$seriesabdh_str = substr($seriesabdh_str,0,strlen($seriesabdh_str)-1)."]";
			$seriesacwh_str = substr($seriesacwh_str,0,strlen($seriesacwh_str)-1)."]";
			$seriestabddh_str = substr($seriestabddh_str,0,strlen($seriestabddh_str)-1)."]";


?>

<script type="text/javascript">
$(function () {
        $('#primeira').highcharts({
            chart: {
                zoomType: 'x'
            },
            title: {
                text: '' 
            },
            xAxis: [{
                categories: [<?php echo $seriestabdmd_str; ?>,
				labels: {
                rotation: 300
            }
            }],
            yAxis: [{ // Primary yAxis
				lineWidth: 2,
				gridLineDashStyle: 'dot',
				min: 0,
                labels: {
                    formatter: function() {
                        return this.value;
                    },
                    style: {
                        color: '#89A54E'
                    }
                },
                title: {
                    text: ''
                },
                opposite: true
    
            }, { // Secondary yAxis
                gridLineWidth: 1,
                title: {
                    text: '',
                    style: {
                        color: '#4572A7'
                    }
                },
                labels: {
                    formatter: function() {
                        return this.value +' ';
                    },
                    style: {
                        color: '#4572A7'
                    }
                }
    
            }, { // Tertiary yAxis
                gridLineWidth: 1,
                title: {
                    text: '',
                    style: {
                        color: '#AA4643'
                    }
                },
                labels: {
                    formatter: function() {
                        return this.value +' ';
                    },
                    style: {
                        color: '#AA4643'
                    }
                },
                opposite: true
            }],
            tooltip: {
                shared: true
            },
           
            series:[{
            name :'TMA',
            color: '#4682B4',
            type: 'line',
            data:[<?php echo $seriestmam_str; ?>,
			marker: {
									enabled: false
								},
								dashStyle: 'shortdot',
								tooltip: {
									valueSuffix: ' '
								}
         },{  
		 name :'Hold',
            color: '#828282',
            type: 'line',
            data:[<?php echo $seriesabdm_str; ?>,
			marker: {
									enabled: false
								},
								dashStyle: 'shortdot',
								tooltip: {
									valueSuffix: ' '
								}
         },{  
                
                name: 'ACW',
                color: '#CD2626',
                type: 'line',
                data:[<?php echo $seriesacwm_str; ?>,
				marker: {
									enabled: false
								},
								dashStyle: 'shortdot',
								tooltip: {
									valueSuffix: ' '
								}
        }]
});
});

</script>

<script type="text/javascript">
$(function () {
        $('#segunda').highcharts({
            chart: {
                zoomType: 'x'
            },
            title: {
                text: '' 
            },
            xAxis: [{
                categories: [<?php echo $seriestabddd_str; ?>,
				labels: {
                rotation: 300
            }
            }],
            yAxis: [{ // Primary yAxis
				lineWidth: 2,
				gridLineDashStyle: 'dot',
				min: 0,
                labels: {
                    formatter: function() {
                        return this.value;
                    },
                    style: {
                        color: '#89A54E'
                    }
                },
                title: {
                    text: ''
                },
                opposite: true
    
            }, { // Secondary yAxis
                gridLineWidth: 1,
                title: {
                    text: '',
                    style: {
                        color: '#4572A7'
                    }
                },
                labels: {
                    formatter: function() {
                        return this.value +' ';
                    },
                    style: {
                        color: '#4572A7'
                    }
                }
    
            }, { // Tertiary yAxis
                gridLineWidth: 1,
                title: {
                    text: '',
                    style: {
                        color: '#AA4643'
                    }
                },
                labels: {
                    formatter: function() {
                        return this.value +' ';
                    },
                    style: {
                        color: '#AA4643'
                    }
                },
                opposite: true
            }],
            tooltip: {
                shared: true
            },
           
            series:[{
            name :'TMA',
            color: '#4682B4',
            type: 'line',
            data:[<?php echo $seriestmad_str; ?>,
			marker: {
									enabled: false
								},
								dashStyle: 'shortdot',
								tooltip: {
									valueSuffix: ' '
								}
         },{  
		 name :'Hold',
            color: '#828282',
            type: 'line',
            data:[<?php echo $seriesabdd_str; ?>,
			marker: {
									enabled: false
								},
								dashStyle: 'shortdot',
								tooltip: {
									valueSuffix: ' '
								}
         },{  
                
                name: 'ACW',
                color: '#CD2626',
                type: 'line',
                data:[<?php echo $seriesacwd_str; ?>,
				marker: {
									enabled: false
								},
								dashStyle: 'shortdot',
								tooltip: {
									valueSuffix: ' '
								}
        }]
});
});

</script>

<script type="text/javascript">
$(function () {
        $('#terceira').highcharts({
            chart: {
                zoomType: 'x'
            },
            title: {
                text: '' 
            },
            xAxis: [{
                categories: [<?php echo $seriestabddh_str; ?>,
				labels: {
                rotation: 300
            }
            }],
            yAxis: [{ // Primary yAxis
				lineWidth: 2,
				gridLineDashStyle: 'dot',
				min: 0,
                labels: {
                    formatter: function() {
                        return this.value;
                    },
                    style: {
                        color: '#89A54E'
                    }
                },
                title: {
                    text: ''
                },
                opposite: true
    
            }, { // Secondary yAxis
                gridLineWidth: 1,
                title: {
                    text: '',
                    style: {
                        color: '#4572A7'
                    }
                },
                labels: {
                    formatter: function() {
                        return this.value +' ';
                    },
                    style: {
                        color: '#4572A7'
                    }
                }
    
            }, { // Tertiary yAxis
                gridLineWidth: 1,
                title: {
                    text: '',
                    style: {
                        color: '#AA4643'
                    }
                },
                labels: {
                    formatter: function() {
                        return this.value +' ';
                    },
                    style: {
                        color: '#AA4643'
                    }
                },
                opposite: true
            }],
            tooltip: {
                shared: true
            },
           
            series:[{
            name :'TMA',
            color: '#4682B4',
            type: 'line',
            data:[<?php echo $seriestmah_str; ?>,
			marker: {
									enabled: false
								},
								dashStyle: 'shortdot',
								tooltip: {
									valueSuffix: ' '
								}
         },{  
		 name :'Hold',
            color: '#828282',
            type: 'line',
            data:[<?php echo $seriesabdh_str; ?>,
			marker: {
									enabled: false
								},
								dashStyle: 'shortdot',
								tooltip: {
									valueSuffix: ' '
								}
         },{  
                
                name: 'ACW',
                color: '#CD2626',
                type: 'line',
                data:[<?php echo $seriesacwh_str; ?>,
				marker: {
									enabled: false
								},
								dashStyle: 'shortdot',
								tooltip: {
									valueSuffix: ' '
								}
        }]
});
});

</script>

<div id="primeira" style="width: 400px; height: 250px; margin: 0 auto"></div>
<div id="segunda" style="width: 400px; height: 250px; margin: 0 auto"></div>
<div id="terceira" style="width: 400px; height: 250px; margin: 0 auto"></div>


</body>
</html>