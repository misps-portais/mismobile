﻿<?php

	$acao = $_GET['acao'];
	$turma = $_GET['turma'];
	
	$arr = array();
		
	$conn = odbc_connect("MISPG", "", "");

	switch($acao){
	case 'atualiza':
		$matricula = $_GET['matricula'];
		
		$sql = "SELECT eq_mes as mes, eq_atendidas, ag_atendidas, eq_tmo, ag_tmo, eq_faltas,ag_faltas,eq_nota,ag_nota 
		   FROM (SELECT                                                         
				   date_part('month' , data) as eq_mes, 
				   SUM(atd)/(SELECT count( q1.matricula) 
				   FROM (
						   SELECT DISTINCT tbl_produtividade_agente_dia_2013.matricula FROM tbl_produtividade_agente_dia_2013
						   INNER JOIN tbl_turmas_escola
								   ON tbl_turmas_escola.matricula = tbl_produtividade_agente_dia_2013.matricula
						   INNER JOIN tbl_ccm7_hierarquia
								   ON tbl_ccm7_hierarquia.cod_re_rh = tbl_produtividade_agente_dia_2013.matricula
						   WHERE        tbl_turmas_escola.turma = '$turma'
				   ) as q1) as eq_atendidas,                
				   CASE WHEN SUM(atd) > 0
						   THEN SUM(atd*tmo)/SUM(atd)
				   ELSE 0 END as eq_tmo, 
				   CASE WHEN SUM(faltas) IS NOT NULL
				   THEN SUM(faltas)
				   ELSE 0 END as eq_faltas, 
				   CASE WHEN SUM(qtd_monitorias) > 0 
						   THEN ROUND(sum(qtd_monitorias*nota)/sum(qtd_monitorias),2)
				   ELSE 0 END as eq_nota
				   FROM tbl_produtividade_agente_dia_2013
				   INNER JOIN tbl_turmas_escola
						   ON tbl_turmas_escola.matricula = tbl_produtividade_agente_dia_2013.matricula
				   INNER JOIN tbl_ccm7_hierarquia
						   ON tbl_ccm7_hierarquia.cod_re_rh = tbl_produtividade_agente_dia_2013.matricula
				   WHERE         tbl_turmas_escola.turma = '$turma' 
				   GROUP BY eq_mes
				   ORDER BY eq_mes) as query1 --QUERY QUE RETORNA INFORMACOES DA EQUIPE
		   LEFT JOIN
				(SELECT                 
				   date_part('month' , data) as ag_mes, 
				   SUM(atd) as ag_atendidas,                 
				   CASE WHEN SUM(atd) > 0
						   THEN SUM(atd*tmo)/SUM(atd)
				   ELSE 0 END as ag_tmo, 
				   CASE WHEN SUM(faltas) IS NOT NULL
				   THEN SUM(faltas)
				   ELSE 0 END as ag_faltas, 
				   CASE WHEN SUM(qtd_monitorias) > 0 
						   THEN ROUND(sum(qtd_monitorias*nota)/sum(qtd_monitorias),2)
				   ELSE 0 END as ag_nota
				   FROM tbl_produtividade_agente_dia_2013
				   INNER JOIN tbl_turmas_escola
						   ON tbl_turmas_escola.matricula = tbl_produtividade_agente_dia_2013.matricula
				   INNER JOIN tbl_ccm7_hierarquia
						   ON tbl_ccm7_hierarquia.cod_re_rh = tbl_produtividade_agente_dia_2013.matricula
				   WHERE  tbl_produtividade_agente_dia_2013.data >= tbl_turmas_escola.data_inicio + interval '0 month' AND 
						   tbl_produtividade_agente_dia_2013.data < tbl_turmas_escola.data_inicio + interval '6 month' and 
						   tbl_ccm7_hierarquia.cod_re_rh = '$matricula'
				   GROUP BY ag_mes
				   ORDER BY ag_mes) as query2 --QUERY QUE RETORNA INFORMACOES DO AGENTE NO PERIODO DE 180 DIAS
		   ON query2.ag_mes = query1.eq_mes
		   WHERE eq_atendidas <> 0

		   ";
		   
		$rs = odbc_exec ($conn,$sql);
			
		$mes = array("","Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro" , "Novembro", "Dezembro");
		
		$i = 0;
		
		while (odbc_fetch_row($rs))
		{
			$series_str[$i] = (int)odbc_result($rs,'eq_atendidas');
			$series_ag[$i] = (int)odbc_result($rs,'ag_atendidas');
			$tmo_str[$i] = (int)odbc_result($rs,'eq_tmo');
			$tmo_ag[$i] = (int)odbc_result($rs,'ag_tmo');
			$faltas_str[$i] = (int)odbc_result($rs,'eq_faltas');
			$faltas_ag[$i] = (int)odbc_result($rs,'ag_faltas');
			$qualidade_str[$i] = (int)odbc_result($rs,'eq_nota');
			$qualidade_ag[$i] = (float)odbc_result($rs,'ag_nota');
			
			$xaxis[$i] = $mes[odbc_result($rs,'mes')];
			$i++;
		}
		
		$arr[0] = $xaxis;
		$arr[1] = $series_str;
		$arr[2] = $series_ag;
		$arr[3] = $tmo_str;
		$arr[4] = $tmo_ag;
		$arr[5] = $faltas_str;
		$arr[6] = $faltas_ag;
		$arr[7] = $qualidade_str;
		$arr[8] = $qualidade_ag;
		
		echo json_encode($arr);
	break;
	case 'agentes':
			$sql = "
			SELECT DISTINCT nome_ronda as nome,cod_re_rh as matr
			FROM tbl_produtividade_agente_dia_2013
			INNER JOIN tbl_turmas_escola
				ON tbl_turmas_escola.matricula = tbl_produtividade_agente_dia_2013.matricula
			INNER JOIN tbl_ccm7_hierarquia
				ON tbl_ccm7_hierarquia.cod_re_rh = tbl_produtividade_agente_dia_2013.matricula
			WHERE tbl_turmas_escola.turma = '$turma'
			ORDER BY nome";
			$rs = odbc_exec($conn,$sql);
			
			$nome = 'turma';
				echo "<select id='agentes' onchange='gra(".chr(34).$turma.chr(34).",this.options[selectedIndex].value);'>";
				echo "<option value='%'>Selecione um operador</option>";
					while(odbc_fetch_row($rs)){
						$nome = odbc_result($rs,"nome");
						$matr = odbc_result($rs,"matr");
						echo "<option value='$matr'>$nome</option>";
					}
			echo "</select>";
	break;
}
if(isset($conn)){
odbc_close($conn);
};
?>