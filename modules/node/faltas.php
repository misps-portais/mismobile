<html>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />	
	<head>
		<link rel="shortcut icon" href="http://172.23.14.175/drupal7/misc/favicon.ico" type="image/vnd.microsoft.icon">
		<title>Relat�rio de Faltas | MIS Atendimento</title>
		<style type="text/css" media="all">
		@import url("http://172.23.14.175/drupal7/modules/system/system.base.css?mi9jpt");
		@import url("http://172.23.14.175/drupal7/modules/system/system.menus.css?mi9jpt");
		@import url("http://172.23.14.175/drupal7/modules/system/system.messages.css?mi9jpt");
		@import url("http://172.23.14.175/drupal7/modules/system/system.theme.css?mi9jpt");
		</style>
		<style type="text/css" media="all">
		@import url("http://172.23.14.175/drupal7/misc/ui/jquery.ui.core.css?mi9jpt");
		@import url("http://172.23.14.175/drupal7/misc/ui/jquery.ui.theme.css?mi9jpt");
		@import url("http://172.23.14.175/drupal7/modules/overlay/overlay-parent.css?mi9jpt");
		@import url("http://172.23.14.175/drupal7/modules/contextual/contextual.css?mi9jpt");
		</style>
		<style type="text/css" media="all">
		@import url("http://172.23.14.175/drupal7/themes/skeletontheme/css/skeleton.css?mi9jpt");
		@import url("http://172.23.14.175/drupal7/themes/skeletontheme/css/style.css?mi9jpt");
		@import url("http://172.23.14.175/drupal7/themes/skeletontheme/css/layout.css?mi9jpt");
		@import url("http://172.23.14.175/drupal7/themes/skeletontheme/color/colors.css?mi9jpt");
		</style>
		<!-- <link href="css/960.css" rel="stylesheet" media="screen" /> -->
		<link href="../css/defaultTheme.css" rel="stylesheet" media="screen" />
		<link href="css/myTheme.css" rel="stylesheet" media="screen" />
		<script src="../jquery.min.js"></script>
		<script src="../jquery.fixedheadertable.js"></script>
		<script src="demo.js"></script>
		<script type="text/javascript">
			function filter (phrase, _id, cellNr){
			var suche = phrase.value.toLowerCase();
			var table = document.getElementById(_id);
			var ele;
			for (var r = 0; r < table.rows.length; r++){
			ele = table.rows[r].cells[cellNr].innerHTML.replace(/<[^>]+>/g,"");
			if (ele.toLowerCase().indexOf(suche)>=0 )
			table.rows[r].style.display = '';
			else table.rows[r].style.display = 'none';
			}
			}
		</script>
		<script type="text/javascript">
			function escalafalta(escala, falta){
				document.getElementById('ESCALADO').innerHTML = escala;
				document.getElementById('FALTAS').innerHTML = falta;
			}
		</script>
	</head>
	<body>
		<div id="wrap">
			   <div class="container"> 

				<!-- #header -->
				<div id="header" class="sixteen columns clearfix" >
					<div class="inner">

						<a href="/drupal7/" title="In�cio" rel="home" id="logo">
						<img src="http://172.23.14.175/drupal7/themes/skeletontheme/logo.png" alt="In�cio">
						</a>

						<div id="name-and-slogan">

						<div id="site-name">
						<a href="/drupal7/" title="In�cio" rel="home">MIS Atendimento</a>
						</div>

						</div>
					</div><!-- /#header -->

					<!-- #navigation -->
					<div id="navigation" class="sixteen columns clearfix" style="padding-top:10px;">

						<div class="menu-header">
						<h2 class="element-invisible">Main menu</h2><ul id="main-menu-links" class="menu clearfix"><li class="menu-218 first last active"><a href="/drupal7/" class="active">Home</a></li>
						</ul>                        </div>

					</div><!-- /#navigation -->

					<div id="breadcrumb"><h2 class="element-invisible">You are here</h2><div class="breadcrumb"><a href="http://172.23.14.175/drupal7/">In�cio</a><span class="sep">-></span>Relat�rio de Faltas</div></div>

					<div id="main">



						<h1 class="title" id="page-title">
						Relat�rio de Faltas                </h1>

						<div class="region region-content">
						
							<div id="block-system-main" class="block block-system">


							<div class="content">
							<div id="node-1" class="node node-page clearfix" about="/drupal7/node/1" typeof="foaf:Document">


							<div class="content clearfix">
							<div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even" property="content:encoded">

							</div></div>  </div>

							</div>
							</div>
							</div>
							</div>

						</div>

					</div>
					 
					<table style='font-size: 10px;width:100%;'>
						<th>Login</th>
						<th><input name="filt" type="text" onKeyUp="filter(this, 'tabela', '0')"></th>
						<th>Nome</th>
						<th><input name="filt" type="text" onKeyUp="filter(this, 'tabela', '1')"></th>
					<th>Opera��o</th>
					<th>
					<select name="filt" type="text" onChange="filter(this, 'tabela', '15')">
						<option value="">TODOS</option>
						<option value="SA�DE - CONSOLIDADO">SA�DE - CONSOLIDADO</option>
						<option value="SAUDE APOIO">SAUDE APOIO</option>
						<option value="CT24H - APOIO SINISTRO">CT24H - APOIO SINISTRO</option>
						<option value="CT24H - APOIO ADM">CT24H - APOIO ADM</option>
						<option value="UNID - AMETISTA_CONSOL">UNID - AMETISTA_CONSOL</option>
						<option value="UNID - RUBI_CONSOLIDADO">UNID - RUBI_CONSOLIDADO</option>
						<option value="CENTRAL - VIDA E PREVID�NCIA - CONSOLIDADO">CENTRAL - VIDA E PREVID�NCIA - CONSOLIDADO</option>
						<option value="UNID - CHAT">UNID - CHAT</option>
						<option value="CT24H - NOVOS">CT24H - NOVOS</option>
						<option value="C. O. APOIO">C. O. APOIO</option>
						<option value="UNID - CRISTAL_CONSOLIDADO">UNID - CRISTAL_CONSOLIDADO</option>
						<option value="CT24H - SAC">CT24H - SAC</option>
						<option value="CT24H - AVISO DE SINISTRO">CT24H - AVISO DE SINISTRO</option>
						<option value="CT24H - CONSOLIDADO">CT24H - CONSOLIDADO</option>
						<option value="CT24H - APOIO OP">CT24H - APOIO OP</option>
						<option value="C.O. - CONSOLIDADO">C.O. - CONSOLIDADO</option>
						<option value="CT24H - SINISTRO">CT24H - SINISTRO</option>
						<option value="CT24H - CHAT SINISTRO">CT24H - CHAT SINISTRO</option>
						<option value="UNID - ESMERALDA_CONSOL">UNID - ESMERALDA_CONSOL</option>
						<option value="CT24H - CHAT">CT24H - CHAT</option>
						<option value="C.O. - SAPS">C.O. - SAPS</option>
						<option value="CT24H - AZUL">CT24H - AZUL</option>
						<option value="CT24H - MADRUGADA">CT24H - MADRUGADA</option>
						<option value="CT24H - VIDROS">CT24H - VIDROS</option>
						<option value="CT24H - 6x1 AZUL">CT24H - 6x1 AZUL</option>
						<option value="CT24H - 6x1 NOVOS">CT24H - 6x1 NOVOS</option>
						<option value="C.O. - COMPLEMENTO">C.O. - COMPLEMENTO</option>
						<option value="CT24H - 6X1 VIDROS">CT24H - 6X1 VIDROS</option>
						<option value="CT24H - 6x1 AVISO DE SINISTRO">CT24H - 6x1 AVISO DE SINISTRO</option>
						<option value="OR�AMENTO - APOIO">OR�AMENTO - APOIO</option>
						<option value="COBRAN�A">COBRAN�A</option>
						<option value="CART�O CORRETOR">CART�O CORRETOR</option>
						<option value="CART�O AN�LISE DE RISCO">CART�O AN�LISE DE RISCO</option>
						<option value="CART�O RETEN��O">CART�O RETEN��O</option>
						<option value="CART�O CLIENTE">CART�O CLIENTE</option>
						<option value="CART�O SAC">CART�O SAC</option>
						<option value="COBRAN�A APOIO">COBRAN�A APOIO</option>
						<option value="HD APOIO">HD APOIO</option>
						<option value="HDC CONSOLIDADO">HDC CONSOLIDADO</option>
						<option value="HDC CHAT">HDC CHAT</option>
						<option value="PORTO SEG">PORTO SEG</option>
						<option value="CT24HS PAT ITA�">CT24HS PAT ITA�</option>
						<option value="CT24HS PAT APOIO">CT24HS PAT APOIO</option>
						<option value="CT24HS PAT AZUL SEGUROS">CT24HS PAT AZUL SEGUROS</option>
						<option value="CT24HS PAT PORTO SEGURO">CT24HS PAT PORTO SEGURO</option>
						<option value="PAT CENTRAL DE APOIO">PAT CENTRAL DE APOIO</option>
						<option value="PAT SA�DE CONSOLIDADO">PAT SA�DE CONSOLIDADO</option>
					</select>
					</th>

					<table>
					
					<table style='font-size: 12px;width:100%;'>
					<tr>
						<th>Total escalado <label class="description"  id="ESCALADO">0</label></th>
						<th>Total de Faltas <label class="description" id="FALTAS">0 </label></th>
					</tr>
					<table>
					
					<?php
					$hoje = date('m/d/Y');
					$faltas = 0;
					$escala = 0;
					$conn=odbc_connect('CCM7','ccmsys','ccmsysadmin');
					$sql="Exec CUSTOM_USP_CCM_RELATORIO_AGENTES_PORTO '{$hoje}', '{$hoje}' , 1";
					$rs=odbc_exec($conn,$sql);
					echo "<table class='fancyTable' id='myTable02' style='font-size: 10px;'><tr>";
					echo "<thead style='cursor: default;'><th>Login</th>";
					echo "<th >Operador</th>";
					echo "<th>Data</th>";
					echo "<th>ABS</th>";
					echo "<th>Atraso</th>";
					echo "<th>Carga hor�ria</th>";
					echo "<th >Login realizado</th>";
					echo "<th>Login escalado</th>";
					echo "<th>Logout realizado</th>";
					echo "<th>Logout escalado</th>";
					echo "<th>Ader�ncia</th>";
					echo "<th style='padding-right:300px;'>Tempo Logado</th>";
					echo "<th style='display:none;'>Falta</th>";
					echo "<th style='display:none;'>Supervisor</th>";
					echo "<th style='display:none;'>Coordenador</th>";
					echo "<th style='display:none;'>Opera��o</th></thead><tbody id='tabela'>";

					while (odbc_fetch_row($rs))
					{
					$logid=str_replace(",00","",str_replace(".","",odbc_result($rs,"COD_LOGIN")));
					$op=odbc_result($rs,"NOME_OPERADOR");
					$sup=odbc_result($rs,"NOME_SUPERVISOR");
					$cord=odbc_result($rs,"NOME_CORDENADOR");
					$oper=odbc_result($rs,"OPERACAO");
					$dt= str_replace("00:00:00","",odbc_result($rs,"DATA"));
					$abs=odbc_result($rs,"ABSENTEISMO");
					$atr=odbc_result($rs,"ATRASO");
					$carga=odbc_result($rs,"CARGA_HORARIA_ESCALADO");
					$li=odbc_result($rs,"LOGIN");
					$lie=odbc_result($rs,"LOGIN_ESCALA");
					$lo=odbc_result($rs,"LOGOUT");
					$fal=odbc_result($rs,"FALTA");
					$loe=odbc_result($rs,"LOGOUT_ESCALA");
					$ade=odbc_result($rs,"ADERENCIA_LOGIN_LOGOUT");
					$temp=odbc_result($rs,"TEMPO_LOGADO");

					if($lie <> "")
						$escala += 1;
					$faltas = $faltas + $fal;
					echo "</tr><td>$logid</td>";
					echo "<td>$op</td>";
					echo "<td>$dt</td>";
					echo "<td>$abs%</td>";
					echo "<td>$atr</td>";
					echo "<td>$carga</td>";
					echo "<td>$li</td>";
					echo "<td>$lie</td>";
					echo "<td>$lo</td>";
					echo "<td>$loe</td>";
					echo "<td>$ade</td>";
					echo "<td>$temp</td>";
					echo "<td style='display:none;'>$fal</td>";
					echo "<td style='display:none;'>$sup</td>";
					echo "<td style='display:none;'>$cord</td>";
					echo "<td style='display:none;'>$oper</td>";
					}
					odbc_close($conn);
					echo "</tr></tbody></table>";
					echo "<script>escalafalta('$escala','$faltas');</script>";
					?>

					<div id="footer" class="sixteen columns clearfix">

						<div class="region region-footer">
							<div id="block-system-powered-by" class="block block-system contextual-links-region">

							<div class="contextual-links-wrapper contextual-links-processed"><a class="contextual-links-trigger" href="http://172.23.14.175/drupal7/node/1#">Configure</a><ul class="contextual-links"><li class="block-configure first last"><a href="http://172.23.14.175/drupal7/admin/structure/block/manage/system/powered-by/configure?destination=node/1">Configurar bloco</a></li>
							</ul></div>
							<div class="content">
							<span>Powered by <a href="http://drupal.org/">Drupal</a></span>  </div>
							</div>
						</div>

						<div id="credits">2012 MIS - Desenvolvido com <a href="http://www.drupal.org/" target="_blank">Drupal</a><br>
						Ported to Drupal for the Open Source Community by <a href="http://www.drupalizing.com/" target="_blank">Drupalizing</a>, a Project of <a href="http://www.morethanthemes.com/" target="_blank">More than (just) Themes</a>. Original design by <a href="http://www.simplethemes.com/" target="_blank">Simple Themes</a>.
						</div>

					</div>
				</div>
			 </div> <!-- /#wrap -->  

		</div>
	</body>
</html>