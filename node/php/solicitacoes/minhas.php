<div class="top">
	<div class="navigation">
		<?php require(SOLIC_PATH.'/navigation.php');?>
		<div class="clearfix"></div>
	</div>
</div><!-- / fim content-top -->

<div class="bottom">
	<div class="inner">
		<div class="solicitacoes">


<?php 


$usuario = $user->uid;

if(in_array('administrator',$user->roles)){
 	$query = "
		SELECT solic.id, solic.status, solic.tipo, tipo.nome AS tipo_nome, solic.solicitante, solic.data_hora_solicitacao, solic.responsavel
		FROM solicitacoes.tbl_solicitacoes AS solic
		LEFT OUTER JOIN solicitacoes.tbl_tipos AS tipo ON solic.tipo=tipo.id
		ORDER By solic.id DESC";
}
else{
	$query = "
		SELECT solic.id, solic.status, solic.tipo, tipo.nome AS tipo_nome, solic.solicitante, solic.data_hora_solicitacao, solic.responsavel
		FROM solicitacoes.tbl_solicitacoes AS solic
		LEFT OUTER JOIN solicitacoes.tbl_tipos AS tipo ON solic.tipo=tipo.id
		WHERE solic.solicitante='$usuario'
		ORDER By solic.id DESC";
}


$exec = odbc_exec($conn,$query);
$total = odbc_num_rows($exec);

if($total == 0){
	echo 'Não existem solicitações';

}
else{
?>

			<div class="todos_departamentos">
				<table>
					<thead>
						<tr>
							
							<th style="width:15%;">NÚMERO</th>
							<th style="width:15%;">STATUS</th>
							<th style="width:25%;">TIPO</th>
							<th>RESPONSÁVEL</th>
							<th style="width:15%;">ABERTURA</th>
						</tr>
					</thead>
					<tbody>

						<?php 
						
						while($resultado = odbc_fetch_array($exec)){
							switch($resultado['status']){
								case 0:	$status = 'Fechado'; break;
								case 1:	$status = 'Aberto';	break;
								case 2: $status = 'Em atendimento';	break;
							}
						?>
						<tr>
							
							<td><a href="#<?php echo $resultado['id'];?>"><?php echo $resultado['id'];?></a></td>
							<td><?php echo $status;?></td>
							<td><?php echo utf8_encode($resultado['tipo_nome']);?></td>
							<td><?php echo $resultado['responsavel'] ;?></td>
							<td><?php echo date('d-m-Y H:i',$resultado['data_hora_solicitacao']);?></td>
						</tr>
						<?php } ?>

					</tbody>
				</table>
			</div>
<?php } ?>

		</div>
	</div><!-- / fim inner -->
</div><!-- / fim content-bottom -->