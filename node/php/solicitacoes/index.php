<?php

//
define('ENVIRONMENT', 'test');
define('APP_PATH', 'C:\xampp\htdocs\mismobile');
define('NODE_PATH', APP_PATH.'\node');
define('SOLIC_PATH', NODE_PATH.'\php\solicitacoes');
ini_set('date.timezone','America/Sao_Paulo');

//
require(NODE_PATH.'\php\fs_functions.php');
(empty($_GET['acao'])) ? $rota = 404 : $rota = $_GET['acao'];


//
$conn = odbc_connect('MISPG','','');


//
if(ENVIRONMENT == 'develop'){
    error_reporting(E_ALL);
    session_start();
    header('Content-Type: text/html; charset=utf-8');
    header("Last-Modified: ".date("D, d M Y H:i:s")." GMT");
}

if(ENVIRONMENT == 'test'){
    error_reporting(0);
    global $user;

}

if(ENVIRONMENT == 'production'){
    error_reporting(0);
    global $user;
}

/**
 *   Rotas para as páginas do sistema
 *
 */
switch($rota){
    case 'home':
        require(SOLIC_PATH.'\home.php');
        break;

    case 'nova': 
        require(SOLIC_PATH.'\nova.php');
        break;

    case 'minhas': 
        require(SOLIC_PATH.'\minhas.php');
        break;

    case 'editar': 
        require(SOLIC_PATH.'\editar.php');
        break;

    case 'acompanhamento': 
        require(SOLIC_PATH.'\acompanhamento.php');
        break;



	default:
        header("HTTP/1.0 404 Not Found");
        echo 'Erro 404: Página não encontrada';
        break;
}

/**
 *   Acaba com o banco!  #ohnooo
 */
odbc_close($conn);

?>