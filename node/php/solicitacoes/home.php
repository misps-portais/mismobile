<div class="interno">
	<div class="menu-secundario">
		<?php require(SOLIC_PATH.'/navigation.php');?>
	</div>

	<div class="solicitacoes">
		<div class="alert-box"><?php echo exibe_alerta(); destroi_alerta(); ?></div>
		<?php 


	$usuario = $user->name;

	if(in_array('administrator',$user->roles) || in_array('planejamento',$user->roles)){
	 	$query = "
			SELECT solic.id, solic.status, solic.tipo, tipo.sla AS tipo_sla, tipo.nome AS tipo_nome, solic.solicitante, solic.datahora_solicitacao, to_char(to_timestamp(solic.datahora_solicitacao),'DD/MM/YYYY HH24:MI') AS datahora_solicitacao_human, solic.responsavel
			FROM solicitacoes.tbl_solicitacoes AS solic
			LEFT OUTER JOIN solicitacoes.tbl_tipos AS tipo ON solic.tipo=tipo.id
			WHERE solic.status <> 3
			ORDER By solic.id DESC;";
	}
	else{
		$query = "
			SELECT solic.id, solic.status, solic.tipo, tipo.sla AS tipo_sla, tipo.nome AS tipo_nome, solic.solicitante, solic.datahora_solicitacao, to_char(to_timestamp(solic.datahora_solicitacao),'DD/MM/YYYY HH24:MI') AS datahora_solicitacao_human, solic.responsavel
			FROM solicitacoes.tbl_solicitacoes AS solic
			LEFT OUTER JOIN solicitacoes.tbl_tipos AS tipo ON solic.tipo=tipo.id
			WHERE solic.status <> 3 AND solic.solicitante='$usuario'
			ORDER By solic.id DESC;";
	}


	$exec = odbc_exec($conn,$query);
	$total = odbc_num_rows($exec);

	if($total == 0){
		echo 'Não existem solicitações abertas ou em andamento.';

	}
	else{
	?>

		<table>
			<thead>
				<tr>
					
					<th style="width:15%;">NÚMERO</th>
					<th style="width:15%;">STATUS</th>
					<th style="width:25%;">TIPO</th>
					<th>RESPONSÁVEL</th>
					<th style="width:15%;">DATA ABERTURA</th>
					<th style="width:15%;">DATA FINAL</th>
				</tr>
			</thead>
			<tbody>

				<?php 
				
				while($resultado = odbc_fetch_array($exec)){
					switch($resultado['status']){
						case 0:	$status = 'Nova'; break;
						case 1:	$status = 'Aberta';	break;
						case 2: $status = 'Ag. Finalização'; break;
						case 3: $status = 'Finalizado'; break;
					}
				?>
				<tr>
					
					<td><a href="solicitacoes?acao=editar&sid=<?php echo $resultado['id'];?>"><?php echo $resultado['id'];?></a></td>
					<td><?php echo $status;?></td>
					<td><?php echo utf8_encode($resultado['tipo_nome']);?></td>
					<td><?php echo (empty($resultado['responsavel'])) ? $resultado['solicitante'] : $resultado['responsavel'] ;?></td>
					<td><?php echo $resultado['datahora_solicitacao_human'];?></td>
					<td><?php echo data_final_sla($resultado['tipo_sla']/3600,date('d-m-Y h:i:s',$resultado['datahora_solicitacao']));?></td>
				</tr>
				<?php } ?>

			</tbody>
		</table>
	<?php } ?>
	</div><!-- / fim  -->
</div>