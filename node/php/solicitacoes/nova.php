<?php 


if($_SERVER['REQUEST_METHOD'] == 'POST'){

	// Parâmetros iniciais
	$validacoes = true;
	$exec_query = true;
	$solicita_id = date('Ymdhis');
	//$solicita_id = uniqid();
	$timestamp = time();
	$maquina = (empty($_SERVER["HTTP_X_FORWARDED_FOR"])) ? $_SERVER['REMOTE_ADDR'] : $_SERVER["HTTP_X_FORWARDED_FOR"];
	$usuario = $user->name;


	// Previne a possibilidade de enviar 2x o mesmo formulario com os mesmos dados
	if($_POST[session_id()] != $_SESSION[session_id()]){
		gera_alerta('<div class="messages error">001# '.mensagem_erro('gen',2).'</div>');
		$validacoes = false;
	}

	//
	if(empty($_POST['form_id'])){
		gera_alerta('<div class="messages error">002# '.mensagem_erro('gen',2).'</div>');
		$validacoes = false;
	}

	// Sucesso nas validações, continue!
	if($validacoes == true){

		//
		switch($_POST['form_id']){

			case 'tcdf':

				//
				$tipo = sanitiza('post','tipo');
				$operacao = sanitiza('post','operacao');
				$matricula1 = sanitiza('post','matricula1');
				$matricula2 = sanitiza('post','matricula2');
				$nome1 = sanitiza('post','nome1');
				$nome2 = sanitiza('post','nome2');
				$data1 = sanitiza('post','data1');
				$data2 = sanitiza('post','data2');

				//  id, status, tipo, titulo, descricao, solicitante, datahora_solicitacao, responsavel, datahora_interacao
				// (solicitacao, matricula1, matricula2, nome_atendimento1, nome_atendimento2, data1, data2, operacao, observacoes)
				$query = "
					INSERT INTO solicitacoes.tbl_solicitacoes VALUES ('$solicita_id',0,'$tipo',NULL,NULL,'$usuario','$timestamp',NULL,NULL);
					INSERT INTO solicitacoes.tbl_dados_8 VALUES ('$solicita_id','$matricula1','$matricula2','$nome1','$nome2','$data1','$data2','$operacao')";
				$exec = odbc_exec($conn,$query);

			break;

			case 'td':

				$tipo = sanitiza('post','tipo');
				$operacao = sanitiza('post','operacao');
				$matricula = sanitiza('post','matricula');
				$nome = sanitiza('post','nome');
				$horario1 = sanitiza('post','horario1');
				$jornada1 = sanitiza('post','jornada1');
				$horario2 = sanitiza('post','horario2');
				$jornada2 = sanitiza('post','jornada2');
				$data = sanitiza('post','data');


				// id, status, tipo, usuario, data_hora, ip
				// solicitacao, matricula, nome_atendimento, horario_atual, jornada_atual, novo_horario, nova_jornada, data_execucao, operacao, observacoes
				$query = "
					INSERT INTO solicitacoes.tbl_solicitacoes VALUES ('$solicita_id',0,'$tipo',NULL,NULL,'$usuario','$timestamp',NULL,NULL);
					INSERT INTO solicitacoes.tbl_dados_2 VALUES ('$solicita_id','$matricula','$nome','$horario1','$jornada1','$horario2','$jornada2','$data','$operacao',NULL)";
				$exec = odbc_exec($conn,$query);

			break;

			case 'tdh':

				$tipo = sanitiza('post','tipo');
				$operacao = sanitiza('post','operacao');
				$matricula = sanitiza('post','matricula');
				$nome = sanitiza('post','nome');
				$horario1 = sanitiza('post','horario1');
				$jornada1 = sanitiza('post','jornada1');
				$horario2 = sanitiza('post','horario2');
				$jornada2 = sanitiza('post','jornada2');
				$data = sanitiza('post','data');


				// id, status, tipo, usuario, data_hora, ip
				// solicitacao, matricula, nome_atendimento, horario_atual, jornada_atual, novo_horario, nova_jornada, data_execucao, operacao, observacoes
				$query = "
					INSERT INTO solicitacoes.tbl_solicitacoes VALUES ('$solicita_id',0,'$tipo',NULL,NULL,'$usuario','$timestamp',NULL,NULL);
					INSERT INTO solicitacoes.tbl_dados_3 VALUES ('$solicita_id','$matricula','$nome','$horario1','$jornada1','$horario2','$jornada2','$data','$operacao',NULL)";
				$exec = odbc_exec($conn,$query);

			break;

			case 'ts':
				
				$tipo = sanitiza('post','tipo');
				$status = sanitiza('post','status');
				$operacao = sanitiza('post','operacao');
				$matricula = sanitiza('post','matricula');
				$nome = sanitiza('post','nome');
				$data1 = sanitiza('post','data1');
				$data2 = sanitiza('post','data2');


				// id, status, tipo, usuario, data_hora, ip
				// solicitacao, matricula, nome_atendimento, status, data_inicio, data_retorno, operacao, observacoes
				$query = "
					INSERT INTO solicitacoes.tbl_solicitacoes VALUES ('$solicita_id',0,'$tipo',NULL,NULL,'$usuario','$timestamp',NULL,NULL);
					INSERT INTO solicitacoes.tbl_dados_5 VALUES ('$solicita_id','$matricula','$nome','$status','$data1','$data2','$operacao',NULL)";
				$exec = odbc_exec($conn,$query);

			break;

			case 'tlna':

				$tipo = sanitiza('post','tipo');
				$status = sanitiza('post','status');
				$operacao = sanitiza('post','operacao');
				$matricula = sanitiza('post','matricula');
				$nome1 = sanitiza('post','nome1');
				$nome2 = sanitiza('post','nome2');
				$login1 = sanitiza('post','login1');
				$login2 = sanitiza('post','login2');
				$supervisor = sanitiza('post','supervisor');
				$data = sanitiza('post','data');


				// id, status, tipo, usuario, data_hora, ip
				// solicitacao, matricula, nome_atendimento1, nome_atendimento2, login1, login2, supervisor, data_execucao, operacao, observacoes
				$query = "
					INSERT INTO solicitacoes.tbl_solicitacoes VALUES ('$solicita_id',0,'$tipo',NULL,NULL,'$usuario','$timestamp',NULL,NULL);
					INSERT INTO solicitacoes.tbl_dados_14 VALUES ('$solicita_id','$matricula','$nome1','$nome2','$login1','$login2','$supervisor','$data','$operacao',NULL)";
				$exec = odbc_exec($conn,$query);

			
			break;

			case 'lee':
				
				$tipo = sanitiza('post','tipo');
				$operacao = sanitiza('post','operacao');
				$matricula = sanitiza('post','matricula');
				$nome = sanitiza('post','nome');
				$evento = sanitiza('post','evento');
				$intervalo = sanitiza('post','de').'-'.sanitiza('post','para');
				$data = sanitiza('post','data');


								// id, status, tipo, usuario, data_hora, ip
				// solicitacao, matricula, nome_atendimento, data_evento, hora_intervalo, descricao_evento, operacao, observacoes
				$query = "
					INSERT INTO solicitacoes.tbl_solicitacoes VALUES ('$solicita_id',0,'$tipo',NULL,NULL,'$usuario','$timestamp',NULL,NULL);
					INSERT INTO solicitacoes.tbl_dados_7 VALUES ('$solicita_id','$matricula','$nome','$data','$intervalo','$evento','$operacao',NULL)";
				$exec = odbc_exec($conn,$query);

			break;

			case 'thrq':

				$tipo = sanitiza('post','tipo');
				$operacao = sanitiza('post','operacao');
				$matricula = sanitiza('post','matricula');
				$nome = sanitiza('post','nome');
				$superv1 = sanitiza('post','superv1');
				$superv2 = sanitiza('post','superv2');
				$data = sanitiza('post','data');

				// solicitacao, matricula, nome_atendimento, nome_supervisor_anterior, nome_supervisor_atual, data_execucao, operacao, observacoes
				$query = "
					INSERT INTO solicitacoes.tbl_solicitacoes VALUES ('$solicita_id',0,'$tipo',NULL,NULL,'$usuario','$timestamp',NULL,NULL);
					INSERT INTO solicitacoes.tbl_dados_4 VALUES ('$solicita_id','$matricula','$nome','$superv1','$superv2','$data','$operacao',NULL)";
				$exec = odbc_exec($conn,$query);

			break;

			case 'rsccm7':

				$tipo = sanitiza('post','tipo');
				$operacao = sanitiza('post','operacao');
				$matricula = sanitiza('post','matricula');
				$nome = sanitiza('post','nome');
				$apelido = sanitiza('post','apelido');
				$data = sanitiza('post','data');

				// solicitacao, matricula, nome_atendimento, apelido, data_execucao, operacao, observacoes
				$query = "
					INSERT INTO solicitacoes.tbl_solicitacoes VALUES ('$solicita_id',0,'$tipo',NULL,NULL,'$usuario','$timestamp',NULL,NULL);
					INSERT INTO solicitacoes.tbl_dados_12 VALUES ('$solicita_id','$matricula','$nome','$apelido','$data','$operacao',NULL)";
				$exec = odbc_exec($conn,$query);

			break;

			case 'tccus':

				$tipo = sanitiza('post','tipo');
				$operacao = sanitiza('post','operacao');
				$matricula = sanitiza('post','matricula');
				$nome = sanitiza('post','nome');
				$ccusto1 = sanitiza('post','ccusto1');
				$ccusto2 = sanitiza('post','ccusto2');
				$data = sanitiza('post','data');

				// solicitacao, matricula, nome_atendimento, centro_custo_anterior, centro_custo_atual, data_evento, operacao, observacoes
				$query = "
					INSERT INTO solicitacoes.tbl_solicitacoes VALUES ('$solicita_id',0,'$tipo',NULL,NULL,'$usuario','$timestamp',NULL,NULL);
					INSERT INTO solicitacoes.tbl_dados_11 VALUES ('$solicita_id','$matricula','$nome','$ccusto1','$ccusto2','$data','$operacao',NULL)";
				$exec = odbc_exec($conn,$query);

			break;


			case 'cnccm7':

				$tipo = sanitiza('post','tipo');
				$operacao = sanitiza('post','operacao');
				$dac = sanitiza('post','dac');
				$site = sanitiza('post','site');
				$pas = sanitiza('post','pas');
				$horario = sanitiza('post','horario');
				$feriado = sanitiza('post','feriado');

				// solicitacao, dac, site, trabalha_feriado, horario, pa_disponivel, operacao, observacoes
				$query = "
					INSERT INTO solicitacoes.tbl_solicitacoes VALUES ('$solicita_id',0,'$tipo',NULL,NULL,'$usuario','$timestamp',NULL,NULL);
					INSERT INTO solicitacoes.tbl_dados_10 VALUES ('$solicita_id','$dac','$site','$feriado','$horario','$pas','$operacao',NULL)";
				$exec = odbc_exec($conn,$query);

			break;


			case 'ina':

				$tipo = sanitiza('post','tipo');
				$operacao = sanitiza('post','operacao');
				$matricula = sanitiza('post','matricula');
				$nome = sanitiza('post','nome');
				$apelido = sanitiza('post','apelido');
				$avaya = sanitiza('post','avaya');
				$ccusto = sanitiza('post','ccusto');

				// id, status, tipo, usuario, data_hora, ip
				// solicitacao, matricula, nome, apelido, login_avaya, operacao, centro_custo, observacoes
				$query = "
					INSERT INTO solicitacoes.tbl_solicitacoes VALUES ('$solicita_id',0,'$tipo',NULL,NULL,'$usuario','$timestamp',NULL,NULL);
					INSERT INTO solicitacoes.tbl_dados_1 VALUES ('$solicita_id','$matricula','$nome','$apelido','$avaya','$operacao','$ccusto',NULL)";
				$exec = odbc_exec($conn,$query);

			break;

			case '':
			break;

			case '':
			break;


		}

		//
		if($exec == false){
			//salva_log(mensagem_erro('sys',1,$query));
			gera_alerta('<div class="formee-msg-error">005# '.mensagem_erro('gen',2).'</div>');
			echo $query;
		}
		else{
			gera_alerta('<div class="formee-msg-success">O dados foram salvos com sucesso!</div>');
			//header('Location: http://172.23.14.155/mismobile/documentos?acao=departamentos');
			//exit;
		}
	
	} // Validações

} // Send by POST










/**
 *
 */

$query = "SELECT * FROM skills.tbl_assunto_1 ORDER by assunto_1 ASC";
$exec = odbc_exec($conn,$query);

while($resultado = odbc_fetch_array($exec)){ 
	$operacoes .= '<option value="'.$resultado['id'].'">'.utf8_encode($resultado['assunto_1']).'</option>';
}

$query = "SELECT * FROM solicitacoes.tbl_status_agente ORDER by nome ASC";
$exec = odbc_exec($conn,$query);

while($resultado = odbc_fetch_array($exec)){
	$status .= '<option value="'.$resultado['id'].'">'.utf8_encode($resultado['nome']).'</option>';
}



?>

<div class="navegacao">
	<?php require(SOLIC_PATH.'/navigation.php');?>
</div>

<div class="solicitacoes">
	<div class="alert-box"><?php echo exibe_alerta(); destroi_alerta(); ?></div>

<?php

switch($_GET['s']){

case 'tcdf': // Troca casada de folgas
?>
		<h1>Troca de Folgas</h1>

		<form action="" method="POST" class="formee">
			<select name="operacao"> 
				<option value="null">Selecione a Operação</option>
				<option value="null"></option>
				<?php echo $operacoes; ?>
			</select>
			<br/><br/><br/>
			<fieldset>
				<legend>Agente 1</legend>
				<div class="grid-2-12" style=""><input type="text" name="matricula1" placeholder="Matrícula" maxlength="8" /></div>
				<div class="grid-8-12" style=""><input type="text" name="nome1" placeholder="Nome" /></div>
				<div class="grid-2-12" style="width:14.5%;"><input type="date" name="data1" maxlength="10" /></div>
			</fieldset>
			<fieldset>
				<legend>Agente 2</legend>
				<div class="grid-2-12" style=""><input type="text" name="matricula2" placeholder="Matrícula" maxlength="8" /></div>
				<div class="grid-8-12" style=""><input type="text" name="nome2" placeholder="Nome" /></div>
				<div class="grid-2-12" style="width:14.5%;"><input type="date" name="data2" maxlength="10" /></div>
			</fieldset>
			<input type="submit" value="Enviar" />
			<input type="hidden" name="<?php echo session_id(); ?>" value="<?php echo gera_sessao(session_id(),sha1(uniqid())); ?>" />
			<input type="hidden" name="form_id" value="tcdf" />
			<input type="hidden" name="tipo" value="8" />

		</form>
<?php 
break;
case 'td': // Troca de horário/dia
?>
		<h1>Troca de Horário (Dia)</h1>

		<form action="" method="POST" class="formee">
			<div class="grid-10-12" style="">
				<select name="operacao">
					<option value="null">Selecione a Operação</option>
					<option value="null"></option>
					<?php echo $operacoes; ?>
				</select>
			</div>

			<div class="grid-2-12" style=""><input type="text" name="matricula" placeholder="Matrícula" maxlength="8" /></div>
			<div class="grid-8-12" style=""><input type="text" name="nome" placeholder="Nome" /></div>
			
			<div class="grid-2-12" style=""><input type="text" name="horario1" placeholder="Horário Atual (Ex: 12:00)" maxlength="8" /></div>
			<div class="grid-2-12" style=""><input type="text" name="jornada1" placeholder="Jornada Atual (Ex: 6:20)" maxlength="8" /></div>

			<div class="grid-2-12" style=""><input type="text" name="horario2" placeholder="Novo Horário (Ex: 10:00)" maxlength="8" /></div>
			<div class="grid-2-12" style=""><input type="text" name="jornada2" placeholder="Nova Jornada (Ex: 8:12)" maxlength="8" /></div>
			<div class="grid-2-12" style=""><input type="date" name="data" maxlength="10" /></div>

			<div class="clearfix"></div>
			<input type="submit" value="Enviar" />
			<input type="hidden" name="<?php echo session_id(); ?>" value="<?php echo gera_sessao(session_id(),sha1(uniqid())); ?>" />
			<input type="hidden" name="form_id" value="td" />
			<input type="hidden" name="tipo" value="2" />
		</form>
<?php 
break;
case 'tdh': // Troca definitiva de horario
?>
		<h1>Troca Horário (Definitiva)</h1>

		<form action="" method="POST" class="formee">
			<div class="grid-10-12" style="">
				<select name="operacao">
					<option value="null">Selecione a Operação</option>
					<option value="null"></option>
					<?php echo $operacoes; ?>
				</select>
			</div>

			<div class="grid-2-12" style=""><input type="text" name="matricula" placeholder="Matrícula" maxlength="8" /></div>
			<div class="grid-8-12" style=""><input type="text" name="nome" placeholder="Nome" /></div>
			
			<div class="grid-2-12" style=""><input type="text" name="horario1" placeholder="Horário Atual (Ex: 12:00)" maxlength="8" /></div>
			<div class="grid-2-12" style=""><input type="text" name="jornada1" placeholder="Jornada Atual (Ex: 6:20)" maxlength="8" /></div>

			<div class="grid-2-12" style=""><input type="text" name="horario2" placeholder="Novo Horário (Ex: 10:00)" maxlength="8" /></div>
			<div class="grid-2-12" style=""><input type="text" name="jornada2" placeholder="Nova Jornada (Ex: 8:12)" maxlength="8" /></div>
			<div class="grid-2-12" style=""><input type="date" name="data" maxlength="10" /></div>

			<div class="clearfix"></div>
			<input type="submit" value="Enviar" />
			<input type="hidden" name="<?php echo session_id(); ?>" value="<?php echo gera_sessao(session_id(),sha1(uniqid())); ?>" />
			<input type="hidden" name="form_id" value="tdh" />
			<input type="hidden" name="tipo" value="3" />

		</form>
<?php 
break;
case 'ts': // Troca status
?>
		<h1>Troca de Status</h1>

		<form action="" method="POST" class="formee">
			<div class="grid-10-12" style="">
				<select name="operacao">
					<option value="null">Selecione a Operação</option>
					<option value="null"></option>
					<?php echo $operacoes; ?>
				</select>
			</div>

			<div class="grid-10-12" style="">
				<select name="status">
					<option value="null">Selecione o Status</option>
					<option value="null"></option>
					<?php echo $status; ?>
				</select>
			</div>

			<div class="grid-2-12" style=""><input type="text" name="matricula" placeholder="Matrícula" maxlength="8" /></div>
			<div class="grid-8-12" style=""><input type="text" name="nome" placeholder="Nome" /></div>
			
			<div class="grid-2-12" style=""><label>Saída</label><input type="date" name="data1" maxlength="10" /></div>
			<div class="grid-2-12" style=""><label>Retorno</label><input type="date" name="data2" maxlength="10" /></div>

			<div class="clearfix"></div>
			<input type="submit" value="Enviar" />
			<input type="hidden" name="<?php echo session_id(); ?>" value="<?php echo gera_sessao(session_id(),sha1(uniqid())); ?>" />
			<input type="hidden" name="form_id" value="ts" />
			<input type="hidden" name="tipo" value="5" />

		</form>
<?php 
break;
case 'tlna': // Troca de login/nome atendimento
?>
		<h1>Troca do Login / Nome de Atendimento</h1>

		<form action="" method="POST" class="formee">
			<fieldset>
				<select name="operacao" style="margin:0 0 10px 0;">
					<option value="null">Selecione a Operação</option>
					<option value="null"></option>
					<?php echo $operacoes; ?>
				</select>

				<div class="float-left" style="width:84%;"><input type="text" name="supervisor" placeholder="Supervisor" /></div>
				<div class="float-right" style=""><input type="date" name="data" maxlength="10" /></div>
				<div class="clearfix"></div>
			</fieldset>

			<fieldset>
				<legend>Atual</legend>
				<div class="grid-2-12" style=""><input type="text" name="matricula" placeholder="Matrícula" maxlength="8" /></div>
				<div class="grid-8-12" style=""><input type="text" name="nome1" placeholder="Nome atendimento" /></div>
				<div class="grid-2-12" style="width:14.5%;"><input type="text" name="login1" placeholder="Login" maxlength="8" /></div>
			</fieldset>

			<fieldset>
				<legend>Novo</legend>
				
				<div class="grid-10-12" style="width:81.5%;"><input type="text" name="nome2" placeholder="Nome" /></div>
				<div class="grid-2-12" style=""><input type="text" name="login2" placeholder="Login" maxlength="8" /></div>
			</fieldset>

			

			<div class="clearfix"></div>
			<input type="submit" value="Enviar" />
			<input type="hidden" name="<?php echo session_id(); ?>" value="<?php echo gera_sessao(session_id(),sha1(uniqid())); ?>" />
			<input type="hidden" name="form_id" value="tlna" />
			<input type="hidden" name="tipo" value="14" />
		</form>
<?php 
break;
case 'lee': // Lançamento de Eventos na Escala
?>
		<h1>Lançamento de Eventos na Escala</h1>

		<form action="" method="POST" class="formee">
			<div class="grid-10-12" style="">
				<select name="operacao">
					<option value="null">Selecione a Operação</option>
					<option value="null"></option>
					<?php echo $operacoes; ?>
				</select>
			</div>

			<div class="grid-2-12" style=""><input type="text" name="matricula" placeholder="Matrícula" maxlength="8" /></div>
			<div class="grid-8-12" style=""><input type="text" name="nome" placeholder="Nome" /></div>
			
			<div class="grid-8-12" style=""><input type="text" name="evento" placeholder="Evento" /></div>
			<div class="grid-2-12" style=""><input type="date" name="data" maxlength="10" /></div>
			<div class="grid-8-12" style=""><input type="time" name="de" /><input type="time" name="para" /></div>

			<div class="clearfix"></div>
			<input type="submit" value="Enviar" />
			<input type="hidden" name="<?php echo session_id(); ?>" value="<?php echo gera_sessao(session_id(),sha1(uniqid())); ?>" />
			<input type="hidden" name="form_id" value="lee" />
			<input type="hidden" name="tipo" value="7" />
		</form>
<?php 
break;
case 'thrq': // Troca de Hierarquia
?>
		<h1>Troca de Hierarquia</h1>

		<form action="" method="POST" class="formee">
			<div class="grid-10-12" style="">
				<select name="operacao">
					<option value="null">Selecione a Operação</option>
					<option value="null"></option>
					<?php echo $operacoes; ?>
				</select>
			</div>

			<div class="grid-2-12" style=""><input type="text" name="matricula" placeholder="Matrícula" maxlength="8" /></div>
			<div class="grid-8-12" style=""><input type="text" name="nome" placeholder="Nome" /></div>
			<div class="grid-8-12" style=""><input type="text" name="superv1" placeholder="Nome do Supervisor Anterior" /></div>
			<div class="grid-8-12" style=""><input type="text" name="superv2" placeholder="Nome do Supervisor Atual" /></div>
			<div class="grid-2-12" style=""><input type="date" name="data" maxlength="10" /></div>
			
			<div class="clearfix"></div>
			<input type="submit" value="Enviar" />
			<input type="hidden" name="<?php echo session_id(); ?>" value="<?php echo gera_sessao(session_id(),sha1(uniqid())); ?>" />
			<input type="hidden" name="form_id" value="thrq" />
			<input type="hidden" name="tipo" value="4" />
		</form>
<?php 
break;
case 'rsccm7': // Reset de Senha CCM7
?>
		<h1>Reset de Senha CCM7</h1>

		<form action="" method="POST" class="formee">
			<div class="grid-10-12" style="">
				<select name="operacao">
					<option value="null">Selecione a Operação</option>
					<option value="null"></option>
					<?php echo $operacoes; ?>
				</select>
			</div>

			<div class="grid-2-12" style=""><input type="text" name="matricula" placeholder="Matrícula" maxlength="8" /></div>
			<div class="grid-8-12" style=""><input type="text" name="nome" placeholder="Nome" /></div>
			<div class="grid-8-12" style=""><input type="text" name="apelido" placeholder="Apelido" /></div>
			<div class="grid-2-12" style=""><input type="date" name="data" maxlength="10" /></div>
			
			<div class="clearfix"></div>
			<input type="submit" value="Enviar" />
			<input type="hidden" name="<?php echo session_id(); ?>" value="<?php echo gera_sessao(session_id(),sha1(uniqid())); ?>" />
			<input type="hidden" name="form_id" value="rsccm7" />
			<input type="hidden" name="tipo" value="12" />
		</form>
<?php 
break;
case 'cnccm7': // Criação de Novas Operações CCM7
?>
		<h1>Criação de Novas Operações CCM7</h1>

		<form action="" method="POST" class="formee">
			<div class="grid-10-12" style="">
				<select name="operacao">
					<option value="null">Selecione a Operação</option>
					<option value="null"></option>
					<?php echo $operacoes; ?>
				</select>
			</div>

			<div class="grid-2-12" style=""><input type="text" name="dac" placeholder="Dac" maxlength="20" /></div>
			<div class="grid-2-12" style=""><input type="text" name="site" placeholder="Site" /></div>
			<div class="grid-2-12" style=""><input type="text" name="pas" placeholder="PAs" /></div>
			<div class="grid-2-12" style=""><input type="text" name="horario" placeholder="Horario (Ex: 00:00-00:00)" maxlength="15" /></div>
			<div class="grid-2-12" style=""><input type="date" name="data" maxlength="10" /></div>
			<div class="clearfix"></div>
			<div class="" style="width: 11%;">
				<ul>
					<li style=""><label class="float-left">Trabalha Feriado?</label> <input type="checkbox" name="feriado" value="1" class="float-right" /></li>
				</ul>
			</div>
			
			
			<div class="clearfix"></div>
			<input type="submit" value="Enviar" />
			<input type="hidden" name="<?php echo session_id(); ?>" value="<?php echo gera_sessao(session_id(),sha1(uniqid())); ?>" />
			<input type="hidden" name="form_id" value="cnccm7" />
			<input type="hidden" name="tipo" value="10" />
		</form>
<?php 
break;
case 'tccus': //
?>
		<h1>Troca de Centro de Custo</h1>

		<form action="" method="POST" class="formee">
			<div class="grid-10-12" style="">
				<select name="operacao">
					<option value="null">Selecione a Operação</option>
					<option value="null"></option>
					<?php echo $operacoes; ?>
				</select>
			</div>

			<div class="grid-2-12" style=""><input type="text" name="matricula" placeholder="Matrícula" maxlength="8" /></div>
			<div class="grid-8-12" style=""><input type="text" name="nome" placeholder="Nome" /></div>
			<div class="grid-2-12" style=""><input type="text" name="ccusto1" placeholder="Centro de Custo Anterior" /></div>
			<div class="grid-2-12" style=""><input type="text" name="ccusto2" placeholder="Centro de Custo Atual" /></div>
			<div class="grid-2-12" style=""><input type="date" name="data" maxlength="10" /></div>
			
			<div class="clearfix"></div>
			<input type="submit" value="Enviar" />
			<input type="hidden" name="<?php echo session_id(); ?>" value="<?php echo gera_sessao(session_id(),sha1(uniqid())); ?>" />
			<input type="hidden" name="form_id" value="tccus" />
			<input type="hidden" name="tipo" value="11" />

		</form>
<?php 
break;
case 'ina': //
?>
		<h1>Inclusão de Novos Agentes</h1>

		<form action="" method="POST" class="formee">
			<div class="grid-10-12" style="">
				<select name="operacao">
					<option value="null">Selecione a Operação</option>
					<option value="null"></option>
					<?php echo $operacoes; ?>
				</select>
			</div>

			<div class="grid-2-12" style=""><input type="text" name="matricula" placeholder="Matrícula" maxlength="8" /></div>
			<div class="grid-8-12" style=""><input type="text" name="nome" placeholder="Nome" /></div>
			<div class="grid-2-12" style=""><input type="text" name="apelido" placeholder="Apelido" /></div>
			<div class="grid-2-12" style=""><input type="text" name="avaya" placeholder="Login Avaya" /></div>
			<div class="grid-2-12" style=""><input type="text" name="ccusto" placeholder="Centro de Custo" /></div>
			
			<div class="clearfix"></div>
			<input type="submit" value="Enviar" />
			<input type="hidden" name="<?php echo session_id(); ?>" value="<?php echo gera_sessao(session_id(),sha1(uniqid())); ?>" />
			<input type="hidden" name="form_id" value="ina" />
			<input type="hidden" name="tipo" value="1" />

		</form>

<?php 
break;
case '': //
?>
		<ul>
			<li><a href="?acao=nova&s=ina" title="">Inclusão de Novos Agentes</a></li>
			<li><a href="?acao=nova&s=tdh" title="">Troca definitiva de Horário</a></li>
			<li><a href="?acao=nova&s=ts" title="">Troca de Status</a></li>
			<li><a href="?acao=nova&s=tcdf" title="">Troca de Casada de Folgas</a></li>
			<li><a href="?acao=nova&s=tlna" title="">Troca de Login/Nome de Atendimento</a></li>
			<li><a href="?acao=nova&s=lee" title="">Lançamento de Eventos na Escala</a></li>
			<li><a href="?acao=nova&s=td" title="">Troca de Horário/Dia</a></li>
			<li><a href="?acao=nova&s=thrq" title="">Troca de Hierarquia</a></li>
			<li><a href="?acao=nova&s=pdi" title="">Premissas para Dimensionamento</a></li>
			<li><a href="?acao=nova&s=rsccm7" title="">Reset de senha CCM7</a></li>
			<li><a href="?acao=nova&s=cnccm7" title="">Criação de Novas Operações CCM7</a></li>
			<li><a href="?acao=nova&s=tccus" title="">Troca do Centro de Custo</a></li>
			<!-- <li><a href="?acao=nova&s=" title=""></a></li>-->
		</ul>
<?php 
break;

}
?>

</div><!-- / fim content-bottom -->