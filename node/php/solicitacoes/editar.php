<?php

if($_SERVER['REQUEST_METHOD'] == 'POST'){
	
	if($_POST['form_id'] == 'comment'){
		$solicita_id = sanitiza('post','sid');
		$comentario = sanitiza('post','comentario');
		$timestamp = time();
		$maquina = (empty($_SERVER["HTTP_X_FORWARDED_FOR"])) ? $_SERVER['REMOTE_ADDR'] : $_SERVER["HTTP_X_FORWARDED_FOR"];
		$usuario = $user->name;

		// id, status, solicitacao, comentario, usuario, data_hora, ip) VALUES ();
		$query = "
			INSERT INTO solicitacoes.tbl_interacoes VALUES (DEFAULT,1,'$solicita_id','$comentario','$usuario','$timestamp','$ip');
			UPDATE solicitacoes.tbl_solicitacoes SET status=2 WHERE id='$solicita_id';";
		$exec = odbc_exec($conn,$query);

		//
		if($exec == false){
			echo $query;
		}
	}
	else if($_POST['form_id'] == 'atende'){
		$solicita_id = sanitiza('post','sid');
		$timestamp = time();
		$maquina = (empty($_SERVER["HTTP_X_FORWARDED_FOR"])) ? $_SERVER['REMOTE_ADDR'] : $_SERVER["HTTP_X_FORWARDED_FOR"];
		$usuario = $user->name;

		//
		$query = "
			INSERT INTO solicitacoes.tbl_interacoes VALUES (DEFAULT,1,'$solicita_id','Esta solicitação está em andamento.','$usuario','$timestamp','$ip');
			UPDATE solicitacoes.tbl_solicitacoes SET status=2, responsavel='$usuario' WHERE id='$solicita_id';";
		$exec = odbc_exec($conn,$query);

		//
		if($exec == false){
			echo $query;
		}
	}

	else if($_POST['form_id'] == 'aprove'){
		$solicita_id = sanitiza('post','sid');
		$urole = sanitiza('post','urole');
		$status = ($urole == 'supervisor') ? 3 : 2 ;
		$mensagem = ($urole == 'supervisor') ? 'Esta solicitação foi validada e finalizada com sucesso.' : 'Esta solicitação foi concluída porém aguarda validação por parte do solicitante.' ;
		$timestamp = time();
		$maquina = (empty($_SERVER["HTTP_X_FORWARDED_FOR"])) ? $_SERVER['REMOTE_ADDR'] : $_SERVER["HTTP_X_FORWARDED_FOR"];
		$usuario = $user->name;

		//
		$query = "
			INSERT INTO solicitacoes.tbl_interacoes VALUES (DEFAULT,1,'$solicita_id','$mensagem','$usuario','$timestamp','$ip');
			UPDATE solicitacoes.tbl_solicitacoes SET status='$status', responsavel='$usuario', datahora_solucao='$timestamp' WHERE id='$solicita_id';";
		$exec = odbc_exec($conn,$query);

		//
		if($exec == false){
			echo $query;
		}
	}
	
}





/**
 *
 */
if(in_array('administrator',$user->roles)){
	$urole = 'administrator';
}

if(in_array('planejamento',$user->roles)){
	$urole = 'planejamento';
}

if(in_array('solicitacoes',$user->roles)){
	$urole = 'supervisor';
}


$solicita_id = sanitiza('get','sid');


$query = "
	SELECT s.id,
		s.status,
		s.tipo,
		t.nome AS tipo_nome,
		t.sla AS tipo_sla,
		s.solicitante,
		to_timestamp(s.datahora_solicitacao) AS data_inicio,
		to_char(to_timestamp(s.datahora_solicitacao),'DD/MM/YYYY HH24:MI:SS') AS data_inicio_human,
		CURRENT_TIMESTAMP-to_timestamp(s.datahora_solicitacao) AS tempo_decorrido,
		to_timestamp(s.datahora_solicitacao) AS data_solucao,
		to_char(to_timestamp(s.datahora_solucao),'DD/MM/YYYY HH24:MI:SS') AS data_solucao_human,
		s.responsavel
	FROM solicitacoes.tbl_solicitacoes AS s
	LEFT OUTER JOIN solicitacoes.tbl_tipos AS t ON s.tipo=t.id
	WHERE s.id='$solicita_id'";
$exec = odbc_exec($conn,$query);

/*
 Valida o retorno da query
*/
$resultado = odbc_fetch_array($exec);
$tabela = 'solicitacoes.tbl_dados_'.$resultado['tipo'];
$query2 = "
	SELECT dados.*, operacao.assunto_1 AS nome_operacao 
	FROM $tabela AS dados
	LEFT OUTER JOIN skills.tbl_assunto_1 AS operacao ON dados.operacao=operacao.id
	WHERE solicitacao='$solicita_id'";
$exec2 = odbc_exec($conn,$query2);
$resultado2 = odbc_fetch_array($exec2);

//
$sla_horas = $resultado['tipo_sla']/3600;

switch($resultado['status']){
	case 1:	$status = 'Aberto';	break;
	case 2: $status = 'Ag. Finalização'; break;
	case 3: $status = 'Finalizado'; break;
}
?>

<script>
	function exibe_novainteracao(){
		document.getElementById('nova-interacao').style.display = 'block';
	}
</script>



<div class="interno">
	<div class="menu-secundario">
		<?php require(SOLIC_PATH.'/navigation.php');?>
	</div>

	<div class="solicitacoes">
		<div class="alert-box"><?php echo exibe_alerta(); destroi_alerta(); ?></div>
		<div class="topo">
			<table>
				<tr>
					<td style="width:150px;"><strong>Solicitante:</strong></td>
					<td><?php echo $resultado['solicitante'];?></td>
				</tr>
				<tr>
					<td><strong>Tipo:</strong></td>
					<td><?php echo utf8_encode($resultado['tipo_nome']);?></td>
				</tr>
				<tr>
					<td><strong>SLA:</strong></td>
					<td><?php echo converte_hora($resultado['tipo_sla']);?></td>
				</tr>
				<tr>
					<td><strong>Responsável:</strong></td>
					<td><?php echo (empty($resultado['responsavel'])) ? $resultado['solicitante'] : $resultado['responsavel'] ;?></td>
				</tr>
				<tr>
					<td><strong>Status:</strong></td>
					<td><?php echo $status;?></td>
				</tr>
				<tr>
					<td><strong>Abertura:</strong></td>
					<td><?php echo $resultado['data_inicio_human'];?></td>
				</tr>
				<tr>
					<td><strong>Data Final (prev.):</strong></td>
					<td><?php echo data_final_sla($sla_horas,$resultado['data_inicio']);?></td>
				</tr>
				<!--<tr>
					<td><strong>Tempo Decorrido:</strong></td>
					<td><?php echo str_replace('days','dias',$resultado['tempo_decorrido']);?></td>
				</tr>-->
				<tr>
					<td><strong>Data Final:</strong></td>
					<td><?php echo $resultado['data_solucao_human'];?></td>
				</tr>
			</table>

		</div>

		<div class="conteudo">
			<span class="titulo">Descrição</span>
			<div class="descricao">

				<table>

					<?php

					$n = 0;

					while ($i < odbc_num_fields($exec2)){ 
						$i++; 
						$nomecoluna[$n] = odbc_field_name($exec2, $i);
						$n++;
					} 

					for ($i=0; $i<count($nomecoluna); $i++) {

						if($nomecoluna[$i] == 'operacao'){
							//echo '<tr><td style="width:150px;"><strong>'.ucfirst(str_replace('_', ' ',$nomecoluna[$i])).':</strong></td><td> '.$resultado2[$nomecoluna[$i]].'</td></tr>';
						}
						else{
							echo '<tr><td style="width:150px;"><strong>'.ucfirst(str_replace('_', ' ',$nomecoluna[$i])).':</strong></td><td> '.$resultado2[$nomecoluna[$i]].'</td></tr>';
						}

					}
					?>	
					</table>
			</div>
		</div>

		<div class="interacoes">

			<div class=""><span class="titulo">Interações</span> (<span class=""><a href="#" onclick="exibe_novainteracao();">+Nova Interação</a></span>)</div>
			
			<div class="nova-interacao" id="nova-interacao">
				<form action="?acao=editar&sid=<?php echo $resultado['id'];?>" method="POST" class="formee">
					<textarea name="comentario"></textarea>
					<input type="submit" value="Salvar" style="margin-top:10px;" />
					<input type="hidden" name="sid" value="<?php echo $resultado['id'];?>" />
					<input type="hidden" name="form_id" value="comment" />
					<input type="hidden" name="urole" value="<?php echo $urole;?>" />
					<input type="hidden" name="status" value="<?php echo $resultado['status'];?>" />
					
				</form>
			</div>

			<table style="margin:10px 0 0 0;">
			<?php
			$query3 = "
				SELECT id, solicitacao, comentario, usuario, to_char(to_timestamp(data_hora),'DD/MM/YYYY HH24:MI:SS') AS data_hora
				FROM solicitacoes.tbl_interacoes 
				WHERE solicitacao='$solicita_id'
				ORDER by id DESC";
			$exec3 = odbc_exec($conn,$query3);

			while($resultado3 = odbc_fetch_array($exec3)){
			?>
				<tr>
					<td style="width:130px;"><?php echo $resultado3['data_hora'];?></td>
					<td style="width:100px;"><?php echo $resultado3['usuario'];?></td>
					<td><?php echo $resultado3['comentario'];?></td>
				</tr>
			<?php }?>
			</table>

			<?php if($resultado['status'] == 1 && $urole != 'solicitacoes'){?>
			<form action="" method="POST" class="float-left" style="margin:20px 0 0 0;">
				<input type="submit" value="Responder" />
				<input type="hidden" name="sid" value="<?php echo $resultado['id'];?>" />
				<input type="hidden" name="form_id" value="atende" />
				<input type="hidden" name="urole" value="<?php echo $urole;?>" />
				<input type="hidden" name="status" value="<?php echo $resultado['status'];?>" />
			</form>
			<?php }?>

			<?php if($resultado['status'] != 5){?>
			<form action="" method="POST" style="margin:20px 0 0 0;">
				<input type="submit" value="Finalizar" />
				<input type="hidden" name="sid" value="<?php echo $resultado['id'];?>" />
				<input type="hidden" name="form_id" value="aprove" />
				<input type="hidden" name="urole" value="<?php echo $urole;?>" />
				<input type="hidden" name="status" value="<?php echo $resultado['status'];?>" />
			</form>
			<?php }?>

		</div><!-- Interações -->
	</div><!-- / fim -->
</div>





		