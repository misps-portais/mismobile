<?php

/////////////dicionarios/////////////////////////////////////////////////////
	function dic_tipo($tipo) {
		$matriz = array('admin' => 'Administração' ,
						'relatorio' => 'Relatório',
						'usuario' => 'Usuário');
		return $matriz[$tipo];
	}
	function dic_pagina($strpagina) {
		$matriz = array('novo_cadastro' => 'Cadastro de Novo Usuário',
						'cadastro_principal' => 'Cadastro Principal',
						'contatos' => 'Contatos',
						'endereco' => 'Endereço',
						'cadastro_formacao' => 'Formação',
						'cadastro_cursos' => 'Cursos Extracurriculares',
						'cadastro_profissional' => 'Experiência Profissional',
						'cadastro_idiomas' => 'Idiomas',
						'cadastro_lazer' => 'Lazer',
						'relatorio_1' => 'Relatorio 1');
		return $matriz[$strpagina];
	}
//////////////////////////////////////////////////////////////////////////////
	function monta_pagina($conn, $endereco, $parametros) {
		$texto = " <div>";
		$texto = $texto.monta_cabecalho($endereco);
		$texto = $texto.monta_corpo($conn, $endereco, $parametros);
		$texto = $texto.monta_rodape($endereco);
		$texto = $texto." <div>";

		return $texto;
	}
	function monta_cabecalho($endereco){
		$strtipo = dic_tipo($endereco['tipo']);
		$strpagina = dic_pagina($endereco['pagina']);
		echo "
					<div>
						<h3>$strtipo >> $strpagina</h3>
						<h1>$strpagina</h1>
					</div>	
					<hr>
					<br>";

	}
	function monta_rodape($endereco) {
		//$strpagina = $endereco['pagina'];
		echo "<div>
					<input type='button' onclick='enviar(".'"'.$endereco['tipo'].'",'.'"'.$endereco['pagina'].'"'.")' value='enviar' style='float:right'>
				</div>";		
	}
	function monta_corpo($conn, $endereco, $parametros) {
		$form = new imprimir_formulario();
		$strpagina = $endereco['pagina'];
		$texto = "";
		switch ($strpagina) {
			case 'novo_cadastro':
				$form->inicio($strpagina);
				echo "<u><b>Dados do Novo Funcionário:</b></u><br><br>";
				echo "<ul class='formulario'>";
				echo "<li>";
				$form->campo('matricula','');
				$form->ajuda('matricula');
				echo "</li>";
				echo "<li>";
				$form->campo('nome','');	
				echo "</li>";
				echo "<li>";
				$form->campo('cargo','');	
				echo "</li>";
				echo "</ul>";
				echo "<br><br>";
				//echo "<div>Gestor do funcionario";
				echo "<u><b>Dados do Gestor:</b></u><br><br>";
				echo "<ul class='formulario'>";
				echo "<li>";
					echo "<label>Cargo do Gestor:</label>";
					echo "<select id='cargo_gestor' onchange='carrega_campo(this.id, \"div_nome_gestor\", this.value)'>";
					echo "<option value=0></option>";
					foreach (tabela_descricao('cargo') as $key => $value) {
						if ($key==$valor){
							echo "<option value='$key' selected>$value</option>";
						}
						else {
							echo "<option value='$key'>$value</option>";
						}
					}
					echo "</select>";
				echo "<div id='div_nome_gestor'></div>";
				echo "</li>";
				echo "</ul>";
				echo "</div>";
				echo "</div>";			
				break;
			case 'cadastro_principal':
			case 'contatos':
			case 'endereco':
				$strsql = "SELECT * FROM schedule.".$strpagina."_exibe(".$parametros['user'].")";
				$rs=odbc_exec($conn,$strsql);
				$form->inicio($strpagina);

				while(odbc_fetch_row($rs)) {
					for($i=1;$i<=odbc_num_fields($rs);$i++) {
						$campo = odbc_field_name($rs,$i);
						$valor = odbc_result($rs,$i);
						echo "<ul class='formulario'>";
						echo "<li>";
						$form->campo($campo, $valor);
						$form->ajuda($campo);
						echo "</li>";						
				    }
				}
				echo "		</ul>
							</div>
							<br>";				
				break;
			default:
				echo  "<h1>Página em Construção</h1>";
				break;
		}
	}
////////////////////////////////////////////////////////////////////////////////////////////////////////
	/*
	
	*/
	class imprimir_formulario {
		function inicio($strpagina) {
			echo "<div id='sub_formulario' title='$strpagina'>";
		}
		function campo($nome, $valor) {
			$dic_campo = array(
				'matricula' => array('nomeclatura' =>'Matrícula', 'tipo' => 'text', 'estilo' => '', 'adicional' => "onkeypress='somente_numeros()' onchange='valida_matricula(this.id)'"),
				'nome' => array('nomeclatura' =>'Nome Completo', 'tipo' => 'text', 'estilo' => 'width:75%', 'adicional' => ''),
				'cpf' => array('nomeclatura' =>'CPF', 'tipo' => 'text', 'estilo' => '', 'adicional' => 'onkeypress=\'somente_numeros()\''),
				'sexo' => array('nomeclatura' =>'Sexo', 'tipo' => 'radio', 'estilo' => 'width:4%', 'adicional' => ''),
				'dt_nasc' => array('nomeclatura' =>'Data de Nascimento', 'tipo' => 'date', 'estilo' => '', 'adicional' => ''),
				'idade' => array('nomeclatura' =>'Idade', 'tipo' => 'text', 'estilo' => '', 'adicional' => ''),
				'estado_civil' => array('nomeclatura' =>'Estado Civil', 'tipo' => 'select', 'estilo' => '', 'adicional' => ''),
				'cargo' => array('nomeclatura' =>'Cargo', 'tipo' => 'select', 'estilo' => '', 'adicional' => ''),
				'fone_residencial' => array('nomeclatura' =>'Telefone Residêncial', 'tipo' => 'text', 'estilo' => '', 'adicional' => 'onkeypress="fone(this)"'),
				'fone_celular' => array('nomeclatura' =>'Telefone Celular', 'tipo' => 'text', 'estilo' => '', 'adicional' => 'onkeypress="fone(this)"'),
				'email_pessoal' => array('nomeclatura' =>'Email Pessoal', 'tipo' => 'text', 'estilo' => '', 'adicional' => ''),
				'logradouro' => array('nomeclatura' =>'Logradouro', 'tipo' => 'text', 'estilo' => 'width:75%', 'adicional' => ''),
				'numero' => array('nomeclatura' =>'Número', 'tipo' => 'text', 'estilo' => '', 'adicional' => ''),
				'complemento' => array('nomeclatura' =>'Complemento', 'tipo' => 'text', 'estilo' => '', 'adicional' => ''),
				'bairro' => array('nomeclatura' =>'Bairro', 'tipo' => 'text', 'estilo' => '', 'adicional' => ''),
				'cep' => array('nomeclatura' =>'CEP', 'tipo' => 'text', 'estilo' => '', 'adicional' => ''),
				'cidade' => array('nomeclatura' =>'Cidade', 'tipo' => 'text', 'estilo' => '', 'adicional' => ''),
				'regiao' => array('nomeclatura' =>'Região', 'tipo' => 'text', 'estilo' => '', 'adicional' => ''),
				);

			switch ($dic_campo[$nome]['tipo']) {
				case 'select':
					echo "<label>".$dic_campo[$nome]['nomeclatura'].":</label>";
					echo "<select id='$nome'>";
					echo "<option value=0></option>";
					foreach (tabela_descricao($nome) as $key => $value) {
						if ($key==$valor){
							echo "<option value='$key' selected>$value</option>";
						}
						else {
							echo "<option value='$key'>$value</option>";
						}
					}
					echo "</select>";
					break;
				case 'radio':
					echo "<label>".$dic_campo[$nome]['nomeclatura'].":</label>";
					foreach (tabela_descricao($nome) as $key => $value) {
						if ($key==$valor){
							echo "<font size='2px'>$value</font><input type='radio' name='$nome' value='$key' checked='true' style='".$dic_campo[$nome]['estilo']."'>";
						}
						else {
							echo "<font size='2px'>$value</font><input type='radio' name='$nome' value='$key' style='".$dic_campo[$nome]['estilo']."'>";
						}
					}
					break;
				default:
					if (substr($valor, 0,1).substr($valor, -1)=='{}') {
						$valor = str_replace(array("{", "}"), array("", ""), $valor);
					}
					if (empty($valor)) {
						echo "<label>".$dic_campo[$nome]['nomeclatura'].":</label><input style='".$dic_campo[$nome]['estilo']."' type='".$dic_campo[$nome]['tipo']."' id='$nome' ".$dic_campo[$nome]['adicional'].">";
					}
					else {
						echo "<label>".$dic_campo[$nome]['nomeclatura'].":</label><input style='".$dic_campo[$nome]['estilo']."' type='".$dic_campo[$nome]['tipo']."' id='$nome' ".$dic_campo[$nome]['adicional']." value='$valor'>";
					}
					break;
			}


		}
		function ajuda($nome){
			$dic_campo = array(
								'matricula'=>'São aceitos somente números e a <br> matrícula deve conter o digito verificador',
								'fone_residencial'=>'Digitar somente números <br> digitar o DDD sem o zero: (11) 1234-1234',
								'fone_celular'=>'Digitar somente números <br> digitar o DDD sem o zero: (11) 1234-1234',
								);
			if (array_key_exists($nome, $dic_campo)) {
			echo "<div class='help'>
					<img src='..\habilidades\img\message-16-help.png' style='position:relative;width:16px;height: 16px;'>
					<div style='z-index: 100'>".$dic_campo[$nome]."</div>
				</div>";				
			}
		}
	}


	function tabela_descricao($campo) {
		$conn = $GLOBALS["conn"];
		$strsql = "SELECT *
					FROM schedule.tbl_desc_$campo
					ORDER BY 2";
		$rs=odbc_exec($conn,$strsql);
		while(odbc_fetch_row($rs)) {
			$id[] = odbc_result($rs, 1);
			$descricao[] = odbc_result($rs, 2);
		}

		$matriz = array_combine($id, $descricao);
		return $matriz;

	}

?>
