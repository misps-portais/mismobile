﻿CREATE OR REPLACE FUNCTION schedule.novo_cadastro(matricula integer, nome text, login_edicao text, gestor integer) 
RETURNS TEXT
AS $$
DECLARE 
	nome_funcionario text;
	id_funcionario integer; --id do funcionario no schedule
BEGIN 
	
	nome_funcionario := (select distinct fd.nome
	from schedule.tbl_func_schedule as fs
	inner join schedule.tbl_func_dados as fd
	on fs.cf = fd.id
	where fs.matricula = $1);

	IF nome_funcionario is null then

		INSERT INTO schedule.tbl_func_dados (id, nome, dt_edicao, login_edicao)
		VALUES (default, $2, current_timestamp, $3)
		RETURNING id INTO id_funcionario;
		
		INSERT INTO schedule.tbl_func_schedule (cf, matricula, dt_edicao, login_edicao, cf_gestor)
		VALUES (id_funcionario, $1, current_timestamp, $3, $4);
		
		RETURN 'Cadastro realizado com sucesso';
	ELSE
		RETURN 'Cadastro não realizado <br> .Já existe a matrícula no sistema';
	END IF;

END;

$$ LANGUAGE plpgsql;


DROP TYPE IF EXISTS schedule.tipo_cadastro_principal_exibe cascade;
CREATE TYPE  schedule.tipo_cadastro_principal_exibe AS (
		nome text, 
		cpf bigint, 
		sexo integer, 
		dt_nasc date, 
		idade integer, 
		estado_civil integer
);

CREATE OR REPLACE FUNCTION schedule.cadastro_principal_exibe(matricula integer) 
RETURNS SETOF schedule.tipo_cadastro_principal_exibe
AS $$
DECLARE 
	id_funcionario integer;
BEGIN 
	
	id_funcionario := (SELECT DISTINCT fs.cf
				FROM schedule.tbl_func_schedule as fs
				WHERE fs.matricula = $1
				);

	RETURN QUERY (
	SELECT 
		cast(fd.nome as text) as nome, 
		fd.cpf as cpf, 	
		fd.sexo as sexo, 
		fd.dt_nasc as dt_nasc, 
		cast(date_part('year', age(fd.dt_nasc)) as integer) as idade, 
		fd.estado_civil as estado_civil
	FROM schedule.tbl_func_dados as fd
	WHERE fd.id = id_funcionario);
	RETURN;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION schedule.cadastro_principal_envia(matricula integer, nome text, cpf bigint, sexo integer, dt_nasc date, estado_civil integer) 
RETURNS text
AS $$
DECLARE 
	id_funcionario integer;
BEGIN 
	
	id_funcionario := (SELECT DISTINCT fs.cf
				FROM schedule.tbl_func_schedule as fs
				WHERE fs.matricula = $1
				);
	IF id_funcionario IS NULL THEN
		RETURN 'Matrícula não cadastrada <br>. Por favor, procure seu gestor';
	ELSE
		UPDATE schedule.tbl_func_dados as fd
		SET 
		(login_edicao, nome, cpf, sexo, dt_nasc, estado_civil, dt_edicao) 
		= 
		($1, $2, $3, $4, $5, $6, current_timestamp)
		WHERE fd.id = id_funcionario;
		RETURN 'Dados atualizados com sucesso!';
	END IF;
END;
$$ LANGUAGE plpgsql;

DROP TYPE IF EXISTS schedule.tipo_contatos_exibe cascade;
CREATE TYPE  schedule.tipo_contatos_exibe AS (
		fone_residencial bigint[], 
		fone_celular bigint[], 
		email_pessoal character varying(200)[] 
);

CREATE OR REPLACE FUNCTION schedule.contatos_exibe(matricula integer) 
RETURNS SETOF schedule.tipo_contatos_exibe
AS $$
DECLARE 
	id_funcionario integer;
BEGIN 
	
	id_funcionario := (SELECT DISTINCT fs.cf
				FROM schedule.tbl_func_schedule as fs
				WHERE fs.matricula = $1
				);

	RETURN QUERY (
	SELECT 
		fd.fone_residencial as fone_residencial, 
		fd.fone_celular as fone_celular,
		fd.email_pessoal as email_pessoal
	FROM schedule.tbl_func_dados as fd
	WHERE fd.id = id_funcionario);
	RETURN;
END;
$$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION schedule.contatos_envia(matricula integer, fone_residencial bigint[], fone_celular bigint[], email_pessoal character varying(200)[]) 
RETURNS text
AS $$
DECLARE 
	id_funcionario integer;
BEGIN 
	
	id_funcionario := (SELECT DISTINCT fs.cf
				FROM schedule.tbl_func_schedule as fs
				WHERE fs.matricula = $1
				);
	IF id_funcionario IS NULL THEN
		RETURN 'Matrícula não cadastrada <br>. Por favor, procure seu gestor';
	ELSE
		UPDATE schedule.tbl_func_dados as fd
		SET 
		(login_edicao, fone_residencial, fone_celular, email_pessoal, dt_edicao) 
		= 
		($1, $2, $3, $4, current_timestamp)
		WHERE fd.id = id_funcionario;
		RETURN 'Dados atualizados com sucesso!';
	END IF;
END;
$$ LANGUAGE plpgsql;

DROP TYPE IF EXISTS schedule.tipo_endereco_exibe cascade;
CREATE TYPE  schedule.tipo_endereco_exibe AS (
		logradouro character varying(200), 
		numero integer, 
		complemento character varying(200),
		bairro character varying(100),
		cep integer,
		cidade character varying(50),
		regiao character varying(50)
);

CREATE OR REPLACE FUNCTION schedule.endereco_exibe(matricula integer) 
RETURNS SETOF schedule.tipo_endereco_exibe
AS $$
DECLARE 
	id_funcionario integer;
BEGIN 
	
	id_funcionario := (SELECT DISTINCT fs.cf
				FROM schedule.tbl_func_schedule as fs
				WHERE fs.matricula = $1
				);

	RETURN QUERY (
	SELECT 
		fd.logradouro,
		fd.numero,
		fd.complemento,
		fd.bairro,
		fd.cep,
		fd.cidade,
		fd.regiao
	FROM schedule.tbl_func_dados as fd
	WHERE fd.id = id_funcionario);
	RETURN;
END;
$$ LANGUAGE plpgsql;