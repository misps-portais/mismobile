﻿DROP TYPE IF EXISTS schedule.tipo_contatos_exibe cascade;
CREATE TYPE  schedule.tipo_contatos_exibe AS (
		email character varying(200)[],
		fone_residencia bigint[], 
		fone_celular bigint[]
);

CREATE OR REPLACE FUNCTION schedule.cadastro_principal_exibe(matricula integer)
RETURNS SETOF schedule.tipo_cadastro_principal_exibe
AS $$
DECLARE 
	id_funcionario integer;
BEGIN 
	
	id_funcionario := (SELECT DISTINCT fs.cf
				FROM schedule.tbl_func_schedule as fs
				WHERE fs.matricula = $1
				);

	RETURN QUERY (
	SELECT 
		cast(fd.email as text) as nome, 
		fd.cpf as cpf, 	
		fd.sexo as sexo, 
		fd.dt_nasc as dt_nasc, 
		cast(date_part('year', age(fd.dt_nasc)) as integer) as idade, 
		fd.estado_civil as estado_civil
	FROM schedule.tbl_func_dados as fd
	WHERE fd.id = id_funcionario);
	RETURN;
END;
$$ LANGUAGE plpgsql;
