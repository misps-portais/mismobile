-- Database generated with pgModeler (PostgreSQL Database Modeler).
-- PostgreSQL version: 9.2
-- Project Site: pgmodeler.com.br
-- Model Author: ---

SET check_function_bodies = false;
-- ddl-end --


-- Database creation must be done outside an multicommand file.
-- These commands were put in this file only for convenience.
-- -- object: schedule_mis | type: DATABASE --
-- CREATE DATABASE schedule_mis
-- 	ENCODING = 'UTF8'
-- ;
-- -- ddl-end --
-- 

-- object: public.tbl_desc_func | type: TABLE --
CREATE TABLE public.tbl_desc_func(
	id serial NOT NULL,
	cpf smallint,
	nome smallint,
	dt_nasc smallint,
	contato smallint,
	correio_pessoal smallint,
	tm_camisa smallint,
	tm_calca smallint,
	tm_calcado smallint,
	"PCD" smallint,
	"PCD_qual" smallint,
	cep smallint,
	logradouro smallint,
	tipo smallint,
	numero smallint,
	bairro smallint,
	regiao smallint,
	cidade smallint,
	estado smallint,
	estado_civil smallint,
	row_date smallint,
	CONSTRAINT tbl_desc_func_id PRIMARY KEY (id)
)WITH ( OIDS = TRUE );
-- ddl-end --

COMMENT ON COLUMN public.tbl_desc_func.contato IS 'array';
-- ddl-end --

-- object: public.tbl_cursos | type: TABLE --
CREATE TABLE public.tbl_cursos(
	id serial,
	tipo smallint,
	descricao smallint
)WITH ( OIDS = TRUE );
-- ddl-end --

-- ddl-end --

-- object: public.tbl_sites | type: TABLE --
CREATE TABLE public.tbl_sites(
	id serial,
	site character varying(20)
)WITH ( OIDS = TRUE );
-- ddl-end --

-- ddl-end --

-- object: public.tbl_principal | type: TABLE --
CREATE TABLE public.tbl_principal(
	id serial NOT NULL,
	cf serial NOT NULL,
	matricula character varying(8),
	id_empresa smallint,
	sys_avaya_login character varying(8),
	sys_avaya_servidor smallint,
	sys_avaya_dac smallint,
	id_site smallint,
	id_cargo smallint,
	cf_gestor character varying(11),
	usuario_edicao character varying(11),
	row_date date,
	CONSTRAINT cf FOREIGN KEY (cf)
	REFERENCES public.tbl_desc_func (id) MATCH FULL
	ON DELETE NO ACTION ON UPDATE NO ACTION NOT DEFERRABLE,
	CONSTRAINT tbl_principal_cf FOREIGN KEY (cf)
	REFERENCES public.tbl_desc_func (id) MATCH FULL
	ON DELETE NO ACTION ON UPDATE NO ACTION NOT DEFERRABLE
)WITH ( OIDS = TRUE );
-- ddl-end --

COMMENT ON COLUMN public.tbl_principal.usuario_edicao IS 'cpf do usuario que editou';
-- ddl-end --

-- object: public.tbl_empresas | type: TABLE --
CREATE TABLE public.tbl_empresas(
	id serial,
	cod smallint,
	nome text,
	endereco text,
	tipo_colaborador smallint
)WITH ( OIDS = TRUE );
-- ddl-end --

-- ddl-end --

-- object: public.tbl_cargos | type: TABLE --
CREATE TABLE public.tbl_cargos(
	id smallint,
	cargo smallint
)WITH ( OIDS = TRUE );
-- ddl-end --

-- ddl-end --

-- object: public.tbl_hobby_tipo | type: TABLE --
CREATE TABLE public.tbl_hobby_tipo(
	id serial,
	hobby_tipo smallint,
	CONSTRAINT tbl_hobby_tipo_id PRIMARY KEY (id)
)WITH ( OIDS = TRUE );
-- ddl-end --

-- ddl-end --

-- object: public.tbl_hobby | type: TABLE --
CREATE TABLE public.tbl_hobby(
	id serial,
	tipo smallint,
	hobby character varying(40),
	CONSTRAINT tbl_id PRIMARY KEY (id),
	CONSTRAINT tbl_hobby_tipo FOREIGN KEY (tipo)
	REFERENCES public.tbl_hobby_tipo (id) MATCH FULL
	ON DELETE NO ACTION ON UPDATE NO ACTION NOT DEFERRABLE
)WITH ( OIDS = TRUE );
-- ddl-end --

-- ddl-end --

-- object: public.tbl_social | type: TABLE --
CREATE TABLE public.tbl_social(
	id serial,
	cf serial,
	tipo smallint,
	descricao smallint,
	CONSTRAINT tbl_social_cf FOREIGN KEY (cf)
	REFERENCES public.tbl_desc_func (id) MATCH FULL
	ON DELETE NO ACTION ON UPDATE NO ACTION NOT DEFERRABLE
)WITH ( OIDS = TRUE );
-- ddl-end --

COMMENT ON COLUMN public.tbl_social.tipo IS 'rede social ou email';
-- ddl-end --

-- object: public.tbl_func_voz | type: TABLE --
CREATE TABLE public.tbl_func_voz(
	id serial,
	cf serial,
	ddd smallint,
	numero smallint,
	tipo smallint,
	CONSTRAINT tbl_func_voz_cf FOREIGN KEY (cf)
	REFERENCES public.tbl_desc_func (id) MATCH FULL
	ON DELETE NO ACTION ON UPDATE NO ACTION NOT DEFERRABLE
)WITH ( OIDS = TRUE );
-- ddl-end --

-- ddl-end --

-- object: public.tbl_func_transporte | type: TABLE --
CREATE TABLE public.tbl_func_transporte(
	id smallint,
	cf serial,
	cod_transporte smallint,
	descricao smallint,
	transporte_qtd smallint,
	row_date smallint,
	CONSTRAINT tbl_func_transporte_cf FOREIGN KEY (cf)
	REFERENCES public.tbl_desc_func (id) MATCH FULL
	ON DELETE NO ACTION ON UPDATE NO ACTION NOT DEFERRABLE
)WITH ( OIDS = TRUE );
-- ddl-end --

-- ddl-end --

-- object: public.tbl_func_cursos | type: TABLE --
CREATE TABLE public.tbl_func_cursos(
	id smallint,
	cf serial,
	cod_cursos smallint,
	status smallint,
	data_conclusao smallint,
	row_date smallint,
	CONSTRAINT tbl_func_cursos_cf FOREIGN KEY (cf)
	REFERENCES public.tbl_desc_func (id) MATCH FULL
	ON DELETE NO ACTION ON UPDATE NO ACTION NOT DEFERRABLE
)WITH ( OIDS = TRUE );
-- ddl-end --

-- ddl-end --

-- object: public.tbl_cursos_tipos | type: TABLE --
CREATE TABLE public.tbl_cursos_tipos(
	id serial,
	tipo smallint
)WITH ( OIDS = TRUE );
-- ddl-end --

-- ddl-end --

-- object: public.tbl_ccusto | type: TABLE --
CREATE TABLE public.tbl_ccusto(
	id smallint,
	ccusto smallint,
	grupo smallint,
	celula smallint,
	row_date smallint
)WITH ( OIDS = TRUE );
-- ddl-end --

-- ddl-end --

-- object: public.tbl_grupo | type: TABLE --
CREATE TABLE public.tbl_grupo(
	id smallint,
	descricao smallint
)WITH ( OIDS = TRUE );
-- ddl-end --

-- ddl-end --

-- object: public.tbl_celula | type: TABLE --
CREATE TABLE public.tbl_celula(
	id smallint,
	descricao smallint
)WITH ( OIDS = TRUE );
-- ddl-end --

-- ddl-end --

-- object: public.tbl_social_tipo | type: TABLE --
CREATE TABLE public.tbl_social_tipo(
	id smallint,
	tipo smallint
)WITH ( OIDS = TRUE );
-- ddl-end --

-- ddl-end --

-- object: public.tbl_func_hobby | type: TABLE --
CREATE TABLE public.tbl_func_hobby(
	id serial,
	cf serial,
	hobby smallint,
	row_date smallint,
	CONSTRAINT tbl_func_hobby_cf FOREIGN KEY (cf)
	REFERENCES public.tbl_desc_func (id) MATCH FULL
	ON DELETE NO ACTION ON UPDATE NO ACTION NOT DEFERRABLE,
	CONSTRAINT tbl_func_hobby_id PRIMARY KEY (id),
	CONSTRAINT tbl_func_hobby_hobby FOREIGN KEY (hobby)
	REFERENCES public.tbl_hobby (id) MATCH FULL
	ON DELETE NO ACTION ON UPDATE NO ACTION NOT DEFERRABLE
)WITH ( OIDS = TRUE );
-- ddl-end --

-- ddl-end --


