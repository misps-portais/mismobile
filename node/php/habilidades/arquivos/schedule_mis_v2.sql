﻿drop table if exists schedule.tbl_desc_tm_camisa cascade;
drop table if exists schedule.tbl_desc_tm_calca cascade;
drop table if exists schedule.tbl_desc_estado_civil cascade;
DROP TABLE IF EXISTS schedule.tbl_desc_site cascade;
DROP TABLE IF EXISTS schedule.tbl_desc_empresa cascade;
DROP TABLE IF EXISTS schedule.tbl_desc_cargo cascade;
DROP TABLE IF EXISTS schedule.tbl_desc_hobby cascade;
DROP TABLE IF EXISTS schedule.tbl_desc_telefone_tipo cascade;
DROP TABLE IF EXISTS schedule.tbl_desc_transporte_tipo cascade;
DROP TABLE IF EXISTS schedule.tbl_desc_curso_tipo cascade;
DROP TABLE IF EXISTS schedule.tbl_desc_curso_descricao cascade;
DROP TABLE IF EXISTS schedule.tbl_desc_curso_status cascade;
DROP TABLE IF EXISTS schedule.tbl_desc_grupo cascade;
DROP TABLE IF EXISTS schedule.tbl_desc_celula cascade;
DROP TABLE IF EXISTS schedule.tbl_desc_ccusto cascade;
DROP TABLE IF EXISTS schedule.tbl_desc_sexo cascade;

DROP TABLE IF EXISTS schedule.tbl_func_dados cascade;
DROP TABLE IF EXISTS schedule.tbl_func_schedule cascade;
DROP TABLE IF EXISTS schedule.tbl_func_telefone cascade;
DROP TABLE IF EXISTS schedule.tbl_func_transporte cascade;
DROP TABLE IF EXISTS schedule.tbl_func_cursos cascade;
DROP TABLE IF EXISTS schedule.tbl_func_hobby cascade;



---------------------------------------------------------------
--Tabelas de base, usadas para registrar descrição dos campos
---------------------------------------------------------------

--descrição do tamanho da camiseta
drop table if exists schedule.tbl_desc_tm_camisa;
create table schedule.tbl_desc_tm_camisa (
	id serial not null,
	tm_camisa character varying(5),
	primary key(id)
);
--descrição do tamanho da calça
drop table if exists schedule.tbl_desc_tm_calca;
create table schedule.tbl_desc_tm_calca (
	id serial not null,
	tm_calca character varying(5),
	primary key(id)
);
--descrição do estado civil
drop table if exists schedule.tbl_desc_estado_civil;
create table schedule.tbl_desc_estado_civil (
	id serial not null,
	estado_civil character varying(20),
	primary key(id)
);
-- Descrição do site do funcionário
DROP TABLE IF EXISTS schedule.tbl_desc_site;
CREATE TABLE schedule.tbl_desc_site(
	id serial,
	site character varying(20),
	primary key(id)
);
-- Descrição da empresa
DROP TABLE IF EXISTS schedule.tbl_desc_empresa;
CREATE TABLE schedule.tbl_desc_empresa(
	id serial,
	cod smallint,
	nome character varying(200),
	endereco character varying(200),
	cep integer,
	tipo_colaborador smallint,
	primary key (id)
);
-- descrição do cargo
DROP TABLE IF EXISTS schedule.tbl_desc_cargo;
CREATE TABLE schedule.tbl_desc_cargo(
	id serial not null,
	cargo character varying(100),
	primary key(id)
);
-- descrição do hobby
DROP TABLE IF EXISTS schedule.tbl_desc_hobby;
CREATE TABLE schedule.tbl_desc_hobby(
	id serial not null,
	hobby character varying(40),
	PRIMARY KEY (id)
);
--descrição do tipo de transporte utilizado: carro, metro, onibus, etc
DROP TABLE IF EXISTS schedule.tbl_desc_transporte_tipo;
CREATE TABLE schedule.tbl_desc_transporte_tipo(
	id serial not null,
	transporte_tipo character varying(50),
	primary key(id)
);

-- descrição do tipo de cursos: se curso superior, técnico, linguas
DROP TABLE IF EXISTS schedule.tbl_desc_curso_tipo;
CREATE TABLE schedule.tbl_desc_curso_tipo(
	id serial,
	curso_tipo character varying(50),
	primary key(id)
);

-- descricao do nome do curso: matematica, fisica, ingles, mecânica, etc
DROP TABLE IF EXISTS schedule.tbl_desc_curso_descricao;
CREATE TABLE schedule.tbl_desc_curso_descricao(
	id serial,
	curso_descricao character varying(50),
	primary key(id)
);

-- descricao do status do cursos se curso cursando, concluído, interrompido
DROP TABLE IF EXISTS schedule.tbl_desc_curso_status;
CREATE TABLE schedule.tbl_desc_curso_status(
	id serial,
	curso_status character varying(50),
	primary key(id)
);
-- descrição do grupo em um centro de custo
DROP TABLE IF EXISTS schedule.tbl_desc_grupo;
CREATE TABLE schedule.tbl_desc_grupo(
	id serial not null,
	grupo character varying(100),
	primary key(id)
);
-- descrição da celula de um grupo em um centro de custo
DROP TABLE IF EXISTS schedule.tbl_desc_celula;
CREATE TABLE schedule.tbl_desc_celula(
	id serial not null,
	grupo integer references schedule.tbl_desc_grupo(id),
	celula character varying(50),
	primary key(id)
);
-- descrição dos centros de custo
DROP TABLE IF EXISTS schedule.tbl_desc_ccusto;
CREATE TABLE schedule.tbl_desc_ccusto(
	id serial not null,
	ccusto character varying(20),
	grupo integer references schedule.tbl_desc_grupo(id),
	celula integer references schedule.tbl_desc_celula(id),
	primary key (id)
);
-- descrição do sexo
DROP TABLE IF EXISTS schedule.tbl_desc_sexo;
CREATE TABLE schedule.tbl_desc_sexo(
	id serial not null,
	sexo character varying(20),
	primary key (id)
);

------------------------------------------------------------
---Tabelas com dados dos funcionários-----------------------
------------------------------------------------------------

--Tabela principal com informações estáticas do funcionário
DROP TABLE IF EXISTS schedule.tbl_func_dados;
CREATE TABLE schedule.tbl_func_dados(
	id serial not null,
	bairro character varying(100),
	cep integer,
	cidade character varying(50),
	complemento character varying(200),
	cpf bigint,
	dt_edicao timestamp,
	dt_nasc date,
	email_pessoal character varying(200)[],
	estado character varying(20),
	estado_civil integer references schedule.tbl_desc_estado_civil(id),
	fone_celular bigint[],
	fone_residencial bigint[],
	login_edicao character varying(20),
	logradouro character varying(200),
	nome character varying(200),
	numero integer,
	pcd integer,
	regiao character varying(50),
	sexo integer references schedule.tbl_desc_sexo(id),
	tm_calca integer references schedule.tbl_desc_tm_calca(id),
	tm_calcado smallint,
	tm_camisa integer references schedule.tbl_desc_tm_camisa(id),
	PRIMARY KEY (id)
);

COMMENT ON TABLE schedule.tbl_func_dados IS 'Tabela onde estão dados do usuário onde não se faz necessária a atualização diária, nem tampouco se faz necessário guardar histório das alterações';
COMMENT ON column schedule.tbl_func_dados.id IS 'o id dessa tabela é a chave primária para a identificação do usuário nesse sistema ele pode ser identificado nas outras tabela como cf. O objetivo é manter o sistema integro caso ajam casos ajam problemas para preenchimento por meio do cpf';
COMMENT ON COLUMN schedule.tbl_func_dados.bairro IS 'bairro onde reside o funcionário';
COMMENT ON COLUMN schedule.tbl_func_dados.bairro IS 'cep da residencia do funcionário';
COMMENT ON COLUMN schedule.tbl_func_dados.complemento IS 'complemento do endereço: casa 2, apartamento 10 bloco 3, etc';
COMMENT ON COLUMN schedule.tbl_func_dados.cpf IS 'cpf do funcionário';
COMMENT ON COLUMN schedule.tbl_func_dados.dt_edicao IS 'Data e hora da última edicao realizada nos dados do funcionário';
COMMENT ON COLUMN schedule.tbl_func_dados.dt_nasc IS 'data de nascimento do funcionario';
COMMENT ON COLUMN schedule.tbl_func_dados.email_pessoal IS 'endereço de email do funcionário';
COMMENT ON COLUMN schedule.tbl_func_dados.estado IS 'estado da União onde reside o funcionário';
COMMENT ON COLUMN schedule.tbl_func_dados.estado_civil IS 'ver descrição na tabela tbl_desc_estado_civil';
COMMENT ON COLUMN schedule.tbl_func_dados.fone_celular IS 'números de telefone celular do funcionario esse campo é um array onde pode estar contido mais de um número';
COMMENT ON COLUMN schedule.tbl_func_dados.fone_residencial IS 'números de telefone resindencial do funcionario esse campo é um array onde pode estar contido mais de um número';
COMMENT ON COLUMN schedule.tbl_func_dados.login_edicao IS 'login do usuário que realizou a última alteração nos dados desse funcionário';
COMMENT ON COLUMN schedule.tbl_func_dados.logradouro IS 'nome da rua, avenida, alameda onde reside o funcionário';
COMMENT ON COLUMN schedule.tbl_func_dados.nome IS 'nome do funcionário';
COMMENT ON COLUMN schedule.tbl_func_dados.numero IS 'número da residência do funcionário';
COMMENT ON COLUMN schedule.tbl_func_dados.pcd IS 'pessoa com deficiencia';
COMMENT ON COLUMN schedule.tbl_func_dados.regiao IS 'zona leste, zona sul, etc';
COMMENT ON COLUMN schedule.tbl_func_dados.sexo IS 'ver descrição na tabela tbl_desc_sexo';
COMMENT ON COLUMN schedule.tbl_func_dados.tm_calca IS 'ver descrição na tabela tbl_desc_tm_calca';
COMMENT ON COLUMN schedule.tbl_func_dados.tm_camisa IS 'ver descrição na tabela tbl_desc_tm_camisa';




-- Tabela Principal - foto diária do schedule da operação
DROP TABLE IF EXISTS schedule.tbl_func_schedule;
CREATE TABLE schedule.tbl_func_schedule(
	id serial NOT NULL,
	cf integer REFERENCES schedule.tbl_func_dados (id),
	cargo integer references schedule.tbl_desc_cargo(id),
	cf_gestor integer references schedule.tbl_func_dados (id),
	dt_edicao timestamp,
	empresa integer references schedule.tbl_desc_empresa(id),
	login_edicao character varying(20),
	matricula integer,
	site integer references schedule.tbl_desc_site(id),
	sys_avaya_dac smallint,
	sys_avaya_login character varying(8),
	sys_avaya_servidor smallint,
	PRIMARY KEY (id)
);

COMMENT ON COLUMN schedule.tbl_func_schedule.cf IS 'id do funcionario em nosso sistema';
COMMENT ON COLUMN schedule.tbl_func_schedule.cargo IS 'ver descrição em schedule.tbl_desc_cargo';
COMMENT ON COLUMN schedule.tbl_func_schedule.cf_gestor IS 'número de cadastro do gestor do funcionário na data em questão (dt_edicao)';
COMMENT ON COLUMN schedule.tbl_func_schedule.dt_edicao IS 'Data e hora da última alteração na tabela';
COMMENT ON COLUMN schedule.tbl_func_schedule.cargo IS 'ver descrição em schedule.tbl_desc_empresa';
COMMENT ON COLUMN schedule.tbl_func_schedule.login_edicao IS 'login do usuário que realizou a última alteração';
COMMENT ON COLUMN schedule.tbl_func_schedule.login_edicao IS 'matricula do funcinario. Permitido somente número inteiro (sem F ou P ou 0 na frente) e com digito verificador';
COMMENT ON COLUMN schedule.tbl_func_schedule.sys_avaya_dac IS 'vincular em um segundo momento a skills.tbl_dac(id)';
COMMENT ON COLUMN schedule.tbl_func_schedule.sys_avaya_login IS 'login avaya do funcionário (casos exista)';
COMMENT ON COLUMN schedule.tbl_func_schedule.sys_avaya_servidor IS 'vincular em um segundo momento a skills.tbl_servidor(id)';

-- tabela contendo meio de locomoção do funcionário para o trabalho
DROP TABLE IF EXISTS schedule.tbl_func_transporte;
CREATE TABLE schedule.tbl_func_transporte(
	id serial not null,
	cf integer REFERENCES schedule.tbl_func_dados (id),
	transporte_tipo integer references schedule.tbl_desc_transporte_tipo(id),
	primary key (id)
);

-- tabela contendo os cursos realizados pelos funcionários
DROP TABLE IF EXISTS schedule.tbl_func_cursos;
CREATE TABLE schedule.tbl_func_cursos(
	id serial not null,
	cf integer references schedule.tbl_func_dados (id),
	tipo_curso integer references schedule.tbl_desc_curso_tipo(id),
	status integer references schedule.tbl_desc_curso_status(id),
	data_conclusao date,
	row_date date,
	primary key(id)
);

-- tabela contendo os hobbys dos funcionários
DROP TABLE IF EXISTS schedule.tbl_func_hobby;
CREATE TABLE schedule.tbl_func_hobby(
	id serial not null,
	cf integer REFERENCES schedule.tbl_func_dados (id),
	hobby integer REFERENCES schedule.tbl_desc_hobby(id),
	primary key (id)
);


------------------------------------------------------------------------------------------------------------
-----------------------------Popula tabelas-----------------------------------------------------------------
------------------------------------------------------------------------------------------------------------
INSERT INTO schedule.tbl_desc_sexo (sexo)
VALUES
('feminino'),
('masculino');

INSERT INTO schedule.tbl_desc_estado_civil (estado_civil)
VALUES
('casado'),
('solteiro');