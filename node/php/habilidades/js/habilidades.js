function usuario() {
	return document.getElementById("user").value
}

/*Permitir somente números*/
 function somente_numeros(evt) {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode( key );
    var regex = /[0-9]|\'/;
    if( !regex.test(key) ) {
      theEvent.returnValue = false;
      if(theEvent.preventDefault) theEvent.preventDefault();
    }
  }
function fone(obj){
    var valor = obj.value;
    //ACEITAR APENAS NÚMEROS
    valor=valor.replace(/\D/g,"");
    //CONTROLE DOS DADOS
    valor=valor.replace(/(\d{0})(\d)/,"$1($2");
    valor=valor.replace(/(\d{2})(\d)/,"$1) $2");
    valor=valor.replace(/(\d{4})(\d{2,3})$/,"$1-$2");
    obj.value = valor;
}

 function valida_matricula(id) {
 	strmatricula = document.getElementById(id).value;
 	if (String(strmatricula).substring(0,1)=="1" || String(strmatricula).substring(0,1)=="6") {
 		if (String(strmatricula).length!=7) {
 			alert("A matrícula informada possui " + String(strmatricula).length + " digitos. \nA matrícula deve conter 7 ou 8 digitos numéricos");
 			document.getElementById(id).value = "";
 			return;
 		}
 		else {
 			strmatricula = "0" + strmatricula;
 		}
 	}
 	if (String(strmatricula).length!=8) {
 		alert("A matrícula informada possui " + String(strmatricula).length + " digitos. \nA matrícula deve conter 7 ou 8 digitos numéricos");
 		document.getElementById(id).value = "";
 		return;
 	}
 	soma = 0;
 	cont = String(strmatricula).length;
 	for (i = 0; i < String(strmatricula).length-1; i++) {
 		soma = soma + (String(strmatricula).substring(i,i+1) * cont);
 		cont = cont-1;
 	}
 	dv = 11 - (soma % 11);
 	if (dv==10 || dv==11) {
 		dv = 0;
 	}
 	cont = String(strmatricula).length;
 	if (dv!=(String(strmatricula).substring(cont-1,cont))) {
 		alert("Matrícula invalida!");
 		document.getElementById(id).value = "";
 	}
 }

function enviar(strtipo, strpagina) {
	formulario = document.getElementById("sub_formulario");
	strindices = "";
	strparametros = "";
	for (i = 0; i < formulario.getElementsByTagName('input').length; i++) {
		obj = formulario.getElementsByTagName('input')[i]
		valor = document.getElementById(obj.id).value
		switch (obj.type) {
			case 'text':
				if (strpagina=='novo_cadastro') {
					if (isEmpty(valor)) {
						alert("Por favor, preencha o campo " + obj.id);
						return						
					}
				}
				if (obj.id == 'fone_residencial' || obj.id == 'fone_celular') {
					valor = valor.replace(/[)( -]/g, '');
					console.log(valor);
				}
				strindices = strindices + "," + obj.id;	
				strparametros = strparametros + "," + valor;									
				break;
			case 'radio':
				if (obj.checked==true) {
					strindices = strindices + "," + obj.name;	
					strparametros = strparametros + "," + valor;
				}
				break;
			case 'date':
				strindices = strindices + "," + obj.id;	
				strparametros = strparametros + "," + valor;				
				break;
		}
	}
	//loop por todos os selects do formulário
	for (i = 0; i < formulario.getElementsByTagName('select').length; i++) {
		obj = formulario.getElementsByTagName('select')[i]
		if (obj.value>0) {
			strindices = strindices + "," + obj.id;
			strparametros = strparametros + "," + obj.value;					
		}
		else {
			if (strpagina=='novo_cadastro') {
				alert("Por favor, preencha o campo " + obj.id);
				return
			}
		}
	}
	strpagina = formulario.title;
	carrega_formulario("enviar," + strtipo + "," + strpagina, strparametros, strindices);
}
function isEmpty(str) {
    return (!str || 0 === str.length);
}

function carrega_formulario(strendereco, strparametros, strindices) {
	strindices = "user" + strindices;
	strparametros = usuario() + strparametros;
	if (strendereco.length==0) {
		document.getElementById("formulario").innerHTML="";
		return;
	  }
	var xmlhttp=new XMLHttpRequest();
	xmlhttp.onreadystatechange=function() {
	  if (xmlhttp.readyState==4 && xmlhttp.status==200) {
	    document.getElementById("formulario").innerHTML=xmlhttp.responseText;
	    }
	  }
	xmlhttp.open("GET","../habilidades/formulario.php?strendereco="+strendereco+"&&strparametros="+strparametros+"&&strindices="+strindices,true);
	xmlhttp.send();
}
function carrega_campo(id_origem, id_destino, valor) {
	var xmlhttp=new XMLHttpRequest();
	xmlhttp.onreadystatechange=function() {
	  if (xmlhttp.readyState==4 && xmlhttp.status==200) {
	    document.getElementById(id_destino).innerHTML=xmlhttp.responseText;
	    }
	  }
	xmlhttp.open("GET","../habilidades/campo.php?id_origem="+id_origem+"&&id_destino="+id_destino+"&&valor="+valor,true);
	xmlhttp.send();
}
