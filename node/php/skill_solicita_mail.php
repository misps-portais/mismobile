<?php

	// Dados de envio e da mensagem
	
	$nome_remetente	= "Fulano da Silva";	
	$assunto = "Mensagem de teste";
	$email_remetente = "remetente@email.com";
	$email_destinatario = "augusto.nascimento@portoseguro.com.br";
	
	// Conteudo do e-mail (voc� poder� usar HTML)
	
	$mensagem = "Oi!<br />";
	$mensagem .= "Esta � uma mensagem de teste!<br />";
	$mensagem .= "At� mais!<br /><br />";
	$mensagem .= "<b>Fulano da Silva</b><br />";

	// Cabe�alho do e-mail. N�o � necess�rio alterar geralmente...

	$cabecalho = 	"MIME-Version: 1.0\n";
	$cabecalho .= 	"Content-Type: text/html; charset=UTF-8\n";
	$cabecalho .= 	"From: \"{$nome_remetente}\" <{$email_remetente}>\n";

	// Dispara e-mail e retorna status para vari�vel
	
	$status_envio = @mail ($email_destinatario, $assunto, $mensagem, $cabecalho); 

	// Se mensagem foi enviada pelo servidor�
	
	if ($status_envio)
	{
		echo "Mensagem enviada!";
	}
	
	// Se mensagem n�o foi enviada pelo servidor�
	
	else
	{
		echo "Mensagem n�o enviada!";
	}

?>
