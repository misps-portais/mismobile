<?php


/**
 *   Retorna uma identificação unica baseada no tempo milionésimos de segundo.
 */
function gera_uuid($complex=null){
	return uniqid();
}

/**
 *   Grava log de erros gerados por ação do usuário
 *    @param string $name - Nome da sessão.
 */
function salva_log($msg){
	$log_file = DOCS_PATH.'\logs\errors.log';
	error_log(date('d-m-Y H:i:s (T)')." - ".$msg."\r\n", 3, $log_file);
}



/**
 *   Instância sessões personalizadas.
 *    @param string $name - Nome da sessão.
 *    @param string $value - Valor a ser atribuído.
 *    @return Se a sessão for instanciada retorna o seu conteúdo, caso contrário retorna false.
 */
function gera_sessao($name,$value){
    $_SESSION[$name] = $value;
    return (!empty($_SESSION[$name])) ? $_SESSION[$name] : false ;
}

/**
 *   Destroi uma sessão instanciada.
 *    @param string $name - Nome da sessão a ser destruída.
 *    @return bool
 */
function destroi_sessao($name){
    unset($_SESSION[$name]);
    return (empty($_SESSION[$name])) ? true : false ;
}

/**
 *   Regenera uma sessão instanciada.
 *    @param string $name - Nome da sessão.
 *    @param string $value - Valor a ser atribuído.
 *    @return Se a sessão for regenerada retorna o seu conteúdo, caso contrário retorna false.
 */
function regenera_sessao($name,$value){
    destroi_sessao($name);
    gera_sessao($name,$value);
    return (!empty($_SESSION[$name])) ? $_SESSION[$name] : false ;
}


/**
 *   Converte o tamanho dos bytes em formato legível à humanos.
 *    @param int $size - Quantidade de bytes a serem convertidos.
 *    @return Número inteiro e o sufixo KB ou MB (Ex: 1MB).
 */
function formata_byte($size,$precision=2){
	$suffix = array('b','KB','MB','GB','TB','PB','EB');
	$base = log($size) / log(1024);
    return round(pow(1024, $base - floor($base)), $precision) . $suffix[floor($base)];
}

/**
 *   Descricao
 *
 */
function remove_acentos($string){
	return $string;
}
 
/**  
 *	 Sanitiza variáveis usando filtros 
 *    @param string $type null, get e post.
 *    @param string $input Variável a ser sanitizada.
 *    @param string $filter Filtro PHP a ser aplicado.
 *    @return O valor da variável requisitada.
 */
function sanitiza($type,$input,$filter=null){
	switch($type){
		case 'get': 
			$sanitized = filter_input(INPUT_GET,$input,FILTER_SANITIZE_SPECIAL_CHARS); 
			break;
		case 'post': 
			$sanitized = filter_input(INPUT_POST,$input,FILTER_SANITIZE_SPECIAL_CHARS);
			break;
		default: 
			$sanitized = addslashes(htmlspecialchars(strip_tags(trim($input))));
			break;
	}
	return $sanitized;
}


function mensagem_erro($type,$id,$params=null){
	require(DOCS_PATH.'/mensagens.php');
	switch($type){
		case 'sys':
			return $e_sistema[$id];
			break;
		case 'dep':
			return $e_deptos[$id];
			break;
		case 'doc':
			return $e_documentos[$id];
			break;
		case 'upl':
			return $e_uploads[$id];
			break;
		default:
			return $e_generico[$id];
			break;	
	}
}






























function pega_mensagem($type,$code,$params=null){
    require(DOCS_PATH.'/mensagens.php');
    
    switch($type){
    	case 'l': 
    		return $logging[$code];
    		break;
        case 'e': 
        	return $error[$code]; 
        	break;
        case 's': 
        	return $success[$code]; 
        	break;
    }
}

function gera_alerta($msg){
	gera_sessao('alert',$msg);
}

function exibe_alerta(){
    return (isset($_SESSION['alert'])) ? $_SESSION['alert'] : false ;
}

function destroi_alerta(){
	destroi_sessao('alert');
}


//
function font_icon($param=null){
	switch($param){
		case 'edit': $icon = 'icon-pencil'; break;
		case 'del': $icon = 'icon-remove'; break;
		case 'pdf': $icon = 'icon-file-pdf'; break;
		case 'doc': $icon = 'icon-file-word'; break; 
		case 'docx': $icon = 'icon-file-word'; break;
		case 'xls': $icon = 'icon-file-excel'; break;
		case 'xlsx': $icon = 'icon-file-excel'; break;
		case 'xlsm': $icon = 'icon-file-excel'; break;
		case 'ppt': $icon = 'icon-file-powerpoint'; break;
		case 'pptx': $icon = 'icon-file-powerpoint'; break;
		default: $icon = 'icon-file'; break;
		//case '': $icon = ''; break;
	}
	
	return $icon;
}

function regra_acesso($param1,$param2){
	foreach($param1 as $valor){
		if(in_array($valor, $param2) == true){
			return true;
		}
		else{
			return false;
		}
	}
}

function departamentos_aninhados($saida,$pai=1,$nivel=0){

	$conn = odbc_connect('MISPG','','');

	if($saida == 'lista'){
		$sql = "
			SELECT a.*, Deriv1.count 
			FROM docs.tbl_departamentos a 
			LEFT OUTER JOIN (SELECT pai, COUNT(*) AS count 
			FROM docs.tbl_departamentos GROUP BY pai) Deriv1 ON a.id = Deriv1.pai 
			WHERE a.status=1 AND a.pai={$pai} ORDER BY a.nome ASC";
	}

	if($saida == 'listaa'){
		$sql = "
			SELECT a.*, Deriv1.count 
			FROM docs.tbl_departamentos a 
			LEFT OUTER JOIN (SELECT pai, COUNT(*) AS count 
			FROM docs.tbl_departamentos GROUP BY pai) Deriv1 ON a.id = Deriv1.pai 
			WHERE a.status=1 AND a.pai={$pai} ORDER BY a.nome ASC";
	}

	if($saida == 'select'){
		$sql = "
			SELECT a.*, Deriv1.count 
			FROM docs.tbl_departamentos a 
			LEFT OUTER JOIN (SELECT pai, COUNT(*) AS count 
			FROM docs.tbl_departamentos GROUP BY pai) Deriv1 ON a.id = Deriv1.pai 
			WHERE a.status=1 AND a.pai={$pai} ORDER BY a.nome ASC";
	}

	if($saida == 'tabela_depto'){
		$sql = "
			SELECT a.*, Deriv1.count 
			FROM docs.tbl_departamentos a 
			LEFT OUTER JOIN (SELECT pai, COUNT(*) AS count 
			FROM docs.tbl_departamentos GROUP BY pai) Deriv1 ON a.id = Deriv1.pai 
			WHERE a.pai={$pai} ORDER BY a.nome ASC";
	}

	$exec = odbc_exec($conn,$sql); //sql_id #001 (departamentos)
	$total = odbc_num_rows($exec);
	
	if($total > 0){

		$dados = null;
		$classe == 'even';

		while($resultado = odbc_fetch_array($exec)){

			$id = $resultado['id'];
			$status = $resultado['status'];
			$nome = $resultado['nome'];
			$usuario = $resultado['usuario'];
			$atualizado = $resultado['atualizado'];
			$contagem = $resultado['count'];

			if($saida == 'lista'){
				$dados .= lista_departamentos($id,$nome,$nivel,$contagem);
			}

			if($saida == 'listaa'){
				$dados .= listaa_departamentos($id,$nome,$nivel,$contagem);
			}
			
			if($saida == 'select'){
				$dados .= selecao_departamentos($id,$nome,$nivel,$contagem);
			}
			
			if($saida == 'tabela_depto'){
				$dados .= tabela_departamentos($id,$status,$nome,$usuario,$atualizado,$nivel,$contagem);
				$classe = ($classe == 'even') ? 'odd' : 'even' ;
			}
		}
	}
	else{
		$dados = '';
	}
	
	return $dados;
}

function lista_departamentos($id,$nome,$nivel,$contagem){
	
	$li = null;
	
	if($contagem == 0){
		$li .= '<li id="dpt_'.$id.'">';
		$li .= '<h4><a href="#">'.$nome.'</a></h4>';
		$li .= lista_arquivos($id);
		$li .= '</li>';
	}
	else{
		$li .= '<li id="dpt_'.$id.'">';
		$li .= '<h4><a href="#">'.$nome.'</a></h4>';
		$li .= lista_arquivos($id);
		$li .= '<ul>';
		$li .= departamentos_aninhados('lista',$id,$nivel+1);
		$li .= '</ul>';
		$li .= '</li>';
	}
	
	return $li;
}


function listaa_departamentos($id,$nome,$nivel,$contagem){

	global $user;
	
	$li = null;
	
	if($contagem == 0){
		$li .= '<li id="dpt_'.$id.'">';
		$li .= '<h4><a href="#" onclick="populaDiv(\'http://172.23.14.155/mismobile/node/php/docs/docs_indice_ajax.php?dep='.$id.'&uid='.$user->uid.'\');">'.$nome.'</a></h4>';
		$li .= '</li>';
	}
	else{
		$li .= '<li id="dpt_'.$id.'">';
		$li .= '<h4><a href="#" onclick="populaDiv(\'http://172.23.14.155/mismobile/node/php/docs/docs_indice_ajax.php?dep='.$id.'&uid='.$user->uid.'\');">'.$nome.'</a></h4>';
		$li .= '<ul>';
		$li .= departamentos_aninhados('listaa',$id,$nivel+1);
		$li .= '</ul>';
		$li .= '</li>';
	}
	
	return $li;
}

function selecao_departamentos($id,$nome,$nivel,$contagem){
	
	$tab = '';
	$option = null;
	
	if($nivel>0){ $tab = '&nbsp&nbsp'; }
	for($i = 0; $i < $nivel;$i++){ $tab = $tab.$tab; }
	
	if($contagem == 0){
		$option = '<option value='.$id.'>'.$tab.$nome.'</option>';
	}
	else{
		$option = '<option value='.$id.'>';
		$option .= $tab.$nome;														# Nome do departamento
		$option .= '</option>';
		$option .= departamentos_aninhados('select',$id,$nivel+1);			# Aninha os departamentos
	}
	
	return $option;
}

function tabela_departamentos($id,$status,$nome,$usuario,$atualizado,$nivel,$contagem){
	
	$tab = '';
	$option = null;
	$status = ($status == 0) ? 'Desativado' : 'Ativado';
	$atualizado = date('d/m/Y', $atualizado);
	$usuario = user_load($usuario);

	if($nivel>0){ $tab = '&nbsp&nbsp&nbsp'; }
	for($i = 0; $i < $nivel;$i++){ $tab = $tab.$tab; }
	

	if($contagem == 0){
		$option = '
		<tr class="'.$classe.'">
			<td style="">'.$status.'</td>
			<td style="padding: 0 0 0 10px; text-align: left;">'.$tab.$nome.'</td>
			<td style="">'.$usuario->name.'</td>
			<td style="">'.$atualizado.'</td>
			<td><a href="?acao=editar_topico&dep='.$id.'" >Editar</a> <a href="?acao=apagar_topico&dep='.$id.'" >Apagar</a></td> 
		</tr>';
	}
	else{
		$option = '
		<tr class="'.$classe.'">
			<td style="">'.$status.'</td>
			<td style="padding: 0 0 0 10px; text-align: left;">'.$tab.$nome.'</td>
			<td style="">'.$usuario->name.'</td>
			<td style="">'.$atualizado.'</td>
			<td><a href="?acao=editar_topico&dep='.$id.'" >Editar</a> <a href="?acao=apagar_topico&dep='.$id.'" >Apagar</a></td> 
		</tr>';
		$option .= departamentos_aninhados('tabela_depto',$id,$nivel+1);			# Aninha os departamentos
	}
	
	return $option;
}


function lista_arquivos($depto_id){

	$conn = odbc_connect('MISPG','',''); 
	$query = "
		SELECT id, titulo, extensao FROM docs.tbl_documentos WHERE depto={$depto_id}";
	$exec = odbc_exec($conn,$query); //sql_id #002 (lista_arquivos)
	$total = odbc_num_rows($exec);
	
	if($total > 0){
	
		$li = null;
		
		while($resultado = odbc_fetch_array($exec)){
			$li .= '<li class="modafocka" id="arq_'.$resultado['id'].'">'; 
			$li .= '<a href="http://172.23.14.155/mismobile/node/php/docs/download.php?doc='.$resultado['id'].'" class="down-link" target="_blank">';
			$li .= '<img src="http://172.23.14.155/mismobile/themes/portal/images/icones/download.png" alt="Download do Arquivo" class="down-icon" /> <span>'.$resultado['titulo'].'</span></a>';
			$li .= '</li>';
		}
		
		$lista = '<ul id="arq_dpt_'.$depto_id.'">'.$li.'</ul>';
	}
	else{
		$lista = '<!-- Não tem arquivos aqui o/ -->';
	}
	
	return $lista;
}

















function e_message($code, $param=null, $param2=null){
	$message[1] = 'Documento solicitado não foi encontrado.';
	$message[2] = 'Para acessar este recurso você deve fornecer os parâmetros necessários.';
	$message[3] = 'Tentativa de fraude usando F5 ou o botão atualizar ao submeter um form';
	$message[4] = 'Não foi possível enviar o arquivo <strong>'.$param.'</strong>, tente novamente mais tarde!'; 
	$message[5] = 'Não foi possível atualizar o arquivo <strong>'.$param.'</strong>, tente novamente mais tarde!'; 
	$message[6] = 'Não foi possível apagar o arquivo <strong>'.$param.'</strong>, tente novamente mais tarde!';
	$message[7] = 'Não foi possível versionar o arquivo <strong>'.$param.'</strong>, tente novamente mais tarde!';
	$message[8] = 'Tipo de arquivo inválido!';
	$message[9] = 'O arquivo <strong>'.$param.'</strong> é muito grande, ele não pode ser maior que '.$param2;
	$message[10] = 'Não existem documentos no departamento selecionado.';
	//$message[] = 

	return '<div class="messages error">'.$message[$code].'</div>';
}

//
function s_message($code, $param=null){
	$message[1] = 'O arquivo <strong>'.$param.'</strong>, foi enviado com sucesso!'; 
	$message[2] = 'O arquivo <strong>'.$param.'</strong>, foi atualizado com sucesso!'; 
	$message[3] = 'O arquivo <strong>'.$param.'</strong>, foi apagado com sucesso!';
	//$message[1] = 

	return '<div class="messages status">'.$message[$code].'</div>';
}








/*
function menu_departamentos(){

	global $user;
	//$regras_usuario = $user->roles;
	//$regras_permitidas = array('administrator','docs_rw');
	
	$conn			= odbc_connect('MISPG','',''); 
	$sql 				= "SELECT id, nome, url FROM docs.tbl_departamentos ORDER By  nome ASC";
	$exec 			= odbc_exec($conn,$sql);
	$total			= odbc_num_rows($exec);
	
	if($total > 0){
	
		$li = null;
		
		while($resultado = odbc_fetch_array($exec)){
			$li .= '<li>';
			$li .= '<a href="http://172.23.14.155/mismobile/indice-documentos?area='.$resultado['id'].'">'.$resultado['nome'].'</a>';
			$li .= '</li>';
		}
		
		$lista = '<ul id="dpt_'.$depto_id.'">'.$li.'</ul>';
	}
	else{
		$lista = '';
	}
	
	return $lista;
}
*/




?>