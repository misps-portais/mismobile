<script type="text/javascript">
//<![CDATA[
	$(function(){
	
		var MenuTree = {
		collapse: function(element) {
			element.slideToggle(600);
		},

		walk: function() {

			$('h4', '#tree').each(function() {
				var $a = $(this);
				var $li = $a.parent();

				if ($a.next().is('ul')) {
					var $ul = $a.next();
					$a.click(function(e) {
						e.preventDefault();
						MenuTree.collapse($ul);
						$a.toggleClass('active');
					});
				}
			});
		}
	};

	MenuTree.walk();
});

//]]>  

	/*$(document).ready(function(){
		$('#tree a').live('click',function(){
		  	$('#retorno_ajax').load($(this).attr('href'));

  			return false;
 		});
	});*/

	function getXmlHttp(){
		if(navigator.appName == "Microsoft Internet Explorer") {
			xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		else {
			xmlHttp = new XMLHttpRequest();
		}
		return xmlHttp;
	}



	var xmlRequest = getXmlHttp();

	function populaDiv(valor){
		var url = valor;

		xmlRequest.onreadystatechange = mudancaEstado;
		xmlRequest.open('GET',url,true);
		xmlRequest.send(null);

		if(xmlRequest.readyState == 1){
			document.getElementById('retorno_ajax').innerHTML='Carregando...';
		}

		return url;
	}

	function mudancaEstado(){
		if (xmlRequest.readyState == 4){
			document.getElementById('retorno_ajax').innerHTML=xmlRequest.responseText;
		}
	}


</script>



<div class="menu-secundario">
	<?php require(DOCS_PATH.'/navigation.php');?>
</div>
<div class="documentos">
	<div class="alert-box"><?php echo exibe_alerta(); destroi_alerta(); ?></div>

	<div class="float-left esquerda">
		<ul id="tree">
			<?php echo departamentos_aninhados('listaa',1,0); ?>
		</ul>
	</div>
	<div class="float-right direita" id="retorno_ajax"></div>
	<div class="clearfix"></div>
</div><!-- / fim documento -->
