<?php


/**
 *   Realiza o processamento dos dados vindos do formulário.
 */
if($_SERVER['REQUEST_METHOD'] == 'POST'){

	// Sanitiza os dados oriundos do formulário
	$doc_id = sanitiza('post','doc');

	// Manipulando o banco de dados
	$query = "
		SELECT id,nome FROM docs.tbl_documentos WHERE id='{$doc_id}'";
	$exec = odbc_exec($conn,$query);
	$total = odbc_num_rows($exec);

	// Valida a existencia dos registros no banco
	if($total == 0){
		gera_alerta('<div class="formee-msg-error">O documento que você está procurando não foi encontrado ou não existe.</div><!-- #002 -->');
		header('Location: http://172.23.14.155/mismobile/documentos?acao=todos');
	}

	// Parâmetros
	$exec_query = true;


	/**
	 *   Executa as validações
	 */
	
	// Previne a possibilidade de enviar 2x o mesmo formulario com os mesmos dados
	if($_POST[session_id()] != $_SESSION[session_id()]){
		gera_alerta('<div class="formee-msg-error">Erro genérico ao enviar o formulário.</div><!-- ERR:003 -->');
		$exec_query = false;
	}

	// Manipula o bando de dados #again!
	$query = "
		DELETE FROM docs.tbl_documentos WHERE id='{$doc_id}'";
	$exec = odbc_exec($conn,$query);

	//
	if($exec == false){
		salva_log(mensagem_erro('sys',1,$query));
		gera_alerta('<div class="formee-msg-error">Não foi possível apagar o documento <em>'.$nome.'</em>.</div><!-- ERR:004-->');
	}
	else{
		gera_alerta('<div class="formee-msg-success">O documento <em>'.$nome.'</em> foi apagado com sucesso!</div>');
		header('Location: http://172.23.14.155/mismobile/documentos?acao=todos');
		exit;
	}

	// Regenera sessão que valida o envio do formulario
	gera_sessao(session_id(),sha1(uniqid()));

}





/**
 *   Realiza o processamento da pagina solicitada via GET.
 */

// Sanitiza presença do parâmetro 'dep' na URL
$doc_id = sanitiza('get','doc');

// Manipulando o banco de dados
$query = "
	SELECT id,nome,extensao FROM docs.tbl_documentos WHERE id='{$doc_id}'";
$exec = odbc_exec($conn,$query);
$total = odbc_num_rows($exec);

// Valida a existencia de registros no banco
if($total == 0){
	gera_alerta('<div class="formee-msg-error">O documento que você está procurando não foi encontrado ou não existe.</div><!-- ERR:001 -->');
	header('Location: http://172.23.14.155/mismobile/documentos?acao=todos');
}

// Obtem os resultados do banco
$resultado = odbc_fetch_array($exec);


?>





<div class="menu-secundario">
	<?php require(DOCS_PATH.'/navigation.php');?>
</div>
<div class="bottom">
	<div class="inner">
		<div class="documentos">
			<div class="apaga">
				<div class="info">Deseja realmente apagar o documento <strong><?php echo $resultado['nome'];?>.<?php echo $resultado['extensao'];?></strong>?</div>
				<form action="?acao=apagar&doc=<?php echo $resultado['id']; ?>" method="POST" name="apaga_documento" class="formee" onsubmit="return valida_form()" >
					<input class="left" type="submit" title="Apagar" value="Apagar" />
					<input class="left" type="button" title="Cancelar" value="Cancelar" onclick="history.go(-1);" />
					<input type="hidden" name="<?php echo session_id(); ?>" value="<?php echo gera_sessao(session_id(),sha1(uniqid())); ?>" />
					<input type="hidden" name="doc" value="<?php echo $resultado['id']; ?>" />
					<input type="hidden" name="nome" value="<?php echo $resultado['nome']; ?>" />
				</form>
			</div>
		</div><!-- / fim departamento -->

	</div><!-- / fim inner -->
</div><!-- / fim content-bottom -->