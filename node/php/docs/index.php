<?php

//
define('ENVIRONMENT', 'develop');
define('APP_PATH', 'C:\xampp\htdocs\mismobile');
define('DOCS_PATH', APP_PATH.'\node\php\docs');
ini_set('date.timezone','America/Sao_Paulo');

//
require(DOCS_PATH.'\functions.php');
(empty($_GET['acao'])) ? $rota = 404 : $rota = $_GET['acao'];


//
$conn = odbc_connect('MISPG','','');


//
if(ENVIRONMENT == 'develop'){
    error_reporting(E_ALL);
    session_start();
    header('Content-Type: text/html; charset=utf-8');
    header("Last-Modified: ".date("D, d M Y H:i:s")." GMT");
}

if(ENVIRONMENT == 'test'){
    error_reporting(0);
    global $user;

}

if(ENVIRONMENT == 'production'){
    error_reporting(0);
    global $user;
}

/**
 *   Rotas para as páginas do sistema
 *
 *    indice
 *    pesquisa
 *    download
 *    envia
 *    edita
 *    apaga
 *   
 *    novo_topico
 *    edita_topico
 *    apaga_topico
 */
switch($rota){
    case 'todos':
        require(DOCS_PATH.'\docs_indice.php');
        break;
    case 'indice_ajax':
        require (DOCS_PATH.'\docs_indice_ajax.php');
        break;
    case 'indice2': // DEPRECAR URGENTE
        require(DOCS_PATH.'\docs_indice_old.php');
        break;
    case 'download':
        require(DOCS_PATH.'\download.php');
        break;
    case 'novo':
        require(DOCS_PATH.'\docs_novo.php');
        break;
	case 'editar':
        require (DOCS_PATH.'\docs_edita.php');
        break;
	case 'apagar':
        require (DOCS_PATH.'\docs_apaga.php');
        break;
    case 'topicos':
        require(DOCS_PATH.'\depto_lista.php');
        break;
    case 'novo_topico':
        require(DOCS_PATH.'\depto_novo.php');
        break;
    case 'editar_topico':
        require(DOCS_PATH.'\depto_edita.php');
        break;
    case 'apagar_topico':
        require(DOCS_PATH.'\depto_apaga.php');
        break;

	default:
        header("HTTP/1.0 404 Not Found");
        echo 'Erro 404: Página não encontrada';
        break;
}

/**
 *   Acaba com o banco!  #ohnooo
 */
odbc_close($conn);

?>
