<?php

/**
 *   Realiza o processamento dos dados vindos do formulário.
 */
if($_SERVER['REQUEST_METHOD'] == 'POST'){

	// Sanitiza os dados oriundos do formulário
	$pai = sanitiza('post','pai');
	$nome = htmlentities(sanitiza('post','nome'));
	$status = (empty($_POST['status'])) ? 0 : sanitiza('post','status');
	$disp_menu = (empty($_POST['disp_menu'])) ? 0 : sanitiza('post','disp_menu');

	// Parâmetros
	$exec_query = true;
	$timestamp = time();
	$maquina = (empty($_SERVER["HTTP_X_FORWARDED_FOR"])) ? $_SERVER['REMOTE_ADDR'] : $_SERVER["HTTP_X_FORWARDED_FOR"];
	$usuario = $user->name;
	//$usuario = 1;

	/**
	 *   Executa as validações preliminares
	 */
	
	// Previne a possibilidade de enviar 2x o mesmo formulario com os mesmos dados
	if($_POST[session_id()] != $_SESSION[session_id()]){
		gera_alerta('<div class="formee-msg-error">Erro genérico ao enviar o formulário.</div><!-- ERR:001 -->');
		$exec_query = false;
	}

	//
	if(empty($_POST['pai'])){
		gera_alerta('<div class="formee-msg-error">Um ou mais parâmetros não foram preenchidos ao enviar o formulário.</div><!-- ERR:002 -->');
		$exec_query = false;
	}
	
	//
	if(empty($_POST['nome'])){
		gera_alerta('<div class="formee-msg-error">Um ou mais parâmetros não foram preenchidos ao enviar o formulário.</div><!-- ERR:003 -->');
		$exec_query = false;
	}

	//
	if($exec_query == true){

		// Manipula o bando de dados
		$query = "
			INSERT INTO docs.tbl_departamentos 
			VALUES (DEFAULT,'{$status}','{$pai}','{$nome}','{$disp_menu}',0,'{$usuario}','{$timestamp}','{$maquina}')";
		$exec = @odbc_exec($conn,$query);

		// Valida manipulação do banco
		if($exec == false){
			salva_log(mensagem_erro('sys',1,$query));
			gera_alerta('<div class="formee-msg-error">Não foi possível criar o tópico <em>'.$nome.'</em>.</div><!-- ERR:004 -->');
		}
		else{
			gera_alerta('<div class="formee-msg-success">O tópico <em>'.$nome.'</em> foi criado com sucesso!</div>');
		}
	}

	/**
	 *   Regenera sessão que valida o envio do formulario
	 */
	gera_sessao(session_id(),sha1(uniqid()));

}

?>



<div class="menu-secundario">
	<?php require(DOCS_PATH.'/navigation.php');?>
</div>
<div class="departamentos">
	<div class="alert-box"><?php echo exibe_alerta(); destroi_alerta(); ?></div>
	<div class="novo">
		<form action="?acao=novo_topico" method="POST" name="novo_departamento" onsubmit="return valida_form()" class="formee" accept-charset="UTF-8" >
			<div class="grid-12-12">
				<label>Nome do Tópico <em class="formee-req">*</em></label>
				<input type="text" name="nome" value="" class="formee-small" />
			</div>
			<div class="grid-12-12">
				<label>Tópico Pai <em class="formee-req">*</em></label>
				<select name="pai" class="formee-small">
					<option value="1" selected>Tópico Raiz</option>
					<option value="null">-----------</option>
					<?php echo departamentos_aninhados('select',1,0);?>
				</select>
			</div>
			<div class="grid-12-12">
				<ul class="formee-list">
	                <li><input type="checkbox" name="status" value="1" checked /> Ativo</li>
	            </ul>
			</div>
			<div class="grid-12-12">
				<input class="left" type="submit" title="Cadastrar" value="Criar" />
			</div>

			<input type="hidden" name="<?php echo session_id(); ?>" value="<?php echo gera_sessao(session_id(),sha1(uniqid())); ?>" />
		</form>
	</div>
</div><!-- / fim departamento -->