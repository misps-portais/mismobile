<?php


/**
 *   Realiza o processamento dos dados vindos do formulário.
 */
if($_SERVER['REQUEST_METHOD'] == 'POST'){

	// Sanitiza os dados oriundos do formulário
	$depto_id = sanitiza('post','dep');
	$nome = sanitiza('post','nome');

	// Manipulando o banco de dados
	$query = "SELECT id FROM docs.tbl_departamentos WHERE id='{$depto_id}'";
	$exec = odbc_exec($conn,$query);
	$total = odbc_num_rows($exec);

	// Valida a existencia dos registros no banco
	if($total == 0){
		gera_alerta('<div class="formee-msg-error">O tópico que você está procurando não foi encontrado ou não existe.</div><!-- #002 -->');
		header('Location: http://172.23.14.155/mismobile/documentos?acao=topicos');
	}

	// Parâmetros
	$exec_query = true;

	/**
	 *   Executa as validações
	 */
	
	// Previne a possibilidade de enviar 2x o mesmo formulario com os mesmos dados
	if($_POST[session_id()] != $_SESSION[session_id()]){
		gera_alerta('<div class="formee-msg-error">Erro genérico ao enviar o formulário.</div><!-- ERR:003 -->');
		$exec_query = false;
	}

	// Manipula o bando de dados
	$query = "
		DELETE FROM docs.tbl_departamentos WHERE id='{$depto_id}';
		DELETE FROM docs.tbl_departamentos WHERE pai='{$depto_id}';";
	$exec = odbc_exec($conn,$query);

	//
	if($exec == false){
		salva_log(mensagem_erro('sys',1,$query));
		gera_alerta('<div class="formee-msg-error">Não foi possível apagar o tópico <em>'.$nome.'</em>.</div><!-- ERR:004-->');
	}
	else{
		gera_alerta('<div class="formee-msg-success">O tópico <em>'.$nome.'</em> foi apagado com sucesso!</div>');
		header('Location: http://172.23.14.155/mismobile/documentos?acao=topicos');
		exit;
	}

	// Regenera sessão que valida o envio do formulario
	gera_sessao(session_id(),sha1(uniqid()));
} 





/**
 *   Realiza o processamento da pagina solicitada via GET.
 */

// Sanitiza presença do parâmetro 'dep' na URL
$depto_id = sanitiza('get','dep');

// Manipulando o banco de dados
$query = "
	SELECT dpt.*, pai.nome AS nome_pai 
	FROM docs.tbl_departamentos AS dpt 
	LEFT OUTER JOIN docs.tbl_departamentos AS pai ON pai.id=dpt.pai WHERE dpt.id='{$depto_id}'";
$exec = odbc_exec($conn,$query);
$total = odbc_num_rows($exec);

// Valida a existencia de registros no banco
if($total == 0){
	gera_alerta('<div class="formee-msg-error">O tópico que você está procurando não foi encontrado ou não existe.</div><!-- ERR:001 -->');
	header('Location: http://172.23.14.155/mismobile/documentos?acao=topicos');
}

// Obtem os resultados do banco
$resultado = odbc_fetch_array($exec);


?>


<div class="menu-secundario">
	<?php require(DOCS_PATH.'/navigation.php');?>
</div>
<div class="departamentos">
	<div class="alert-box"><?php echo exibe_alerta(); destroi_alerta(); ?></div>
	<div class="apaga">
		<div class="info">Deseja realmente apagar o tópico <strong><?php echo $resultado['nome'];?></strong>?</div>
		<form action="?acao=apagar_topico&dep=<?php echo $resultado['id']; ?>" method="POST" name="apaga_departamento" onsubmit="return valida_form()" class="formee" >
			<input class="left" type="submit" title="Apagar" value="Apagar" />
			<input class="left" type="button" title="Cancelar" value="Cancelar" onclick="history.go(-1);" />
			<input type="hidden" name="<?php echo session_id(); ?>" value="<?php echo gera_sessao(session_id(),sha1(uniqid())); ?>" />
			<input type="hidden" name="dep" value="<?php echo $resultado['id'];?>" />
			<input type="hidden" name="nome" value="<?php echo $resultado['nome'];?>" />
		</form>
	</div>
</div><!-- / fim departamento -->