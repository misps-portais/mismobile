<?php

/**
 *   Realiza o processamento dos dados vindos do formulário.
 */
if($_SERVER['REQUEST_METHOD'] == 'POST'){

	// Parâmetros iniciais
	$validacoes = true;
	$exec_query = true;

	// Parametros para upload
	$storage = DOCS_PATH.'\uploads';
	$tamanho_permitido = 10*1048576; // 1MB = 1048576 bytes
	$extensoes_permitidas = array('txt','csv','pdf','doc','docx','xls','xlsx','xlsm','ppt','pptx','mpp','mpt','jpg','jpeg','png','gif');

	//
	$timestamp = time();
	$maquina = (empty($_SERVER["HTTP_X_FORWARDED_FOR"])) ? $_SERVER['REMOTE_ADDR'] : $_SERVER["HTTP_X_FORWARDED_FOR"];
	$usuario = $user->name;

	// Dados oriundos do formulário
	$doc_id = sanitiza('post','doc');
	$titulo = sanitiza('post','titulo');
	$descricao = sanitiza('post','descr');
	$departamento = sanitiza('post','depto');

	$upload_error = $_FILES['arquivo']['error'];
	$arquivo_temporario = $_FILES['arquivo']['tmp_name'];
	$tamanho_arquivo = $_FILES['arquivo']['size'];
	$nome_base = $_FILES['arquivo']['name'];

	// 
	$up_error = array(
						'Não há nenhum erro, o arquivo foi carregado com sucesso.',
						'O arquivo enviado excede a diretiva upload_max_filesize no php.ini.',
						'O arquivo enviado excede a directiva MAX_FILE_SIZE que foi especificado no formulário HTML.',
						'O arquivo foi apenas parcialmente carregado.',
						'Nenhum arquivo foi enviado para upload.',
						'', // DEPRECATED ON PHP
						'Nenhuma pasta temporária foi encontrada.'
					);

	/**
 	 *    Executa as validações iniciais
 	 */

	// Previne a possibilidade de enviar 2x o mesmo formulario com os mesmos dados
	if($_POST[session_id()] != $_SESSION[session_id()]){
		gera_alerta('<div class="formee-msg-error">Erro genérico ao enviar o formulário.</div><!-- ERR:002 -->');
		$validacoes = false;
	}

	// Captura eventuais erros no upload do arquivo temporario
	if($upload_error > 0){
		salva_log(mensagem_erro('upl',$upload_error));
		gera_alerta('<div class="formee-msg-error">'.$up_error[$upload_error].'</div><!-- ERR:003 -->');
		$validacoes = false;
	}

	// Sucesso nas validações, continue!
	if($validacoes == true){

		// Parâmetros para realizar o upload
		$partes = pathinfo($nome_base);
		$nome = remove_acentos($partes['filename']);
		$extensao = strtolower($partes['extension']);

		$destino = $storage.'\public\\'.$doc_id.'.'.$extensao;

		$backup_origem = $destino;
		$backup_destino = $storage.'\versions\\'.date('Ymd_His').'_'.$doc_id.'.'.$extensao;


		/**
 		 *    Executa as novas validações
 		 */

		// Valida a extensão do arquivo
		if(in_array($extensao, $extensoes_permitidas) == false){
			gera_alerta('<div class="formee-msg-error">Este tipo de arquivo não é permitido por razões de segurança.</div><!-- ERR:004 -->');
			$validacoes = false;
		}

		// Valida o tamanho do arquivo
		if($tamanho_arquivo > $tamanho_permitido){
			gera_alerta('<div class="formee-msg-error">O arquivo não pode ser maior que <em>'.formata_byte($tamanho_permitido,0).'</em>.</div><!-- ERR:005 -->');
			$validacoes = false;
		}


		// Sucesso nas validações, continue!
		if($validacoes == true){

			$copia = copy($backup_origem,$backup_destino);
			$upload = move_uploaded_file($arquivo_temporario,$destino);

			// Pau no gatooo!
			if($copia == true AND $upload == true){

				// Insere os dados no banco
				$query = "
					UPDATE docs.tbl_documentos
					SET id='{$doc_id}',depto='{$departamento}',tamanho='{$tamanho_arquivo}',titulo='{$titulo}',nome='{$nome}',extensao='{$extensao}',usuario='{$usuario}',atualizado='{$timestamp}',ip='{$maquina}'
					WHERE id='{$doc_id}'";
					// docs.tbl_documentos (id, depto, tamanho, titulo, nome, extensao, descricao, usuario, atualizado, ip)
				$exec = odbc_exec($conn,$query); //sql: #upload

				// Valida a execução da query #upload
				if($exec == false){
					unlink($destino);
					salva_log(mensagem_erro('sys',1,$query));
					gera_alerta('<div class="formee-msg-error">Não foi possível atualizar o documento <em>'.$nome_base.'</em>.</div><!-- ERR:007 -->');
				}
				else{
					gera_alerta('<div class="formee-msg-success">O documento <em>'.$nome_base.'</em> foi atualizado com sucesso!</div>');
					//header('Location: http://172.23.14.155/mismobile/documentos?acao=departamentos');
					//exit;
				}

			}
			else{
				salva_log(mensagem_erro('upl',pega_mensagem('l',0)));
				gera_alerta('<div class="formee-msg-error">Não foi possível atualizar o documento <em>'.$nome_base.'</em>.</div><!-- ERR:006 -->');
			}
		}


	}


	// Regenera sessão que valida o envio do formulario
	gera_sessao(session_id(),sha1(uniqid()));

}





/**
 *   Realiza o processamento da pagina solicitada via GET.
 */

// Sanitiza presença do parâmetro 'doc' na URL
$doc_id = sanitiza('get','doc');

// Manipulando o banco de dados
$query = "
	SELECT doc.*, dpt.nome AS nome_depto, dptp.nome AS nome_depto_pai
	FROM docs.tbl_documentos AS doc
	LEFT OUTER JOIN docs.tbl_departamentos AS dpt ON dpt.id=doc.depto
	LEFT OUTER JOIN docs.tbl_departamentos AS dptp ON dptp.id=dpt.pai
	WHERE doc.id='{$doc_id}'";
$exec = odbc_exec($conn,$query);
$total = odbc_num_rows($exec);

// Valida a existencia de registros no banco
if($total == 0){
	gera_alerta('<div class="formee-msg-error">O documento que você está procurando não foi encontrado ou não existe.</div><!-- ERR:001 -->');
	header('Location: http://172.23.14.155/mismobile/documentos?acao=todos');
}

// Obtem os resultados do banco
$resultado = odbc_fetch_array($exec);

?>



<div class="menu-secundario">
	<?php require(DOCS_PATH.'/navigation.php');?>
</div>
<div class="bottom">
	<div class="inner">
		<div class="documentos">
			<div class="alert-box"><?php echo exibe_alerta(); destroi_alerta(); ?></div>

			<div class="editar">
				<!-- <h1>Editar Documento</h1>-->
				<form action="?acao=editar&doc=<?php echo $resultado['id']; ?>" method="POST" name="editar_departamento" class="formee" onsubmit="return valida_form()" enctype="multipart/form-data" accept-charset="UTF-8" >
					<div class="grid-12-12">
						<label>Arquivo <em class="formee-req">*</em></label>
						<input type="file" name="arquivo" class="formee-small" />
					</div>
					<div class="grid-12-12">
						<label>Titulo <em class="formee-req">*</em></label>
						<input type="text" name="titulo" value="<?php echo $resultado['titulo']; ?>" class="formee-small" />
					</div>
					<!--<div class="grid-12-12">
						<label>Descrição</label>
						<textarea name="descr" class="formee-small"><?php echo $resultado['descricao']; ?></textarea>
					</div>-->
					<div class="grid-12-12">
						<label>Tópico <em class="formee-req">*</em></label>
						<select name="depto" class="formee-small">
							<option value="<?php echo $resultado['depto']; ?>"><?php echo $resultado['nome_depto_pai']; ?> > <?php echo $resultado['nome_depto']; ?></option>
							<option value="null">-----------</option>
							<?php echo departamentos_aninhados('select',1,0);?>
						</select>
					</div>
					<div class="grid-12-12">
						<input class="left" type="submit" title="Enviar" value="Atualizar" />
					</div>

					<input type="hidden" name="<?php echo session_id(); ?>" value="<?php echo gera_sessao(session_id(),sha1(uniqid())); ?>" />
					<input type="hidden" name="doc" value="<?php echo $resultado['id']; ?>" />
				</form>
			</div>
		</div><!-- / fim departamento -->

	</div><!-- / fim inner -->
</div><!-- / fim content-bottom -->