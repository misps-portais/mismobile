
<?php

/**
 *   Realiza o processamento dos dados vindos do formulário.
 */
if($_SERVER['REQUEST_METHOD'] == 'POST'){
	
	// Sanitiza os dados oriundos do formulário
	$id = sanitiza('post','dep');
	$pai = sanitiza('post','pai');
	$nome = htmlentities(sanitiza('post','nome'));
	$status = (empty($_POST['status'])) ? 0 : sanitiza('post','status');
	$disp_menu = (empty($_POST['disp_menu'])) ? 0 : sanitiza('post','disp_menu');

	// Manipulando o banco de dados
	$query = "SELECT id FROM docs.tbl_departamentos WHERE id='{$id}'";
	$exec = odbc_exec($conn,$query);
	$total = odbc_num_rows($exec);

	// Valida a existencia dos registros no banco
	if($total == 0){
		gera_alerta('<div class="formee-msg-error">O tópico que você está procurando não foi encontrado ou não existe.</div><!-- ERR:002 -->');
		header('Location: http://172.23.14.155/mismobile/documentos?acao=topicos');
	}

	// Parâmetros
	$exec_query = true;
	$timestamp = time();
	$maquina = (empty($_SERVER["HTTP_X_FORWARDED_FOR"])) ? $_SERVER['REMOTE_ADDR'] : $_SERVER["HTTP_X_FORWARDED_FOR"];
	$usuario = $user->name;
	//$usuario = 1;

	/**
	 *   Executa as validações preliminares
	 */
	
	// Previne a possibilidade de enviar 2x o mesmo formulario com os mesmos dados
	if($_POST[session_id()] != $_SESSION[session_id()]){
		gera_alerta('<div class="formee-msg-error">Erro genérico ao enviar o formulário.</div><!-- ERR:003 -->');
		$exec_query = false;
	}

	//
	if(empty($_POST['pai'])){
		gera_alerta('<div class="formee-msg-error">Um ou mais parâmetros não foram preenchidos ao enviar o formulário.</div><!-- ERR:004 -->');
		$exec_query = false;
	}
	
	//
	if(empty($_POST['nome'])){
		gera_alerta('<div class="formee-msg-error">Um ou mais parâmetros não foram preenchidos ao enviar o formulário.</div><!-- ERR:005 -->');
		$exec_query = false;
	}

	//
	if($exec_query == true){

		// Manipula o bando de dados
		$query = "
			UPDATE docs.tbl_departamentos
			SET status='{$status}',pai='{$pai}',nome='{$nome}',disponivel_menu='{$disp_menu}',usuario='{$usuario}',atualizado='{$timestamp}',ip='{$maquina}'
			WHERE id={$id}";
		$exec = odbc_exec($conn,$query);

		// Valida manipulação do banco
		if($exec == false){
			salva_log(mensagem_erro('sys',1,$query));
			gera_alerta('<div class="formee-msg-error">Não foi possível atualizar o tópico <em>'.$nome.'</em>.</div><!-- ERR:006 -->');
		}
		else{
			gera_alerta('<div class="formee-msg-success">O tópico <em>'.$nome.'</em> foi criado com sucesso!</div>');
			header('Location: http://172.23.14.155/mismobile/documentos?acao=topicos');
		}
	}

	// Regenera sessão que valida o envio do formulario
	gera_sessao(session_id(),sha1(uniqid()));
}




/**
 *   Realiza o processamento da pagina solicitada via GET.
 */

// Sanitiza presença do parâmetro 'doc' na URL
$depto_id = sanitiza('get','dep');

// Manipulando o banco de dados
$query = "
	SELECT dpt.*, pai.nome AS nome_pai 
	FROM docs.tbl_departamentos AS dpt 
	LEFT OUTER JOIN docs.tbl_departamentos AS pai ON pai.id=dpt.pai WHERE dpt.id='{$depto_id}'";
$exec = odbc_exec($conn,$query);
$total = odbc_num_rows($exec);

// Valida a existencia de registros no banco
if($total == 0){
	gera_alerta('<div class="formee-msg-error">O tópico que você está procurando não foi encontrado ou não existe.</div><!-- ERR:001 -->');
	header('Location: http://172.23.14.155/mismobile/documentos?acao=topicos');
}

// Obtem os resultados do banco
$resultado = odbc_fetch_array($exec);


// Determina o nome do pai caso a id deste seja 1 (Raiz)
$resultado['nome_pai'] = ($resultado['pai'] == 1) ? $resultado['nome_pai'] = 'Tópico Raiz' : $resultado['nome_pai'] ; 


// Determina o status e a visibilidade do depto no menu para "checkar" o input
$status_checked = ($resultado['status'] == 1) ? 'checked' : '' ;
$dispmenu_checked = ($resultado['disponivel_menu'] == 1) ? 'checked' : '' ;

?>

<div class="menu-secundario">
	<?php require(DOCS_PATH.'/navigation.php');?>
</div>
<div class="departamentos">
	<div class="alert-box"><?php echo exibe_alerta(); destroi_alerta(); ?></div>
	<div class="editar">
		<!--<h1>Editar Departamento</h1>-->

		<form action="?acao=editar_topico&dep=<?php echo $resultado['id']; ?>" method="POST" name="edita_departamento" class="formee" onsubmit="return valida_form()" accept-charset="UTF-8" >
			<div class="grid-12-12">
				<label>Nome do Tópico <em class="formee-req">*</em></label>
				<input type="text" name="nome" value="<?php echo $resultado['nome'];?>" class="formee-small" />
			</div>
			<div class="grid-12-12">
				<label>Departamento Pai <em class="formee-req">*</em></label>
				<select name="pai" class="formee-small">
					<option value="<?php echo $resultado['pai'];?>"><?php echo $resultado['nome_pai'];?></option>
					<option value="null">-----------</option>
					<?php echo departamentos_aninhados('select',1,0);?>
				</select>
			</div>
			<div class="grid-12-12">
				<ul class="formee-list">
	                <li><input type="checkbox" name="status" value="1" <?php echo $status_checked;?> /> Ativo</li>
	            </ul>
			</div>
			<div class="grid-12-12">
				<ul class="formee-list">
	                 <!--<li><input type="checkbox" name="disp_menu" value="1" <?php echo $dispmenu_checked;?> /> Disponível no menu?</li> -->
	            </ul>
			</div>
			<div class="grid-12-12">
				<input class="left" type="submit" title="Atualizar" value="Atualizar" />
			</div>

			<input type="hidden" name="<?php echo session_id(); ?>" value="<?php echo gera_sessao(session_id(),sha1(uniqid())); ?>" />
			<input type="hidden" name="dep" value="<?php echo $resultado['id']; ?>" />
		</form>
	</div>
</div><!-- / fim departamento -->