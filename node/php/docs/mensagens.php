<div class="messages error">
	<h2 class="element-invisible">Error message</h2>
	Sorry, unrecognized username or password.
</div>



<div class="messages error">
	<h2 class="element-invisible">Error message</h2>
	 <ul>
	  <li>Password field is required.</li>
	  <li>Sorry, unrecognized username or password.</li>
	 </ul>
</div>

<div class="messages error">
<h2 class="element-invisible">Error message</h2>
 <ul>
  <li><em class="placeholder">Notice</em>: Undefined property: stdClass::$name in <em class="placeholder">include()</em> (line <em class="placeholder">22</em> of <em class="placeholder">C:\xampp\htdocs\mismobile\themes\portal\templates\page.tpl.php</em>).</li>
  <li>Sorry, there have been more than 5 failed login attempts for this account. It is temporarily blocked. Try again later.</li>
 </ul>
</div>


<?php
/**
 * 
 *   @author Fabio Silva
 *
 */

/**
 *    Mensagens para o log
 */
$e_sistema = array();
$e_sistema[0] = 'Unknow error.';
$e_sistema[1] = 'Could not execute the query: '.$params;
//$e_sistema[] = '';


/**
 *    Mensagens para o usuário
 */
$e_generico = array();
$e_generico[0] = 'Erro desconhecido.';
$e_generico[1] = 'Para acessar este recurso você deve fornecer os parâmetros necessários.';
$e_generico[2] = 'Erro generico ao enviar o formulario.';
$e_generico[3] = 'Um ou mais parâmetros não foram preenchidos ao enviar o formulário.';
//$e_generico[3] = '';
//$e_generico[4] = '';

$e_deptos = array();
$e_deptos[0] = 'O tópico que você está procurando não foi encontrado ou não existe.';
$e_deptos[1] = 'Não existem sub tópicos ou documentos neste tópico.';
$e_deptos[2] = 'Não foi possível cadastrar este tópico, tente novamente mais tarde!';
$e_deptos[3] = 'Não foi possível atualizar este tópico, tente novamente mais tarde!';
$e_deptos[4] = 'Não foi possível apagar este tópico, tente novamente mais tarde!';
//$e_deptos[] = '';

$e_uploads = array();
$e_uploads[0] = 'Não há nenhum erro, o arquivo foi carregado com sucesso.'; // There is no error, the file uploaded with successfull.
$e_uploads[1] = 'O arquivo enviado excede a diretiva upload_max_filesize no php.ini.'; // The uploaded file exceeds the upload_max_filesize directive in php.ini
$e_uploads[2] = 'O arquivo enviado excede a directiva MAX_FILE_SIZE que foi especificado no formulário HTML.'; //The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form
$e_uploads[3] = 'O arquivo foi apenas parcialmente carregado.'; //The uploaded file was only partially uploaded
$e_uploads[4] = 'Nenhum arquivo foi enviado para upload.'; // o file was uploaded
$e_uploads[5] = ''; // DEPRECATED ON PHP
$e_uploads[6] = 'Nenhuma pasta temporária foi encontrada.'; // Missing a temporary folder
$e_uploads[7] = 'Este tipo de arquivo não é permitido por razões de segurança';
$e_uploads[8] = 'O arquivo não pode ser maior que <strong>'.$params['tamanho_permitido'].'</strong>.';
//$e_uploads[] = '';

$e_documentos = array();
$e_documentos[0] = 'O documento que você está procurando não foi encontrado ou não existe.';
$e_documentos[1] = 'Não foi possível cadastrar este documento, tente novamente mais tarde!';
$e_documentos[2] = 'Não foi possível atualizar este documento, tente novamente mais tarde!';
$e_documentos[3] = 'Não foi possível apagar este documento, tente novamente mais tarde!';
//$e_documentos[] = '';



$logging = array(
					'O metodo move_uploaded_file() retornou false.',
					'Não foi possivel executar a query upload.',
					'Não foi possivel executar a query novo depto.',
					'Não foi possivel executar a query edita depto.',
					'Não foi possivel executar a query apaga depto.',
					'',
				);


$error = array(
				'Erro desconhecido.',
				'Para acessar este recurso você deve fornecer os parâmetros necessários.',
				'Erro no formulario!',
				'O documento que você está procurando não foi encontrado ou não existe.',
				'Não foi possível enviar o arquivo, tente novamente mais tarde!',
				'Não foi possível atualizar o arquivo, tente novamente mais tarde!',
				'Não foi possível apagar o arquivo, tente novamente mais tarde!',
				'Não foi possível versionar o arquivo, tente novamente mais tarde!',
				
				'',
        	  );

$success = array(
				  'O documento <strong>'.$params['nome_arquivo'].'</strong> foi enviado com sucesso!',
				  'O documento <strong>'.$params['nome_arquivo'].'</strong> foi atualizado com sucesso!',
				  'O documento <strong>'.$params['nome_arquivo'].'</strong> foi apagado com sucesso!',
				  'O tópico foi salvo com sucesso!',
				  'O tópico foi atualizado com sucesso!',
				  'O tópico foi removido com sucesso!',
				  '',
				);



/*
$error = array(
				'Erro desconhecido.',
				'Para acessar este recurso você deve fornecer os parâmetros necessários.',
				'Tentativa de fraude ao submeter um formulário, atualize a página e tente novamente.',
				'O tópico que você está procurando não foi encontrado ou não existe.',
				'O documento que você está procurando não foi encontrado ou não existe.',
				'O tipo do arquivo <strong>'.$params['nome_arquivo'].'</strong> não é permitido para envio.',
				'O arquivo <strong>'.$params['nome_arquivo'].'</strong> não pode ser maior que <strong>'.$params['tamanho_permitido'].'</strong>.',
				'Não foi possível enviar o arquivo <strong>'.$params['nome_arquivo'].'</strong>, tente novamente mais tarde!',
				'Não foi possível atualizar o arquivo <strong>'.$params['nome_arquivo'].'</strong>, tente novamente mais tarde!',
				'Não foi possível apagar o arquivo <strong>'.$params['nome_arquivo'].'</strong>, tente novamente mais tarde!',
				'Não foi possível versionar o arquivo <strong>'.$params['nome_arquivo'].'</strong>, tente novamente mais tarde!',
				'Não foi possível cadastrar este tópico, tente novamente mais tarde!',
				'',
        	  );
*/
?>