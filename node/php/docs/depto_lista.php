<?php

function tr_build($pai=1,$nivel=0,$estilo='even'){

	global $user;

	$query = "
		SELECT depto.id, depto.status, depto.pai, depto.nome, depto.usuario, to_char(to_timestamp(depto.atualizado),'DD/MM/YYYY HH24:MI') AS atualizado, filhos.count AS qtd_filhos, docs.count AS qtd_docs
		FROM docs.tbl_departamentos AS depto
		LEFT OUTER JOIN (SELECT pai, COUNT(*) AS count FROM docs.tbl_departamentos GROUP BY pai) AS filhos ON depto.id = filhos.pai 
		LEFT OUTER JOIN (SELECT depto, COUNT(*) AS count FROM docs.tbl_documentos GROUP BY depto) AS docs ON docs.depto = depto.id
		WHERE depto.status=1 AND depto.pai={$pai} ORDER BY depto.nome ASC";

	$conn = odbc_connect('MISPG','','');
	$exec  = odbc_exec($conn,$query);
	$total = odbc_num_rows($exec);

	if($total > 0){

		$tabela = null;

		while($resultado = odbc_fetch_array($exec)){

			$qtd_docs = ($resultado['qtd_docs'] == null) ? 0 : $resultado['qtd_docs'] ;
			$status = ($resultado['status'] == 1) ? 'Ativado' : 'Desativado' ;
			$size = ($nivel > 0) ? 30 : 10;
			for($i = 0; $i < $nivel;$i++){ $padding = $nivel*$size; }

			$tabela .= '<tr class="'.$estilo.'">';
			$tabela .= '	<td>'.$status.'</td>';
			$tabela .= '	<td style="padding-left:'.$padding.'px;text-align:left;">'.$resultado['nome'].'</td>';
			$tabela .= '	<td>'.$resultado['usuario'].'</td>';
			$tabela .= '	<td>'.$resultado['atualizado'].'</td>';

			if(in_array('administrator',$user->roles)){
				$tabela .= '<td>';
				$tabela .= '	<div class="operacoes">';
				$tabela .= '		<a class="float-left acao icn-edit" href="?acao=editar_topico&dep='.$resultado['id'].'" title="Editar"></a>';
				$tabela .= '		<a class="float-left acao icn-remove" href="?acao=apagar_topico&dep='.$resultado['id'].'"  title="Apagar"></a>';
				$tabela .= '	</div>';
				$tabela .= '</td>';
			}

			$tabela .= '</tr>';

			//if($nivel==0){
				$estilo = ($estilo == 'even') ? 'odd' : 'even' ;	
			//}

			$tabela .= tr_build($resultado['id'],$nivel+1,$estilo);
		}
	}
	else{
		$tabela = '';
	}

	return $tabela;
}

?>



<div class="menu-secundario">
	<?php require(DOCS_PATH.'/navigation.php');?>
</div>
<div class="departamentos">
	<div class="alert-box"><?php echo exibe_alerta(); destroi_alerta(); ?></div>
	<!-- <h1>Todos os Departamentos</h1> -->
	<!-- <div class="novo_depto"><a href="?acao=novo_departamento">Novo Departamento</a></div> -->
	<table>
		<thead>
			<tr>
				<th style="width: 8%;">STATUS</th>
				<th style="">NOME</th>
				<th style="width:15%;">USUÁRIO</th>
				<th style="width:15%;">ATUALIZADO</th>
				<?php if(in_array('administrator',$user->roles)){ ?>
				<th style="width:8%;">AÇÕES</th>
				<?php }?>
			</tr>
		</thead>
		<tbody>
			<?php echo tr_build();?>
		</tbody>
	</table>
</div><!-- / fim departamento -->