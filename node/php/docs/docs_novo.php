<?php

/**
 *   Realiza o processamento dos dados vindos do formulário.
 */
if($_SERVER['REQUEST_METHOD'] == 'POST'){

	// Parâmetros iniciais
	$validacoes = true;
	$exec_query = true;

	// Parametros para upload
	$storage = DOCS_PATH.'\uploads';
	$tamanho_permitido = 10*1048576; // 1MB = 1048576 bytes
	$extensoes_permitidas = array('txt','csv','pdf','doc','docx','xls','xlsx','xlsm','ppt','pptx','mpp','mpt','jpg','jpeg','png','gif','url','lnk');

	//
	$timestamp = time();
	$maquina = (empty($_SERVER["HTTP_X_FORWARDED_FOR"])) ? $_SERVER['REMOTE_ADDR'] : $_SERVER["HTTP_X_FORWARDED_FOR"];
	$usuario = $user->name;
	//$usuario = 1;


	// Dados oriundos do formulário
	$titulo = sanitiza('post','titulo');
	$descricao = sanitiza('post','descr');
	$departamento = sanitiza('post','depto');

	$upload_error = $_FILES['arquivo']['error'];
	$arquivo_temporario = $_FILES['arquivo']['tmp_name'];
	$tamanho_arquivo = $_FILES['arquivo']['size'];
	$nome_base = $_FILES['arquivo']['name'];

	//
	$up_error = array(
						'Não há nenhum erro, o arquivo foi carregado com sucesso.',
						'O arquivo enviado excede a diretiva upload_max_filesize no php.ini.',
						'O arquivo enviado excede a directiva MAX_FILE_SIZE que foi especificado no formulário HTML.',
						'O arquivo foi apenas parcialmente carregado.',
						'Nenhum arquivo foi enviado para upload.',
						'', // DEPRECATED ON PHP
						'Nenhuma pasta temporária foi encontrada.'
					);


	/**
 	 *    Executa as validações iniciais
 	 */
	// Previne a possibilidade de enviar 2x o mesmo formulario com os mesmos dados
	if($_POST[session_id()] != $_SESSION[session_id()]){
		gera_alerta('<div class="formee-msg-error">Erro genérico ao enviar o formulário.</div><!-- ERR:001 -->');
		$validacoes = false;
	}

	// Captura eventuais erros no upload do arquivo temporario
	if($upload_error > 0){
		salva_log(mensagem_erro('upl',$upload_error));
		gera_alerta('<div class="formee-msg-error">'.$up_error[$upload_error].'</div><!-- ERR:002 -->');
		$validacoes = false;
	}

	// Sucesso nas validações, continue!
	if($validacoes == true){

		// Parâmetros para realizar o upload
		$doc_id = uniqid();
		$partes = pathinfo($nome_base);
		$nome = remove_acentos($partes['filename']);
		$extensao = strtolower($partes['extension']);
		$destino = $storage.'\public\\'.$doc_id.'.'.$extensao;


		/**
 		 *    Executa as novas validações
 		 */

		// Valida a extensão do arquivo
		if(in_array($extensao, $extensoes_permitidas) == false){
			gera_alerta('<div class="formee-msg-error">Este tipo de arquivo não é permitido por razões de segurança.</div><!-- ERR:003 -->');
			$validacoes = false;
		}

		// Valida o tamanho do arquivo
		if($tamanho_arquivo > $tamanho_permitido){
			gera_alerta('<div class="formee-msg-error">O arquivo não pode ser maior que <em>'.formata_byte($tamanho_permitido,0).'</em>.</div><!-- ERR:004 -->');
			$validacoes = false;
		}


		// Sucesso nas validações, continue!
		if($validacoes == true){

			$upload = move_uploaded_file($arquivo_temporario,$destino);

			// Pau no gatooo!
			if($upload == true){

				// Insere os dados no banco
				$query = "
					INSERT INTO docs.tbl_documentos 
					VALUES ('{$doc_id}','{$departamento}','{$tamanho_arquivo}','{$titulo}','{$nome}','{$extensao}',NULL,'{$usuario}','{$timestamp}','{$maquina}')";
					// (id, depto, tamanho, titulo, nome, extensao, descricao, usuario, atualizado, ip)
				$exec = odbc_exec($conn,$query); //sql: #upload

				// Valida a execução da query #upload
				if($exec == false){
					unlink($destino);
					salva_log(mensagem_erro('sys',1,$query));
					gera_alerta('<div class="formee-msg-error">Não foi possível enviar o documento <em>'.$nome_base.'</em>.</div><!-- ERR:006 -->');
				}
				else{
					gera_alerta('<div class="formee-msg-success">O documento <em>'.$nome_base.'</em> foi enviado com sucesso!</div>');
					//header('Location: http://172.23.14.155/mismobile/documentos?acao=departamentos');
					//exit;
				}

			}
			else{
				salva_log(mensagem_erro('upl',pega_mensagem('l',0)));
				gera_alerta('<div class="formee-msg-error">Não foi possível enviar o documento <em>'.$nome_base.'</em>.</div><!-- ERR:005 -->');
			}
		}


	}


	// Regenera sessão que valida o envio do formulario
	gera_sessao(session_id(),sha1(uniqid()));


}

?>
<div class="menu-secundario">
	<?php require(DOCS_PATH.'/navigation.php');?>
</div>
<div class="bottom">
	<div class="inner">
		<div class="documentos">
			<div class="alert-box"><?php echo exibe_alerta(); destroi_alerta(); ?></div>

			<div class="novo_documento">
				<!-- <h1>Novo Documento</h1> -->
				<form action="?acao=novo" method="POST" name="novo_departamento" class="formee" onsubmit="return valida_form()" enctype="multipart/form-data" accept-charset="UTF-8" >
					<div class="grid-12-12">
						<label>Arquivo <em class="formee-req">*</em></label>
						<input type="file" name="arquivo" class="formee-small" />
					</div>
					<div class="grid-12-12">
						<label>Titulo <em class="formee-req">*</em></label>
						<input type="text" name="titulo" value="" class="formee-small" />
					</div>
					<!--<div class="grid-12-12">
						<label>Descrição</label>
						<textarea name="descr" class="formee-small"></textarea>
					</div>-->
					<div class="grid-12-12">
						<label>Tópico <em class="formee-req">*</em></label>
						<select name="depto" class="formee-small">
							<option value="null">Selecione o Tópico</option>
							<option value="null">-----------</option>
							<?php echo departamentos_aninhados('select',1,0);?>
						</select>
					</div>
					<div class="grid-12-12">
						<input class="left" type="submit" title="Enviar" value="Enviar" />
					</div>

					<input type="hidden" name="<?php echo session_id(); ?>" value="<?php echo gera_sessao(session_id(),sha1(uniqid())); ?>" />
				</form>
			</div>
		</div><!-- / fim departamento -->

	</div><!-- / fim inner -->
</div><!-- / fim content-bottom -->