<?php


/**
 *   Grava log de erros gerados por ação do usuário
 *    @param string $name - Nome da sessão.
 */
function salva_log($msg){
	//$log_file = DOCS_PATH.'\logs\errors.log';
	//error_log(date('d-m-Y H:i:s (T)')." - ".$msg."\r\n", 3, $log_file);
}



/**
 *   Instância sessões personalizadas.
 *    @param string $name - Nome da sessão.
 *    @param string $value - Valor a ser atribuído.
 *    @return Se a sessão for instanciada retorna o seu conteúdo, caso contrário retorna false.
 */
function gera_sessao($name,$value){
    $_SESSION[$name] = $value;
    return (!empty($_SESSION[$name])) ? $_SESSION[$name] : false ;
}

/**
 *   Destroi uma sessão instanciada.
 *    @param string $name - Nome da sessão a ser destruída.
 *    @return bool
 */
function destroi_sessao($name){
    unset($_SESSION[$name]);
    return (empty($_SESSION[$name])) ? true : false ;
}

/**
 *   Regenera uma sessão instanciada.
 *    @param string $name - Nome da sessão.
 *    @param string $value - Valor a ser atribuído.
 *    @return Se a sessão for regenerada retorna o seu conteúdo, caso contrário retorna false.
 */
function regenera_sessao($name,$value){
    destroi_sessao($name);
    gera_sessao($name,$value);
    return (!empty($_SESSION[$name])) ? $_SESSION[$name] : false ;
}


/**
 *   Converte o tamanho dos bytes em formato legível à humanos.
 *    @param int $size - Quantidade de bytes a serem convertidos.
 *    @return Número inteiro e o sufixo KB ou MB (Ex: 1MB).
 */
function formata_byte($size,$precision=2){
	$suffix = array('b','KB','MB','GB','TB','PB','EB');
	$base = log($size) / log(1024);
    return round(pow(1024, $base - floor($base)), $precision) . $suffix[floor($base)];
}

/**
 *   Descricao
 *
 */
function remove_acentos($string){
	return $string;
}
 
/**  
 *	 Sanitiza variáveis usando filtros 
 *    @param string $type null, get e post.
 *    @param string $input Variável a ser sanitizada.
 *    @param string $filter Filtro PHP a ser aplicado.
 *    @return O valor da variável requisitada.
 */
function sanitiza($type,$input,$filter=null){

	switch($type){
		case 'get': 
			$data = filter_input(INPUT_GET,$input,FILTER_SANITIZE_SPECIAL_CHARS);
			break;
		case 'post': 
			$data = filter_input(INPUT_POST,$input,FILTER_SANITIZE_SPECIAL_CHARS);
			break;
		default: 
			$data = addslashes(htmlspecialchars(strip_tags(trim($input))));
			break;
	}

	return $data;
}


function mensagem_erro($type,$id,$params=null){
	require(NODE_PATH.'\php\fs_mensagens.php');
	switch($type){
		case 'sys':
			return $e_sistema[$id];
			break;
		case 'dep':
			return $e_deptos[$id];
			break;
		case 'doc':
			return $e_documentos[$id];
			break;
		case 'upl':
			return $e_uploads[$id];
			break;
		default:
			return $e_generico[$id];
			break;	
	}
}


function mensagem_sucesso($type,$id,$params=null){
	require(NODE_PATH.'\php\fs_mensagens.php');
	return $s_generico[$id];
}


function gera_alerta($msg){
	gera_sessao('alert',$msg);
}

function exibe_alerta(){
    return (isset($_SESSION['alert'])) ? $_SESSION['alert'] : false ;
}

function destroi_alerta(){
	destroi_sessao('alert');
}


/**  
 *	 Retorna o nome de usuário baseado em sua uid
 *    @param int $uid ID do Usuário
 *    @return Nome de usuário.
 */
function drupal_username($uid){
	global $user;
	$user = user_load($uid);
	return $user->name;
}

//   REFATORAR URGENTE
function converte_hora($time){

	$skavurska = array('m'=>60,'h'=>3600,'d'=>86400);

	if($time >= 60 AND $time <= 3599){
		$data = floor($time/$skavurska['m']).' minuto(s)';
	}

	if($time > 3600 AND $time <= 86399){
		$data = floor($time/$skavurska['h']).' hora(s) úteis';
	}

	if($time >= 86400){
		$data = floor($time/$skavurska['d']).' dia(s) úteis';
	}

	return $data;
}

/**  
 *    Calcula todos os feriados baseado no ano
 *     @param int $ano Ano do qual se deseja obter os feriados
 *     @return array Com todos os feriados.
 */
function retorna_feriados($ano){

	$dia = 86400;
	$datas = array();
	$datas['pascoa'] = easter_date($ano);
	$datas['sexta_santa'] = $datas['pascoa'] - (2 * $dia);
	$datas['carnaval'] = $datas['pascoa'] - (47 * $dia);
	$datas['corpus_cristi'] = $datas['pascoa'] + (60 * $dia);
 
	$feriados = array(
		'01/01/'.date('Y'),
		'25/01/'.date('Y'), // Aniversário de São Paulo
		date('d/m/Y',$datas['carnaval']),
		date('d/m/Y',$datas['sexta_santa']),
		date('d/m/Y',$datas['pascoa']),
		'21/04/'.date('Y'),
		'01/05/'.date('Y'),
		date('d/m/Y',$datas['corpus_cristi']),
		'09/07/'.date('Y'), // Revolução Constitucionalista de 1932
		'07/09/'.date('Y'),
		'12/10/'.date('Y'),
		'02/11/'.date('Y'),
		'15/11/'.date('Y'),
		'20/11/'.date('Y'), // Dia da Consciência Negra
		'25/12/'.date('Y')
	);
	return $feriados;
}

function data_final_sla($sla,$data){
	/**
	 *    Para indicar uma data/hora de inicio, passe ela no construtor, ex:
	 *    $date = new DateTime('2013-07-08 09:00:00');
	 */
	$date = new DateTime($data);
	 
	// prazo, em horas se precisar especificar em minutos, coloque valores quebrados, como 15.5
	$prazo = $sla; 
	 
	// inicio/fim do expediente (já transformando em minutos)
	$inicioExpediente = 8*60;
	$fimExpediente = 18*60;
	 
	// feriados
	$feriados = retorna_feriados(date('Y'));
	 
	// dias encontrados para trampo
	$diasUteis = array();
	 
	// convertemos o prazo para minutos
	$prazoMinutos = $prazo*60;
	 
	// enquanto for maior que zero
	while( $prazoMinutos > 0 ){
		// transformamos a hora atual em minutos
		$hora = ($date->format('H') * 60) + $date->format('i');
	 
		// se for menor que a hora do inicio do expediente
		if($hora < $inicioExpediente){
	 		// colocamos igual a hora do expediente
	 		$date->setTime(0, $inicioExpediente, 0);
			continue;
		}
	 
		// data calculada
		$data = $date->format('d/m/Y');
	 
		// se 
		// - for um feriado OU
		// - passar da hora do expediente OU
		// - for um dia de fim de semana (sabado|domingo)
		// vamos para o dia seguinte, no inicio do expediente
		if(in_array($data,$feriados) || $hora >= $fimExpediente || $date->format('w') == 0 || $date->format('w') == 6){
			$date->modify('+1 day');
			$date->setTime(0, $inicioExpediente, 0);
			continue;
		}
	 
		// se chegou aqui, é um dia util.
		// vamos ver se já está na nossa lista de dias
		// se não estiver, colocamos
		if(!in_array($data, $diasUteis)){
			$diasUteis[] = $data;
		}
	 
		// minutos que temos que acrescentar para chegar no
		// fim do expediente de hoje
		$minutos = $fimExpediente - $hora;
	 
		// tiramos do prazo
		$prazoMinutos -= $minutos;
	 
		// se estourou
		if($prazoMinutos < 0){
	 		// tiramos o que estourou
	 		$minutos += $prazoMinutos;
	 	}
	 	// adicionamos os minutos do calculo na data
	 	$date->modify('+' . $minutos . ' minute');
	}
	 
	$prazoFinal = $date->format('d/m/Y H:i:s');
	 
	return $prazoFinal;
	//print_r($diasUteis);
}




























function departamentos_aninhados($saida,$pai=1,$nivel=0){

	$conn = odbc_connect('MISPG','','');

	if($saida == 'lista'){
		$sql = "
			SELECT a.*, Deriv1.count 
			FROM docs.tbl_departamentos a 
			LEFT OUTER JOIN (SELECT pai, COUNT(*) AS count 
			FROM docs.tbl_departamentos GROUP BY pai) Deriv1 ON a.id = Deriv1.pai 
			WHERE a.status=1 AND a.pai={$pai} ORDER BY a.nome ASC";
	}

	if($saida == 'listaa'){
		$sql = "
			SELECT a.*, Deriv1.count 
			FROM docs.tbl_departamentos a 
			LEFT OUTER JOIN (SELECT pai, COUNT(*) AS count 
			FROM docs.tbl_departamentos GROUP BY pai) Deriv1 ON a.id = Deriv1.pai 
			WHERE a.status=1 AND a.pai={$pai} ORDER BY a.nome ASC";
	}

	if($saida == 'select'){
		$sql = "
			SELECT a.*, Deriv1.count 
			FROM docs.tbl_departamentos a 
			LEFT OUTER JOIN (SELECT pai, COUNT(*) AS count 
			FROM docs.tbl_departamentos GROUP BY pai) Deriv1 ON a.id = Deriv1.pai 
			WHERE a.status=1 AND a.pai={$pai} ORDER BY a.nome ASC";
	}

	if($saida == 'tabela_depto'){
		$sql = "
			SELECT a.*, Deriv1.count 
			FROM docs.tbl_departamentos a 
			LEFT OUTER JOIN (SELECT pai, COUNT(*) AS count 
			FROM docs.tbl_departamentos GROUP BY pai) Deriv1 ON a.id = Deriv1.pai 
			WHERE a.pai={$pai} ORDER BY a.nome ASC";
	}

	$exec = odbc_exec($conn,$sql); //sql_id #001 (departamentos)
	$total = odbc_num_rows($exec);
	
	if($total > 0){

		$dados = null;

		while($resultado = odbc_fetch_array($exec)){

			$id = $resultado['id'];
			$status = $resultado['status'];
			$nome = $resultado['nome'];
			$usuario = $resultado['usuario'];
			$atualizado = $resultado['atualizado'];
			$contagem = $resultado['count'];

			if($saida == 'lista'){
				$dados .= lista_departamentos($id,$nome,$nivel,$contagem);
			}

			if($saida == 'listaa'){
				$dados .= listaa_departamentos($id,$nome,$nivel,$contagem);
			}
			
			if($saida == 'select'){
				$dados .= selecao_departamentos($id,$nome,$nivel,$contagem);
			}
			
			if($saida == 'tabela_depto'){
				$dados .= tabela_departamentos($id,$status,$nome,$usuario,$atualizado,$nivel,$contagem);
			}
		}
	}
	else{
		$dados = '';
	}
	
	return $dados;
}

function lista_departamentos($id,$nome,$nivel,$contagem){
	
	$li = null;
	
	if($contagem == 0){
		$li .= '<li id="dpt_'.$id.'">';
		$li .= '<h4><a href="#">'.$nome.'</a></h4>';
		$li .= lista_arquivos($id);
		$li .= '</li>';
	}
	else{
		$li .= '<li id="dpt_'.$id.'">';
		$li .= '<h4><a href="#">'.$nome.'</a></h4>';
		$li .= lista_arquivos($id);
		$li .= '<ul>';
		$li .= departamentos_aninhados('lista',$id,$nivel+1);
		$li .= '</ul>';
		$li .= '</li>';
	}
	
	return $li;
}


function listaa_departamentos($id,$nome,$nivel,$contagem){

	global $user;
	
	$li = null;
	
	if($contagem == 0){
		$li .= '<li id="dpt_'.$id.'">';
		$li .= '<h4><a href="#" onclick="populaDiv(\'http://172.23.14.155/mismobile/node/php/docs/docs_indice_ajax.php?dep='.$id.'&uid='.$user->uid.'\');">'.$nome.'</a></h4>';
		$li .= '</li>';
	}
	else{
		$li .= '<li id="dpt_'.$id.'">';
		$li .= '<h4><a href="#" onclick="populaDiv(\'http://172.23.14.155/mismobile/node/php/docs/docs_indice_ajax.php?dep='.$id.'&uid='.$user->uid.'\');">'.$nome.'</a></h4>';
		$li .= '<ul>';
		$li .= departamentos_aninhados('listaa',$id,$nivel+1);
		$li .= '</ul>';
		$li .= '</li>';
	}
	
	return $li;
}

function selecao_departamentos($id,$nome,$nivel,$contagem){
	
	$tab = '';
	$option = null;
	
	if($nivel>0){ $tab = '&nbsp&nbsp'; }
	for($i = 0; $i < $nivel;$i++){ $tab = $tab.$tab; }
	
	if($contagem == 0){
		$option = '<option value='.$id.'>'.$tab.$nome.'</option>';
	}
	else{
		$option = '<option value='.$id.'>';
		$option .= $tab.$nome;														# Nome do departamento
		$option .= '</option>';
		$option .= departamentos_aninhados('select',$id,$nivel+1);			# Aninha os departamentos
	}
	
	return $option;
}

function tabela_departamentos($id,$status,$nome,$usuario,$atualizado,$nivel,$contagem){
	
	$tab = '';
	$option = null;
	$status = ($status == 0) ? 'Desativado' : 'Ativado';
	$atualizado = date("d-m-Y - H:i:s", $atualizado);
	$usuario = user_load($usuario);

	if($nivel>0){ $tab = '&nbsp&nbsp&nbsp'; }
	for($i = 0; $i < $nivel;$i++){ $tab = $tab.$tab; }
	
	if($contagem == 0){
		$option = '
		<tr>
			<td style="">'.$status.'</td>
			<td style="padding: 0 0 0 10px; text-align: left;">'.$tab.$nome.'</td>
			<td style="">'.$usuario->name.'</td>
			<td style="">'.$atualizado.'</td>
			<td><a href="?acao=editar_topico&dep='.$id.'" >Editar</a> <a href="?acao=apagar_topico&dep='.$id.'" >Apagar</a></td> 
		</tr>';
	}
	else{
		$option = '
		<tr>
			<td style="">'.$status.'</td>
			<td style="padding: 0 0 0 10px; text-align: left;">'.$tab.$nome.'</td>
			<td style="">'.$usuario->name.'</td>
			<td style="">'.$atualizado.'</td>
			<td><a href="?acao=editar_topico&dep='.$id.'" >Editar</a> <a href="?acao=apagar_topico&dep='.$id.'" >Apagar</a></td> 
		</tr>';
		$option .= departamentos_aninhados('tabela_depto',$id,$nivel+1);			# Aninha os departamentos
	}
	
	return $option;
}


function lista_arquivos($depto_id){

	$conn = odbc_connect('MISPG','',''); 
	$query = "
		SELECT id, titulo, extensao FROM docs.tbl_documentos WHERE depto={$depto_id}";
	$exec = odbc_exec($conn,$query); //sql_id #002 (lista_arquivos)
	$total = odbc_num_rows($exec);
	
	if($total > 0){
	
		$li = null;
		
		while($resultado = odbc_fetch_array($exec)){
			$li .= '<li class="modafocka" id="arq_'.$resultado['id'].'">'; 
			$li .= '<a href="http://172.23.14.155/mismobile/node/php/docs/download.php?doc='.$resultado['id'].'" class="down-link" target="_blank">';
			$li .= '<img src="http://172.23.14.155/mismobile/themes/portal/images/icones/download.png" alt="Download do Arquivo" class="down-icon" /> <span>'.$resultado['titulo'].'</span></a>';
			$li .= '</li>';
		}
		
		$lista = '<ul id="arq_dpt_'.$depto_id.'">'.$li.'</ul>';
	}
	else{
		$lista = '<!-- Não tem arquivos aqui o/ -->';
	}
	
	return $lista;
}


?>