
<!DOCTYPE HTML>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<html>
<head>

		<title>Médias Gerencia IV</title>
 
		<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
		<script src="js/highcharts.js"></script>
<script type="text/javascript">

$(function () {
    var chart;
    $(document).ready(function() {
        chart = new Highcharts.Chart({
            chart: {
                renderTo: 'container'
            },
            title: {
                text: 'Média da Gerencia'
            },
			
            xAxis: {

				
                categories: []
            },
            tooltip: {
                formatter: function() {
                    var s;
                    if (this.point.name) { // the pie chart
                        s = ''+
                            this.point.name +': '+ this.y +' Médias';
                    } else {
                        s = ''+
                            this.x  +': '+ this.y;
                    }
                    return s;
                }
            },
			
            series: [{
                type: 'line',
                name: 'Média Mês',
                data: [1,97,57,97,1,100,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
            }, {	
									
               type: 'spline',
                name: 'Meta',
                data: [1,97,57,97,1,100,1,50]
            },{	
									
               type: 'line',
                name: 'Média Diretoria',
                data: [1,97,57,97,1,100,1,97.870,0]
            }, {
                type: 'pie',
                name: 'Total consumption',
                data: [{
                    name: '',
                    y: 0,
                    color: '#4572A7' 
                }, {
                    name: '',
                    y: 0,
                    color: '#AA4643' 
                }, {
                    name: '',
                    y: 0,
                    color: '#89A54E' 
                }],
                center: [0, 0],
                size: 0,
                showInLegend: false,
                dataLabels: {
                    enabled: false

                }
            }]
        });
    });
    
});
		</script>
</head>
	<body>

 
<div id="container" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
<div id="container2" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
 
	</body>
</html>