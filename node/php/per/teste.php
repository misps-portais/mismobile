<!doctype html>
 
<html lang="pt_br">
<head>
   <title>Performance - Atendidas vs Abandonadas</title>
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
  <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
  <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
  
  <script src="js/highcharts.js"></script>
<style type="text/css">

#ns {
	position:absolute;
	width:200px;
	height:115px;
	z-index:2;
	top: 342px;
	left: 399px;
}
#rece {
	position:absolute;
	width:200px;
	height:115px;
	z-index:2;
	top: 595px;
	left: 399px;
}
#tmo {
	position:absolute;
	width:200px;
	height:115px;
	z-index:2;
	top: 848px;
	left: 399px;
}
#abdh {
	position:absolute;
	width:200px;
	height:115px;
	z-index:1;
	left: 790px;
	top: 89px;
}
#nsh {
	position:absolute;
	width:262px;
	height:115px;
	z-index:1;
	left: 790px;
	top: 342px;
}
#receh {
	position:absolute;
	width:262px;
	height:115px;
	z-index:1;
	left: 790px;
	top: 595px;
}
#tmoh {
	position:absolute;
	width:262px;
	height:115px;
	z-index:1;
	left: 790px;
	top: 848px;
}
#abdm {
	position:absolute;
	width:262px;
	height:115px;
	z-index:1;
	left: 8px;
	top: 89px;
}
#nsm {
	position:absolute;
	width:262px;
	height:115px;
	z-index:1;
	left: 8px;
	top: 342px;
}
#recem {
	position:absolute;
	width:262px;
	height:115px;
	z-index:1;
	left: 8px;
	top: 595px;
}
#tmom {
	position:absolute;
	width:262px;
	height:115px;
	z-index:1;
	left: 8px;
	top: 848px;
}
#menu {
	position:absolute;
	width:783px;
	height:57px;
	z-index:3;
	left: 8px;
	top: 0px;
}

  .column { width: 1000px; float: left; padding-bottom: 300px; top:20px;}
  .portlet { margin: 0 1em 1em 0; }
  .portlet-header { margin: 0.3em; padding-bottom: 4px; padding-left: 0.2em; }
  .portlet-header .ui-icon { float: right; }
  .portlet-content { padding: 0.4em; }
  .ui-sortable-placeholder { border: 1px dotted black; visibility: visible !important; height: 50px !important; }
  .ui-sortable-placeholder * { visibility: hidden; }
</style>
<script>
  $(function() {
    $( ".column" ).sortable({
      connectWith: ".column"
    });
 
    $( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" )
      .find( ".portlet-header" )
        .addClass( "ui-widget-header ui-corner-all" )
        .prepend( "<span class='ui-icon ui-icon-minusthick'></span>")
        .end()
      .find( ".portlet-content" );
 
    $( ".portlet-header .ui-icon" ).click(function() {
      $( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
      $( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
    });
 
    $( ".column" ).disableSelection();
  });
  </script>
<body>

	
    
<?php

error_reporting(0);
$gra = $_POST['graf'];

if (empty($gra)){ 
  $gra = "area";
}

$id_skill = $_POST["skill"]; 
$mes = $_POST["mes"];
$dia = $_POST["dia"];

$blevisky = array(
"Jan"=>1,
"Fev"=>2,
"Mar"=>3,
"Abr"=>4,
"Mai"=>5,
"Jun"=>6,
"Jul"=>7,
"Ago"=>8,
"Set"=>9,
"Out"=>10,
"Nov"=>11,
"Dez"=>12
);

$mes = $blevisky[$mes];

			$conn=odbc_connect('MISPG','','');	
			
			$sql = "
								SELECT * FROM proc_performance_tela1(2,$id_skill,$mes,'2000-01-01')

			";
			
			//Laço abandono diario
			$rs=odbc_exec($conn,$sql);
			
			while(odbc_fetch_row($rs)){

					$rec = odbc_result($rs,'abd_perc');
							
			$seriest_str = $seriest_str.$rec.",";
			
			}	
			
			$seriest_str = substr($seriest_str,0,strlen($seriest_str)-1)."]";
			
			
			//Laço nivel de serviço diario
			$rs=odbc_exec($conn,$sql);
			
			while(odbc_fetch_row($rs)){

					$rec = odbc_result($rs,'ns60');
							
			$seriestns_str = $seriestns_str.$rec.",";
			$seriestnsd_str = $seriestnsd_str."'".$recd."'".",";
			
			}	
			
			$seriestns_str = substr($seriestns_str,0,strlen($seriestns_str)-1)."]";
			$seriestnsd_str = substr($seriestnsd_str,0,strlen($seriestnsd_str)-1)."]";
			
			//Laço nivel de contatos diario
			$rs=odbc_exec($conn,$sql);
			
			while(odbc_fetch_row($rs)){

					$rec = odbc_result($rs,'rec');
					$recd = odbc_result($rs,'periodo');
							
			$seriestrece_str = $seriestrece_str.$rec.",";
			$seriestreced_str = $seriestreced_str."'".$recd."'".",";
			}	
			
			$seriestrece_str = substr($seriestrece_str,0,strlen($seriestrece_str)-1)."]";
			$seriestreced_str = substr($seriestreced_str,0,strlen($seriestreced_str)-1)."]";
			
			//Laço TMO diario
			$rs=odbc_exec($conn,$sql);
			
			while(odbc_fetch_row($rs)){

					$rec = odbc_result($rs,'tmo');
					$recd = odbc_result($rs,'row_date');
							
			$seriesttmo_str = $seriesttmo_str.$rec.",";
			
			}	
			
			$seriesttmo_str = substr($seriesttmo_str,0,strlen($seriesttmo_str)-1)."]";
			
			//Fim do laço diario
			
			$sqlh = "
							SELECT * FROM proc_performance_tela1(3,$id_skill,".$mes.",'".$dia."')
							
							";
							
			//Laço Abandono intra hora
			$rs=odbc_exec($conn,$sqlh);
			
			while(odbc_fetch_row($rs)){

					$rech = odbc_result($rs,'abd_perc');
					$recd = odbc_result($rs,'periodo');
							
			$seriestabdh_str = $seriestabdh_str.$rech.",";
			$seriestabdhd_str = $seriestabdhd_str."'".$recd."'".",";
			
			}	
			
			$seriestabdh_str = substr($seriestabdh_str,0,strlen($seriestabdh_str)-1)."]";
			$seriestabdhd_str = substr($seriestabdhd_str,0,strlen($seriestabdhd_str)-1)."]";
			
			//Laço nivel de serviço intra hora
			
			$rs=odbc_exec($conn,$sqlh);
			
			while(odbc_fetch_row($rs)){

					$rech = odbc_result($rs,'ns60');
					$seriesnsh_str = $seriesnsh_str.$rech.",";
					}
					$seriesnsh_str = substr($seriesnsh_str,0,strlen($seriesnsh_str)-1)."]";
					
			//Laço volume de contatos intra hora
			
			$rs=odbc_exec($conn,$sqlh);
			
			while(odbc_fetch_row($rs)){

					$rech = odbc_result($rs,'rec');
					$seriesreceh_str = $seriesreceh_str.$rech.",";
					}
					$seriesreceh_str = substr($seriesreceh_str,0,strlen($seriesreceh_str)-1)."]";
					
			//Laço tmo intra hora
			
			$rs=odbc_exec($conn,$sqlh);
			
			while(odbc_fetch_row($rs)){

					$rech = odbc_result($rs,'tmo');
					$seriestmoh_str = $seriestmoh_str.$rech.",";
					}
					$seriestmoh_str = substr($seriestmoh_str,0,strlen($seriestmoh_str)-1)."]";
					

					// graph mensal
					
					$sqlm = "
							SELECT * FROM proc_performance_tela1(1,$id_skill,0,'2000-01-01')
					";
					
			//Laço Abandono mes
			
			$rs=odbc_exec($conn,$sqlm);
			
			while(odbc_fetch_row($rs)){

					$rech = odbc_result($rs,'abd_perc');
					$recd = odbc_result($rs,'periodo');
					
			$seriestabdm_str = $seriestabdm_str.$rech.",";
			$seriestabdmd_str = $seriestabdmd_str."'".$recd."'".",";
			}	
			$seriestabdm_str = substr($seriestabdm_str,0,strlen($seriestabdm_str)-1)."]";
			$seriestabdmd_str = substr($seriestabdmd_str,0,strlen($seriestabdmd_str)-1)."]";
			
			//Laço nivel de serviço mes
			$rs=odbc_exec($conn,$sqlm);
			
			while(odbc_fetch_row($rs)){

			$rech = odbc_result($rs,'ns60');
			$seriesnsm_str = $seriesnsm_str.$rech.",";
			}
			$seriesnsm_str = substr($seriesnsm_str,0,strlen($seriesnsm_str)-1)."]";
			
			//Laço volume de contatos mes
			$rs=odbc_exec($conn,$sqlm);
			
			while(odbc_fetch_row($rs)){

			$rech = odbc_result($rs,'rec');
			$seriesrecem_str = $seriesrecem_str.$rech.",";
			}
			$seriesrecem_str = substr($seriesrecem_str,0,strlen($seriesrecem_str)-1)."]";
			
			//Laço TMO mes
			$rs=odbc_exec($conn,$sqlm);
			
			while(odbc_fetch_row($rs)){

			$rech = odbc_result($rs,'tmo');
			$seriestmom_str = $seriestmom_str.$rech.",";
			}
			$seriestmom_str = substr($seriestmom_str,0,strlen($seriestmom_str)-1)."]";
			
?>
<script type="text/javascript">
$(function () {
        $('#abd').highcharts({
            chart: {
                zoomType: 'x'
            },
            title: {
                text: '' 
            },
            xAxis: [{
                categories: [<?php echo $seriestreced_str; ?>,
				labels: {
                rotation: 300
            }
            }],
            yAxis: [{ // Primary yAxis
				lineWidth: 1,
				gridLineDashStyle: 'dot',
                labels: {
                    formatter: function() {
                        return this.value;
                    },
                    style: {
                        color: '#89A54E'
                    }
                },
                title: {
                    text: ''
                },
                opposite: true
    
            }, { // Secondary yAxis
                gridLineWidth: 0,
                title: {
                    text: '',
                    style: {
                        color: '#4572A7'
                    }
                },
                labels: {
                    formatter: function() {
                        return this.value +' mm';
                    },
                    style: {
                        color: '#4572A7'
                    }
                }
    
            }, { // Tertiary yAxis
                gridLineWidth: 0,
                title: {
                    text: '',
                    style: {
                        color: '#AA4643'
                    }
                },
                labels: {
                    formatter: function() {
                        return this.value +' mb';
                    },
                    style: {
                        color: '#AA4643'
                    }
                },
                opposite: true
            }],
            tooltip: {
                shared: true
            },
           
            series:[{
            name :'Realizado',
            color: '#4682B4',
            type: '<?php echo $gra ?>',
            data:[<?php echo $seriest_str; ?>,
			marker: {
									enabled: false
								},
								dashStyle: 'Solid',
								tooltip: {
									valueSuffix: ' '
								}
         },{    
                
                name: 'Planejado',
                color: '#CD2626',
                type: 'line',
                data:[<?php echo $seriest_str; ?>,
				marker: {
									enabled: false
								},
								dashStyle: 'Solid',
								tooltip: {
									valueSuffix: ' '
								}
        }]
});
});

</script>

<script type="text/javascript">
$(function () {
        $('#ns').highcharts({
            chart: {
                zoomType: 'x'
            },
            title: {
                text: ''
            },
                xAxis: [{
                categories: [<?php echo $seriestreced_str; ?>,
				labels: {
                rotation: 300
            }
            }],
            yAxis: [{ // Primary yAxis
				lineWidth: 1,
				gridLineDashStyle: 'dot',
                labels: {
                    formatter: function() {
                        return this.value;
                    },
                    style: {
                        color: '#89A54E'
                    }
                },
                title: {
                    text: ''                    
                },
                opposite: true
    
            }, { // Secondary yAxis
                gridLineWidth: 0,
                title: {
                    text: '',
                    style: {
                        color: '#4572A7'
                    }
                },
                labels: {
                    formatter: function() {
                        return this.value +' mm';
                    },
                    style: {
                        color: '#4572A7'
                    }
                }
    
            }, { // Tertiary yAxis
                gridLineWidth: 0,
                title: {
                    text: '',
                    style: {
                        color: '#AA4643'
                    }
                },
                labels: {
                    formatter: function() {
                        return this.value +' mb';
                    },
                    style: {
                        color: '#AA4643'
                    }
                },
                opposite: true
            }],
            tooltip: {
                shared: true
            },

            series:[{
            name :'Realizado',
            color: '#4682B4',
            type: '<?php echo $gra ?>',
            data:[<?php echo $seriestns_str; ?>,
			marker: {
									enabled: false
								},
								dashStyle: 'Solid',
								tooltip: {
									valueSuffix: ' '
								}
         },{    
                
                name: 'Planejado',
                color: '#CD2626',
                type: 'line',
                data:[<?php echo $seriestns_str; ?>,
				marker: {
									enabled: false
								},
								dashStyle: 'Solid',
								tooltip: {
									valueSuffix: ' '
								}
        }]
});
});

</script>

<script type="text/javascript">
$(function () {
        $('#rece').highcharts({
            chart: {
                zoomType: 'x'
            },
            title: {
                text: ''
            },
                xAxis: [{
                categories: [<?php echo $seriestreced_str; ?>,
				labels: {
                rotation: 300
            }
            }],
            yAxis: [{ // Primary yAxis
				lineWidth: 1,
				gridLineDashStyle: 'dot',
                labels: {
                    formatter: function() {
                        return this.value;
                    },
                    style: {
                        color: '#89A54E'
                    }
                },
                title: {
                    text: ''                    
                },
                opposite: true
    
            }, { // Secondary yAxis
                gridLineWidth: 0,
                title: {
                    text: '',
                    style: {
                        color: '#4572A7'
                    }
                },
                labels: {
                    formatter: function() {
                        return this.value +' mm';
                    },
                    style: {
                        color: '#4572A7'
                    }
                }
    
            }, { // Tertiary yAxis
                gridLineWidth: 0,
                title: {
                    text: '',
                    style: {
                        color: '#AA4643'
                    }
                },
                labels: {
                    formatter: function() {
                        return this.value +' mb';
                    },
                    style: {
                        color: '#AA4643'
                    }
                },
                opposite: true
            }],
            tooltip: {
                shared: true
            },

            series:[{
            name :'Realizado',
            color: '#4682B4',
            type: '<?php echo $gra ?>',
            data:[<?php echo $seriestrece_str; ?>,
			marker: {
									enabled: false
								},
								dashStyle: 'Solid',
								tooltip: {
									valueSuffix: ' '
								}
         },{    
                
                name: 'Planejado',
                color: '#CD2626',
                type: 'line',
                data:[<?php echo $seriestrece_str; ?>,
				marker: {
									enabled: false
								},
								dashStyle: 'Solid',
								tooltip: {
									valueSuffix: ' '
								}
        }]
});
});

</script>

<script type="text/javascript">
$(function () {
        $('#tmo').highcharts({
            chart: {
                zoomType: 'x'
            },
            title: {
                text: ''
            },
            xAxis: [{
                categories: [<?php echo $seriestreced_str; ?>,
				labels: {
                rotation: 300
            }
            }],
            yAxis: [{ // Primary yAxis
				lineWidth: 1,
				gridLineDashStyle: 'dot',
                labels: {
                    formatter: function() {
                        return this.value;
                    },
                    style: {
                        color: '#89A54E'
                    }
                },
                title: {
                    text: ''                    
                },
                opposite: true
    
            }, { // Secondary yAxis
                gridLineWidth: 0,
                title: {
                    text: '',
                    style: {
                        color: '#4572A7'
                    }
                },
                labels: {
                    formatter: function() {
                        return this.value +' mm';
                    },
                    style: {
                        color: '#4572A7'
                    }
                }
    
            }, { // Tertiary yAxis
                gridLineWidth: 0,
                title: {
                    text: '',
                    style: {
                        color: '#AA4643'
                    }
                },
                labels: {
                    formatter: function() {
                        return this.value +' mb';
                    },
                    style: {
                        color: '#AA4643'
                    }
                },
                opposite: true
            }],
            tooltip: {
                shared: true
            },

            series:[{
            name :'Realizado',
            color: '#4682B4',
            type: '<?php echo $gra ?>',
            data:[<?php echo $seriesttmo_str; ?>,
			marker: {
									enabled: false
								},
								dashStyle: 'Solid',
								tooltip: {
									valueSuffix: ' '
								}
         },{    
                
                name: 'Planejado',
                color: '#CD2626',
                type: 'line',
                data:[<?php echo $seriesttmo_str; ?>,
				marker: {
									enabled: false
								},
								dashStyle: 'Solid',
								tooltip: {
									valueSuffix: ' '
								}
        }]
});
});

</script>

<script type="text/javascript">
$(function () {
        $('#abdh').highcharts({
            chart: {
                zoomType: 'x'
            },
            title: {
                text: ''
            },
            xAxis: [{
                categories: [<?php echo $seriestabdhd_str; ?>,
				labels: {
                rotation: 300
            }
            }],
            yAxis: [{ // Primary yAxis
				lineWidth: 1,
				gridLineDashStyle: 'dot',
                labels: {
                    formatter: function() {
                        return this.value;
                    },
                    style: {
                        color: '#89A54E'
                    }
                },
                title: {
                    text: ''                    
                },
                opposite: true
    
            }, { // Secondary yAxis
                gridLineWidth: 0,
                title: {
                    text: '',
                    style: {
                        color: '#4572A7'
                    }
                },
                labels: {
                    formatter: function() {
                        return this.value +' mm';
                    },
                    style: {
                        color: '#4572A7'
                    }
                }
    
            }, { // Tertiary yAxis
                gridLineWidth: 0,
                title: {
                    text: '',
                    style: {
                        color: '#AA4643'
                    }
                },
                labels: {
                    formatter: function() {
                        return this.value +' mb';
                    },
                    style: {
                        color: '#AA4643'
                    }
                },
                opposite: true
            }],
            tooltip: {
                shared: true
            },

            series:[{
            name :'Realizado',
            color: '#4682B4',
            type: '<?php echo $gra ?>',
            data:[<?php echo $seriestabdh_str; ?>,
			marker: {
									enabled: false
								},
								dashStyle: 'Solid',
								tooltip: {
									valueSuffix: ' '
								}
         },{    
                
                name: 'Planejado',
                color: '#CD2626',
                type: 'line',
                data:[<?php echo $seriestabdh_str; ?>,
				marker: {
									enabled: false
								},
								dashStyle: 'Solid',
								tooltip: {
									valueSuffix: ' '
								}
        }]
});
});

</script>

<script type="text/javascript">
$(function () {
        $('#nsh').highcharts({
            chart: {
                zoomType: 'x'
            },
            title: {
                text: ''
            },
            xAxis: [{
                categories: [<?php echo $seriestabdhd_str; ?>,
				labels: {
                rotation: 300
            }
            }],
            yAxis: [{ // Primary yAxis
				lineWidth: 1,
				gridLineDashStyle: 'dot',
                labels: {
                    formatter: function() {
                        return this.value;
                    },
                    style: {
                        color: '#89A54E'
                    }
                },
                title: {
                    text: ''                    
                },
                opposite: true
    
            }, { // Secondary yAxis
                gridLineWidth: 0,
                title: {
                    text: '',
                    style: {
                        color: '#4572A7'
                    }
                },
                labels: {
                    formatter: function() {
                        return this.value +' mm';
                    },
                    style: {
                        color: '#4572A7'
                    }
                }
    
            }, { // Tertiary yAxis
                gridLineWidth: 0,
                title: {
                    text: '',
                    style: {
                        color: '#AA4643'
                    }
                },
                labels: {
                    formatter: function() {
                        return this.value +' mb';
                    },
                    style: {
                        color: '#AA4643'
                    }
                },
                opposite: true
            }],
            tooltip: {
                shared: true
            },

            series:[{
            name :'Realizado',
            color: '#4682B4',
            type: '<?php echo $gra ?>',
            data:[<?php echo $seriesnsh_str; ?>,
			marker: {
									enabled: false
								},
								dashStyle: 'Solid',
								tooltip: {
									valueSuffix: ' '
								}
         },{    
                
                name: 'Planejado',
                color: '#CD2626',
                type: 'line',
                data:[<?php echo $seriesnsh_str; ?>,
				marker: {
									enabled: false
								},
								dashStyle: 'Solid',
								tooltip: {
									valueSuffix: ' '
								}
        }]
});
});

</script>

<script type="text/javascript">
$(function () {
        $('#receh').highcharts({
            chart: {
                zoomType: 'x'
            },
            title: {
                text: ''
            },
            xAxis: [{
                categories: [<?php echo $seriestabdhd_str; ?>,
				labels: {
                rotation: 300
            }
            }],
            yAxis: [{ // Primary yAxis
				lineWidth: 1,
				gridLineDashStyle: 'dot',
                labels: {
                    formatter: function() {
                        return this.value;
                    },
                    style: {
                        color: '#89A54E'
                    }
                },
                title: {
                    text: ''                    
                },
                opposite: true
    
            }, { // Secondary yAxis
                gridLineWidth: 0,
                title: {
                    text: '',
                    style: {
                        color: '#4572A7'
                    }
                },
                labels: {
                    formatter: function() {
                        return this.value +' mm';
                    },
                    style: {
                        color: '#4572A7'
                    }
                }
    
            }, { // Tertiary yAxis
                gridLineWidth: 0,
                title: {
                    text: '',
                    style: {
                        color: '#AA4643'
                    }
                },
                labels: {
                    formatter: function() {
                        return this.value +' mb';
                    },
                    style: {
                        color: '#AA4643'
                    }
                },
                opposite: true
            }],
            tooltip: {
                shared: true
            },

            series:[{
            name :'Realizado',
            color: '#4682B4',
            type: '<?php echo $gra ?>',
            data:[<?php echo $seriesreceh_str; ?>,
			marker: {
									enabled: false
								},
								dashStyle: 'Solid',
								tooltip: {
									valueSuffix: ' '
								}
         },{    
                
                name: 'Planejado',
                color: '#CD2626',
                type: 'line',
                data:[<?php echo $seriesreceh_str; ?>,
				marker: {
									enabled: false
								},
								dashStyle: 'Solid',
								tooltip: {
									valueSuffix: ' '
								}
        }]
});
});

</script>

<script type="text/javascript">
$(function () {
        $('#tmoh').highcharts({
            chart: {
                zoomType: 'x'
            },
            title: {
                text: ''
            },
            xAxis: [{
                categories: [<?php echo $seriestabdhd_str; ?>,
				labels: {
                rotation: 300
            }
            }],
            yAxis: [{ // Primary yAxis
				lineWidth: 1,
				gridLineDashStyle: 'dot',
                labels: {
                    formatter: function() {
                        return this.value;
                    },
                    style: {
                        color: '#89A54E'
                    }
                },
                title: {
                    text: ''                    
                },
                opposite: true
    
            }, { // Secondary yAxis
                gridLineWidth: 0,
                title: {
                    text: '',
                    style: {
                        color: '#4572A7'
                    }
                },
                labels: {
                    formatter: function() {
                        return this.value +' mm';
                    },
                    style: {
                        color: '#4572A7'
                    }
                }
    
            }, { // Tertiary yAxis
                gridLineWidth: 0,
                title: {
                    text: '',
                    style: {
                        color: '#AA4643'
                    }
                },
                labels: {
                    formatter: function() {
                        return this.value +' mb';
                    },
                    style: {
                        color: '#AA4643'
                    }
                },
                opposite: true
            }],
            tooltip: {
                shared: true
            },

            series:[{
            name :'Realizado',
            color: '#4682B4',
            type: '<?php echo $gra ?>',
            data:[<?php echo $seriestmoh_str; ?>,
			marker: {
									enabled: false
								},
								dashStyle: 'Solid',
								tooltip: {
									valueSuffix: ' '
								}
         },{    
                
                name: 'Planejado',
                color: '#CD2626',
                type: 'line',
                data:[<?php echo $seriestmoh_str; ?>,
				marker: {
									enabled: false
								},
								dashStyle: 'Solid',
								tooltip: {
									valueSuffix: ' '
								}
        }]
});
});

</script>

<script type="text/javascript">
$(function () {
        $('#abdm').highcharts({
            chart: {
                zoomType: 'x'
            },
            title: {
                text: ''
            },
            xAxis: [{
                categories: [<?php echo $seriestabdmd_str; ?>,
				labels: {
                rotation: 300
            }
            }],
            yAxis: [{ // Primary yAxis
				lineWidth: 1,
				gridLineDashStyle: 'dot',
                labels: {
                    formatter: function() {
                        return this.value;
                    },
                    style: {
                        color: '#89A54E'
                    }
                },
                title: {
                    text: ''                    
                },
                opposite: true
    
            }, { // Secondary yAxis
                gridLineWidth: 0,
                title: {
                    text: '',
                    style: {
                        color: '#4572A7'
                    }
                },
                labels: {
                    formatter: function() {
                        return this.value +' mm';
                    },
                    style: {
                        color: '#4572A7'
                    }
                }
    
            }, { // Tertiary yAxis
                gridLineWidth: 0,
                title: {
                    text: '',
                    style: {
                        color: '#AA4643'
                    }
                },
                labels: {
                    formatter: function() {
                        return this.value +' mb';
                    },
                    style: {
                        color: '#AA4643'
                    }
                },
                opposite: true
            }],
            tooltip: {
                shared: true
            },

            series:[{
            name :'Realizado',
            color: '#4682B4',
            type: '<?php echo $gra ?>',
            data:[<?php echo $seriestabdm_str; ?>,
			marker: {
									enabled: false
								},
								dashStyle: 'Solid',
								tooltip: {
									valueSuffix: ' '
								}
         },{    
                
                name: 'Planejado',
                color: '#CD2626',
                type: 'line',
                data:[<?php echo $seriestabdm_str; ?>,
				marker: {
									enabled: false
								},
								dashStyle: 'Solid',
								tooltip: {
									valueSuffix: ' '
								}
        }]
});
});

</script>

<script type="text/javascript">
$(function () {
        $('#nsm').highcharts({
            chart: {
                zoomType: 'x'
            },
            title: {
                text: ''
            },
            xAxis: [{
                categories: [<?php echo $seriestabdmd_str; ?>,
				labels: {
                rotation: 300
            }
            }],
            yAxis: [{ // Primary yAxis
				lineWidth: 1,
				gridLineDashStyle: 'dot',
                labels: {
                    formatter: function() {
                        return this.value;
                    },
                    style: {
                        color: '#89A54E'
                    }
                },
                title: {
                    text: ''                    
                },
                opposite: true
    
            }, { // Secondary yAxis
                gridLineWidth: 0,
                title: {
                    text: '',
                    style: {
                        color: '#4572A7'
                    }
                },
                labels: {
                    formatter: function() {
                        return this.value +' mm';
                    },
                    style: {
                        color: '#4572A7'
                    }
                }
    
            }, { // Tertiary yAxis
                gridLineWidth: 0,
                title: {
                    text: '',
                    style: {
                        color: '#AA4643'
                    }
                },
                labels: {
                    formatter: function() {
                        return this.value +' mb';
                    },
                    style: {
                        color: '#AA4643'
                    }
                },
                opposite: true
            }],
            tooltip: {
                shared: true
            },

            series:[{
            name :'Realizado',
            color: '#4682B4',
            type: '<?php echo $gra ?>',
            data:[<?php echo $seriesnsm_str; ?>,
			marker: {
									enabled: false
								},
								dashStyle: 'Solid',
								tooltip: {
									valueSuffix: ' '
								}
         },{    
                
                name: 'Planejado',
                color: '#CD2626',
                type: 'line',
                data:[<?php echo $seriesnsm_str; ?>,
				marker: {
									enabled: false
								},
								dashStyle: 'Solid',
								tooltip: {
									valueSuffix: ' '
								}
        }]
});
});

</script>

<script type="text/javascript">
$(function () {
        $('#recem').highcharts({
            chart: {
                zoomType: 'x'
            },
            title: {
                text: ''
            },
            xAxis: [{
                categories: [<?php echo $seriestabdmd_str; ?>,
				labels: {
                rotation: 300
            }
            }],
            yAxis: [{ // Primary yAxis
				lineWidth: 1,
				gridLineDashStyle: 'dot',
                labels: {
                    formatter: function() {
                        return this.value;
                    },
                    style: {
                        color: '#89A54E'
                    }
                },
                title: {
                    text: ''                    
                },
                opposite: true
    
            }, { // Secondary yAxis
                gridLineWidth: 0,
                title: {
                    text: '',
                    style: {
                        color: '#4572A7'
                    }
                },
                labels: {
                    formatter: function() {
                        return this.value +' mm';
                    },
                    style: {
                        color: '#4572A7'
                    }
                }
    
            }, { // Tertiary yAxis
                gridLineWidth: 0,
                title: {
                    text: '',
                    style: {
                        color: '#AA4643'
                    }
                },
                labels: {
                    formatter: function() {
                        return this.value +' mb';
                    },
                    style: {
                        color: '#AA4643'
                    }
                },
                opposite: true
            }],
            tooltip: {
                shared: true
            },

            series:[{
            name :'Realizado',
            color: '#4682B4',
            type: '<?php echo $gra ?>',
            data:[<?php echo $seriesrecem_str; ?>,
			marker: {
									enabled: false
								},
								dashStyle: 'Solid',
								tooltip: {
									valueSuffix: ' '
								}
         },{    
                
                name: 'Planejado',
                color: '#CD2626',
                type: 'line',
                data:[<?php echo $seriesrecem_str; ?>,
				marker: {
									enabled: false
								},
								dashStyle: 'Solid',
								tooltip: {
									valueSuffix: ' '
								}
        }]
});
});

</script>

<script type="text/javascript">
$(function () {
        $('#tmom').highcharts({
            chart: {
                zoomType: 'x'
            },
            title: {
                text: ''
            },
            xAxis: [{
                categories: [<?php echo $seriestabdmd_str; ?>,
				labels: {
                rotation: 300
            }
            }],
            yAxis: [{ // Primary yAxis
				lineWidth: 1,
				gridLineDashStyle: 'dot',
                labels: {
                    formatter: function() {
                        return this.value;
                    },
                    style: {
                        color: '#89A54E'
                    }
                },
                title: {
                    text: ''                    
                },
                opposite: true
    
            }, { // Secondary yAxis
                gridLineWidth: 0,
                title: {
                    text: '',
                    style: {
                        color: '#4572A7'
                    }
                },
                labels: {
                    formatter: function() {
                        return this.value +' ';
                    },
                    style: {
                        color: '#4572A7'
                    }
                }
    
            }, { // Tertiary yAxis
                gridLineWidth: 0,
                title: {
                    text: '',
                    style: {
                        color: '#AA4643'
                    }
                },
                labels: {
                    formatter: function() {
                        return this.value +' ';
                    },
                    style: {
                        color: '#AA4643'
                    }
                },
                opposite: true
            }],
            tooltip: {
                shared: true
            },

            series:[{
            name :'Realizado',
            color: '#4682B4',
            type: '<?php echo $gra ?>',
            data:[<?php echo $seriestmom_str; ?>,
			marker: {
									enabled: false
								},
								dashStyle: 'Solid',
								tooltip: {
									valueSuffix: ' '
								}
         },{    
                
                name: 'Planejado',
                color: '#CD2626',
                type: 'line',
                data:[<?php echo $seriestmom_str; ?>,
				marker: {
									enabled: false
								},
								dashStyle: 'Solid',
								tooltip: {
									valueSuffix: ' '
								}
        }]
});
});

</script>

<div id="menu">
<form action="" method="post" id="form" onchange="form.submit(this.value);">
			
			<select name="skill" id="skill">
                <option >Selecione a Fila Desejada</option>
			  
			  <?php
			  
					$sql = "SELECT * FROM skills.tbl_assunto_2";
					$rs = odbc_exec($conn, $sql);
					if (!$rs)
					  {exit("Error in SQL");}
					while (odbc_fetch_row($rs)) 
					{
					  $id_skill = odbc_result ($rs,"id");
					  $assunto = odbc_result($rs,"assunto_2");
					  if($id_skill == $_POST['skill']){
						echo "<option value='$id_skill' selected>$assunto</option>\n";
					  }
					  else{
						echo "<option value='$id_skill'>$assunto</option>\n";
					  }				  
					}
					
					odbc_close($conn); 

			  ?>

				</select>
			  <select name="mes" id="mes">
                <option >M�s</option>
			  
			  <?php
			  $temp = $mes;
					$sqlm = "SELECT * FROM proc_performance_tela1(1,".$_POST["skill"].",0,'2013-01-01')";
					$rs = odbc_exec($conn, $sqlm);
					if (!$rs)
					  {exit("Error in SQL");}
					while (odbc_fetch_row($rs)) 
					{
					  $mes = odbc_result ($rs,"periodo");
					if($mes == $_POST['mes']){
						echo "<option value='$mes' selected>$mes</option>\n";
					  }
					  else{
						echo "<option value='$mes'>$mes</option>\n";
					  }				  
					}
					
					odbc_close($conn); 

			  ?>

			  </select>
			  <select name="dia" id="dia">
                <option >Dia</option>
			  
			  <?php
					$sqld = "SELECT * FROM proc_performance_tela1(2,".$_POST["skill"].",".$temp.",'2013-01-01')";
					$rs = odbc_exec($conn, $sqld);
					if (!$rs)
					  {exit("Error in SQL");}
					while (odbc_fetch_row($rs)) 
					{
					  $dia = odbc_result ($rs,"periodo");
					  $dia2 = "2013-".$temp."-".explode("-",$dia)[0];
					if($dia2 == $_POST['dia']){
						echo "<option value='$dia2' selected>$dia</option>\n";
					  }
					  else{
						echo "<option value='$dia2'>$dia</option>\n";
					  }				  
					}
					
					odbc_close($conn); 

			  ?>

			  </select>
			  
			  <select name="graf" id="valor" class="selct" >
<option selected="selected" value="">Tipo</option>
<option value="area">Area</option>
<option value="column">Coluna</option>
<option value="line">Linha</option>
<option value="spline">Tendencia</option>
</select>
			  
		</form>
</div>


<div class="column">

<div class="portlet">
    <div class="portlet-header">Abandono</div>
	<div class="portlet-content" id="abdm" style="width: 400px; height: 250px; margin: 0 auto"></div>
	<div class="portlet-content" id="abd" style="width: 400px; height: 250px; margin: 0 auto"></div>
	<div class="portlet-content" id="abdh" style="width: 400px; height: 250px; margin: 0 auto"></div>
</div>
</div>

<div id="ns" style="width: 400px; height: 250px; margin: 0 auto"></div>
<div id="rece" style="width: 400px; height: 250px; margin: 0 auto"></div>
<div id="tmo" style="width: 400px; height: 250px; margin: 0 auto"></div>

<div id="nsh" style="width: 400px; height: 250px; margin: 0 auto"></div>
<div id="receh" style="width: 400px; height: 250px; margin: 0 auto"></div>
<div id="tmoh" style="width: 400px; height: 250px; margin: 0 auto"></div>

<div id="nsm" style="width: 400px; height: 250px; margin: 0 auto"></div>
<div id="recem" style="width: 400px; height: 250px; margin: 0 auto"></div>
<div id="tmom" style="width: 400px; height: 250px; margin: 0 auto"></div>

</body>
</html>