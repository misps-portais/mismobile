<?php

//
define('ENVIRONMENT', 'develop');
define('APP_PATH', 'C:\xampp\htdocs\mismobile');
define('SIS_PATH', APP_PATH.'\node\php\sistemas');
ini_set('date.timezone','America/Sao_Paulo');

//
//require(SIS_PATH.'\functions.php');
(empty($_GET['acao'])) ? $rota = 404 : $rota = $_GET['acao'];


//
$conn = odbc_connect('MISPG','','');


//
if(ENVIRONMENT == 'develop'){
    error_reporting(E_ALL);
    session_start();
    header('Content-Type: text/html; charset=utf-8');
    header("Last-Modified: ".date("D, d M Y H:i:s")." GMT");
}

if(ENVIRONMENT == 'test'){
    error_reporting(0);
    global $user;

}

if(ENVIRONMENT == 'production'){
    error_reporting(0);
    global $user;
}

/**
 *    Rotas para as páginas do sistema
 */
switch($rota){
    case 'todos':
        require(SIS_PATH.'\todos.php');
        break;

	default:
        header("HTTP/1.0 404 Not Found");
        echo 'Erro 404: Página não encontrada';
        break;
}

/**
 *   Acaba com o banco!  #ohnooo
 */
odbc_close($conn);

?>