<!-- HOME PAGE DO PORTAL -->
<cfheader name="X-XSS-Protection" value="0">
<?php

global $base_root;

$skavurska = array('Nenhum', 'Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro');


function graph($div,$titulo,$label,$dir,$dem,$cor){

	$dir2 = number_format($dir,0,',','.');

	echo "
		$('#$div').highcharts({
			chart: {
			    plotBackgroundColor: null,
			    plotBorderWidth: 0,
			    plotShadow: false,
			    margin: [0, 0, 0, 0],
				spacingTop: 0,
				spacingBottom: 0,
				spacingLeft: 0,
				spacingRight: 0
			},
			title: {
				style: { fontSize: '12px' },
			    text: '$titulo<br\>$dir2',
			    align: 'center',
			    verticalAlign: 'middle',
			    y: -15
			},
			tooltip: {
				enabled: false,
			    pointFormat: '{series.name}: <b>{point.y:f}</b>'
			},
			plotOptions: {
			    pie: {
					dataLabels: {
		                enabled: false
		            },
					startAngle: -90,
					endAngle: 270,
					center: ['50%', '50%']
			    }
			},
			series: [{
			    type: 'pie',
			    name: '$label',
			    innerSize: '99%',
			    data: [
				{name:'Diretoria<br>Atd',   y: $dir,color:'$cor'},
				{name:'',   y: $dem,color:'#e9e9e9'}
			    ]
			}],
		});
	";
}

function graph_por($div,$titulo,$label,$dir,$dem,$cor){

	$dir_com = 100 - $dir;
	$dem_com = 100 - $dem;
	$dir2 = number_format($dir,2,',','.');
	echo "
	    $('#$div').highcharts({
		chart: {
		    plotBackgroundColor: null,
		    plotBorderWidth: 0,
			plotShadow: false,
		    margin: [0, 0, 0, 0],
			spacingTop: 0,
			spacingBottom: 0,
			spacingLeft: 0,
			spacingRight: 0
		},
		title: {
			style: { fontSize: '12px' },
		    text: '$titulo<br\>$dir2%',
		    align: 'center',
		    verticalAlign: 'middle',
		    y: -15
		},
		tooltip: {
			enabled: false,
		    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
		},
		plotOptions: {
		    pie: {
				dataLabels: {
	                enabled: false
	            },
				startAngle: -90,
				endAngle: 270,
				center: ['50%', '50%']
		    }
		},
		series: [{
		    type: 'pie',
		    name: '$label',
		    innerSize: '99%',
		    data: [
			{name:'Diretoria<br>Atd',   y: $dir,color:'$cor'},
			{name:'',   y: $dir_com,color:'#e9e9e9'}
		    ]
		}]
	    });
	";
};

$conn = odbc_connect('MISPG','','');

$sql = "SELECT 
tdir.diretoria as diretoria,
SUM(ta.callsoffered)::text AS recebidas,
CASE SUM(ta.callsoffered)
		WHEN 0 THEN
			0::text
		ELSE
			ROUND(CAST((SUM(ta.abncalls)-SUM(ta.abncalls1)) AS numeric)/CAST(SUM(ta.callsoffered) AS numeric)*100,2)::text
	END AS abd_per,
	CASE sum(ta.callsoffered)
		WHEN 0 THEN 
			0::text
		ELSE 
			ROUND(CAST(sum(ta.acdcalls-ta.acdcalls10-ta.acdcalls9) AS numeric) / CAST(sum(ta.callsoffered) as numeric)*100,2)::text
	END as ns60
FROM performance.tbl_performance_dia as ta
	INNER JOIN skills.tbl_diretoria tdir ON
	tdir.id = ta.diretoria
WHERE row_date = current_date
GROUP BY ta.row_date, tdir.diretoria,ta.diretoria
ORDER BY ta.diretoria,tdir.diretoria,ta.row_date;";
$rs = odbc_exec($conn, $sql);

$i = 0;

while(odbc_fetch_row($rs)){

	$resultado[$i][0] = odbc_result($rs,'recebidas');
	$resultado[$i][1] = odbc_result($rs,'abd_per');
	$resultado[$i][2] = odbc_result($rs,'ns60');
	$i++;	

}


//
$sql_ = "SELECT to_char(max(data_atualiza) + '00:30:00','DD-MM-YYYY HH24:MI:ss') as data_atualiza FROM avaya.tbl_avaya_extracao_info";
$rs_ = odbc_exec($conn, $sql_);
$data_atualiza = odbc_result($rs_, 'data_atualiza');



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



$wdays = array('Dom','Seg','Ter','Qua','Qui','Sex','Sab'); # Dias da Semana
$today = 11; # Dia corrente
$month = date('m'); # Mes atual
$year = date('Y'); # Ano atual

$days_in_cm = date('t'); # Total de dias no mês atual
$days_in_lm = date('t',strtotime("-1 month")); # Total de dias no mês anterior

$counter = 0;
$counter_nm = 1; 

$wday_start_cm = date('N',strtotime('first day of this month')); # Em que dia da semana começa o mes atual
$wday_end_cm = date('N',strtotime('last day of this month'));
$wday_end_lm = $wday_start_cm-1;

##
$calendario  = '<table class="calendario">';
$calendario .= '<thead>';
$calendario .= '<tr>';

foreach($wdays as $day){
	$calendario .= '<th>'.$day.'</th>';

	if($day == 'Dom' or $day == 'Sab'){
		$calendario .= '<th class="fds">'.$day.'</th>';
	}
}

$calendario .= '</tr>';
$calendario .= '</thead>';
$calendario .= '<tbody>';

for($tr = 1; $tr <= 5; $tr++){
	$calendario .= '<tr id="semana-">';

	for($td = 0; $td <= 6; $td++){
		if(($counter - $wday_start_cm) < 0){
			$day = (($days_in_lm-$wday_end_lm) + $counter);
			$class = 'cinza';
		}
		else if(($counter - $wday_start_cm) >= $days_in_cm){
			$day = $counter_nm;
			$counter_nm++;
			$class = 'cinza';
		}
		else{
			$day = $counter - $wday_start_cm + 1;
		}

		$calendario .= '<td class="'.$class.'">'.$day.'</td>';
		$counter++;
		$class = null;
	}

	$calendario .= '</tr>';
}

$calendario .= '</tbody>';
$calendario .= '</table>';

?>

<script type="text/javascript">
	$(function () {<?php graph('rec_gra_dir','Ligações<br>Recebidas','Recebidas',$resultado[0][0],$resultado[1][0],'#46b4af') ?>});
	$(function () {<?php graph_por('abd_gra_dir','Ligações<br>Abandonadas','% Abandono',$resultado[0][1],$resultado[1][1],'#46b4af') ?>});
	$(function () {<?php graph_por('ns_gra_dir','Nível de<br>Serviço até 60','NS 60',$resultado[0][2],$resultado[1][2],'#46b4af') ?>});
	$(function () {<?php graph('rec_gra_dem','Ligações<br>Recebidas','Recebidas',$resultado[1][0],$resultado[0][0],'#3498db') ?>});
	$(function () {<?php graph_por('abd_gra_dem','Ligações<br>Abandonadas','% Abandono',$resultado[1][1],$resultado[0][1],'#3498db') ?>});
	$(function () {<?php graph_por('ns_gra_dem','Nível de<br>Serviço até 60','NS 60',$resultado[1][2],$resultado[0][2],'#3498db') ?>});
</script>

<div class="pagina-inicial">
	<div class="regiao1 destaques">
		<div class="interno">
			<h3>Destaques</h3>
			<div class="flutua-esq bloco">
				<a href="#"><img src="<?php echo $base_root;?>/mismobile/assets/images/destaques-fechamento-2013.png" alt="" /></a>
			</div>
			<div class="flutua-esq bloco">
				<a href="#"><img src="<?php echo $base_root;?>/mismobile/assets/images/destaques-ppr.png" alt="" /></a>
			</div>
			<div class="flutua-esq bloco">
				<a href="http://novaportonet/PortoNetIntranet/Participe-da-Campanha-de-Sugest%C3%B5es" target="_blank"><img src="<?php echo $base_root;?>/mismobile/assets/images/destaques-campanha-sugestoes.png" alt="" /></a>
			</div>
			<div class="flutua-esq bloco">
				<a href="<?php echo $base_root;?>/mismobile/assets/ENCONTROS_EXECUTIVOS_2014_V1.docx" download="ENCONTROS_EXECUTIVOS_2014.docx"><img src="<?php echo $base_root;?>/mismobile/assets/images/destaques-agenda-diretoria.png" alt="" /></a>
			</div>
			<div class="flutua-esq bloco sem-margem">
				<a href="#"><img src="<?php echo $base_root;?>/mismobile/assets/images/destaques-cafe-filosofico.png" alt="" /></a>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<div class="regiao2 saudacao-usuario">
		<div class="interno"></div>
	</div>
	<div class="regiao3">
		<div class="interno">
			<div class="flutua-esq indicadores">
				<h3>Volumes</h3>
				<div class="diretorias">
					<h4>Diretoria de atendimento</h4>
					<div class="flutua-esq bloco" id="rec_gra_dir" ></div>
					<div class="flutua-esq bloco" id="abd_gra_dir"></div>
					<div class="flutua-esq bloco" id="ns_gra_dir"></div>
					<div class="clearfix"></div>
				</div>
				<div class="diretorias">
					<h4>Demais diretorias</h4>
					<div class="flutua-esq bloco" id="rec_gra_dem"></div>
					<div class="flutua-esq bloco" id="abd_gra_dem"></div>
					<div class="flutua-esq bloco" id="ns_gra_dem"></div>
					<div class="clearfix"></div>
				</div>
				<span>* Atualizado até <?php echo $data_atualiza;?></span>
			</div>
			<div class="flutua-esq acesso-rapido">
				<h3>Acesso Rápido</h3>
				<h4>Notícias</h4>
				<ul>
					<li class="noticia">
						<h5>Estrategia e Governança</h5>
						<h4>Novo WFM</h4>
						<span>Sistema de planejamento e gestão de escalas.</span>
					</li>
					<li class="noticia">
						<h5>Estrategia e Governança</h5>
						<h4>Programa Fluir</h4>
						<span>Programa de melhoria contínua dos processos de planejamento, controle e MIS.</span>
					</li>
					<li class="noticia">
						<h5>Estrategia e Governança</h5>
						<h4>POMAR</h4>
						<span>Programa de otimização do modelo de atendimento e relacionamento.</span>
					</li>
					<li class="noticia">
						<h5>Desenvolvimento de Atendimento</h5>
						<h4>Destaques Nota 11</h4>
						<span>Confira os destques das Centrais de Atendimento.</span>
					</li>
				</ul>
			</div>
			<div class="flutua-esq agenda-npc">
				<h3>Agenda NPC</h3>
				<div class="calendario">
					<h3><?php echo $skavurska[date('n')].' '.date('Y');?></h3>
					<!--
					<img src="<?php echo $base_root;?>/mismobile/themes/portal/images/calendario-completo.png" alt="">
					-->
					<?php echo $calendario;?>
				</div>
				<div class="flutua-esq glossario">
					<a href="#EmConstrucao"><img src="<?php echo $base_root;?>/mismobile/themes/portal/images/glossario.png" alt="" /></a>
				</div>
				<div class="flutua-dir web-aulas">
					<a href="http://portoseguro.webaula.com.br/aluno/login" target="_blank"><img src="<?php echo $base_root;?>/mismobile/themes/portal/images/web-aulas.png" alt="" /></a>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="flutua-esq area-indefinida1">
				<ul>
					<a href="<?php echo $base_root;?>/mismobile/assets/03_Report_Fluir_14-11-2013.xlsx" download="03_Report_Fluir_14-11-2013.xlsx">
						<li>
							<div class="flutua-dir icone60 boletim-fluir"></div>
							<h4>Boletim<br/>FLUIR</h4><span>Último Status</span>
						</li>
					</a>
					<a href="<?php echo $base_root;?>/mismobile/assets/Boletim_POMAR_v3.jpg" download="Boletim_POMAR_v3.jpg">
						<li>
							<div class="flutua-dir icone60 boletim-pomar"></div>
							<h4>Boletim<br/>POMAR</h4><span>Último Status</span>
						</li>
					</a>
					<a href="http://172.23.144.196/rad/sap/cliente/rap.asp" target="_blank">
						<li>
							<div class="flutua-dir icone60 rap"></div>
							<h4>R.A.P</h4><span>Acompanhe <br/>os projetos</span>
						</li>
					</a>
					<a href="<?php echo $base_root;?>/mismobile/assets/O_Comite_VF_V2.pptx" download="O_Comite_VF_V2.pptx">
						<li>
							<div class="flutua-dir icone60 comite-mais"></div>
							<h4>Comitê<br/>+</h4><span>Ver a página</span>
						</li>
					</a>
					<a href="<?php echo $base_root;?>/mismobile/assets/Programa_D+.pptx" download="Programa_D+.pptx">
						<li>
							<div class="flutua-dir icone60 programa-dmais"></div>
							<h4>Programa<br/>D+</h4><span>Últimas ações</span>
						</li>
					</a>
					<a href="<?php echo $base_root;?>/mismobile/assets/Reuniao_Progresso_2013_VF.pptx" download="Reuniao_Progresso_2013_VF.pptx">
						<li>
							<div class="flutua-dir icone60 reuniao-progresso"></div>
							<h4>Reunião<br/>Progresso</h4><span>Últimas ações</span>
						</li>
					</a>
					
				</ul>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>

	<div class="regiao4">
		<div class="interno">
			<div class="flutua-esq acontece">
				<h3>O que Acontece?</h3>
				<div class="cx-informativo">
					<div class="flutua-esq destaque"><h2>Dissídio</h2><h3>2014</h3></div>
					<div class="flutua-dir descricao">
						<p>Sindicato desmente boatos e reafirma <br/>sua luta em defesa da categoria.</p>
						<a href="http://www.securitariosp.org.br/Noticia.aspx?pId=15&nId=1185" target="_blank">Clique aqui e confira</a>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="cx-informativo">
					<div class="flutua-esq destaque"><h2>Qualidade <br/>de Vida</h2></div>
					<div class="flutua-dir descricao">
						<p>Empresas, sindicatos e governo desenvolvem ações especiais integradas para o combate prévio ao estresse e, assim evitar transtornos ao colaborador.</p>
						<a href="http://portalcallcenter.consumidormoderno.uol.com.br/gestao/responsabilidade-social/setor-investe-em-acoes-para-a-qualidade-de-vida-do-operador" target="_blank">Clique aqui e confira</a>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="flutua-dir porto-player">
				<h3>Porto Player</h3>
				<div class="cx-video">
					<div class="flutua-esq destaque">
						<p>Quanto mais você usa no dia a dia, mais barato dica o seguro do teu carro.</p>
						<p>O cartão de crédito que tem tudo o que você espera de um cartão e vantagens que você não esperaca no seguro do seu carro.</p>
						<p>Confira a campanha!</p>
					</div>
					<iframe class="flutua-dir video" id="ytplayer" src="https://www.youtube.com/embed/nNa6gsuPGYI" type="text/html" frameborder="0" allowfullscreen></iframe>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>

	<div class="regiao5">
		<div class="interno">
			<div class="flutua-esq area-indefinida2">
				<ul>
					<a href="http://novaportonet/portal/site/SocialIntranet/home/resolver.vcarp/blog/1.11.3490/Blog%20Fabio%20Luchetti" target="_blank">
						<li class="flutua-esq icone60 luchetti">
							<h4>Blog<br/>Corporativo</h4>
							<span>Fabio Luchetti</span>
						</li>
					</a>
					<a href="http://novaportonet/portal/site/SocialIntranet/home/resolver.vcarp/blog/1.11.3548/F%C3%B3rum_Atendimento" target="_blank">
						<li class="flutua-esq icone60 sonia-rica">
							<h4>Fórum de<br/>Atendimento</h4>
							<span>Sonia Rica</span>
						</li>
					</a>
					<a href="<?php echo $base_root;?>/mismobile/assets/Organograma_v2.pptx" download="Organograma_v2.pptx">
						<li class="flutua-esq icone60 sem-margem organograma">
							<h4>Organograma</h4>
							<span>Estrutura da diretoria<br/>de atendimento</span>
						</li>
					</a>
				</ul>
			</div>
			<div class="flutua-dir avalie-o-portal">
				<h3>Avalie nosso portal!</h3>
				<img src="<?php echo $base_root;?>/mismobile/themes/portal/images/avalie.png" alt="avalie" />
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
<script src="<?php echo $base_root;?>/mismobile/node/js/highcharts.js" type="text/javascript"></script>
<script src="<?php echo $base_root;?>/mismobile/node/js/performance.js" type="text/javascript"></script>
<!-- HOME PAGE DO PORTAL -->
