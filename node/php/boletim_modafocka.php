<!--
<link href="http://172.23.14.155/mismobile/themes/portal/style?base" rel="stylesheet" type="text/css">
<link href="http://172.23.14.155/mis/node/css/myTheme.css" rel="stylesheet" media="screen">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">

-->

<style>

	table{
		width: 100%;
		min-width:400px;
	}
	
	ul.legenda{
		list-style: none;
		float: left;
		width: 50%;
		-webkit-margin-before: 0;
		-webkit-margin-after: 0;
		-webkit-margin-start: 0px;
		-webkit-margin-end: 0px;
		-webkit-padding-start: 0;
		margin:0 10px 20px 20px;
		color: #215867;
	    font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
	}
	
</style>



<div class="top">
	<div class="navigation">
		<?php require('C:\xampp\htdocs\mismobile\node\php\relatorios\/navigation.php');?>
		<div class="clearfix"></div>
	</div>
</div><!-- / fim content-top -->

<div class="bottom">
	<div class="inner">
		<div class="boletim" style="margin-top:20px;">

<?php
//include 'hit.php';

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
		function min_volume_bm($intervalo){
			
			$dicintervalo = array(
								'00:00:00' => 0.252399424,'00:30:00'=> 0.434289198,'01:00:00'=> 0.571414294,'01:30:00'=> 0.682032546,'02:00:00'=> 0.770225512,'02:30:00'=> 0.839762274,'03:00:00'=> 0.902055047,'03:30:00'=> 0.965158177,'04:00:00'=> 1.024266049,'04:30:00'=> 1.086063138,'05:00:00'=> 1.161730056,'05:30:00'=> 1.273212443
								,'06:00:00'=> 1.448485534,'06:30:00'=> 1.735069934,'07:00:00'=> 2.231476004,'07:30:00'=> 2.865035166,'08:00:00'=> 3.538671041,'08:30:00'=> 4.749831418,'09:00:00'=> 6.457537374,'09:30:00'=> 8.564940108,'10:00:00'=> 10.86562585,'10:30:00'=> 13.23767233
								,'11:00:00'=> 15.58448179,'11:30:00'=> 17.79150825,'12:00:00'=> 19.59116223,'12:30:00'=> 21.1095317,'13:00:00'=> 22.58310923,'13:30:00'=> 24.17662355,'14:00:00'=> 25.95304957,'14:30:00'=> 27.88908365,'15:00:00'=> 29.89591786,'15:30:00'=> 31.91771272
								,'16:00:00'=> 33.94819853,'16:30:00'=> 35.95554316,'17:00:00'=> 37.87016524,'17:30:00'=> 39.54145822,'18:00:00'=> 40.80306213,'18:30:00'=> 41.70476175,'19:00:00'=> 42.96154589,'19:30:00'=> 44.16727608,'20:00:00'=> 45.30930491,'20:30:00'=> 46.33734007
								,'21:00:00'=> 47.23087615,'21:30:00'=> 48.03767196,'22:00:00'=> 48.69989936,'22:30:00'=> 49.2380923,'23:00:00'=> 49.66803368,'23:30:00'=> 50);
						
			return $dicintervalo[$intervalo];
		}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
		function abd_cor($numero){
			
			$numero = floatval(str_replace(",",".",str_replace("%","",$numero)));
			
			if($numero >= 10){
				return "red";
			}
			else{
				return "";
			}
			
		}
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
		function cabecalho($id, $titulo){
		
			echo "<table class='fancyTable' id='myTable02' name='myTable02' ><thead><tr><td style='font-size: 16px;
																									font-weight: bold;
																									color: #215867;'>
					$titulo
					</td></tr></thead></table>";
					
					echo "<table class='fancyTable' id='myTable02' name='myTable02' style='font-size: 10px;word-break:break-all;width:100%'>
								<thead>
									<tr>";
									if($id == 0){
										echo "	<th >ÁREA/FILA</th>
												<th style='width:10%'>REC</th>
												<th style='width:10%'> NS - 60''</th>
												<th style='width:10%'> (%) ABD Aceit.</th>
												<th style='width:10%'> (%) ABD</th>
												<th style='width:10%'> TMAXESP</th>";
									}
                                                                        else if($id == 2){
									echo"	<th > ÁREA/FILA</th>
											<th style='width:10%'> REC</th>
											<th style='width:10%'> ATD</th>
											<th style='width:10%'> ABD > 5</th>
											<th style='width:10%'> TMA</th>
											<th style='width:10%'> NS 60''</th>
											<th style='width:10%'> (%) ABD</th>";
                                                                        }
									else{									
									echo"	<th > ÁREA/FILA</th>
											<th style='width:10%'> REC</th>
											<th style='width:10%'> ATD</th>
											<th style='width:10%'> ABD > 5</th>
											<th style='width:10%'> TMO</th>
											<th style='width:10%'> NS 60''</th>
											<th style='width:10%'> (%) ABD</th>";
									}
					echo"		</tr>
								</thead><tbody>";
								
		
		}
		
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
		function raining_blood($titulo,$indicadores,$cores,$esp,$rs){
		
					cabecalho(0, $titulo);
					
					while(odbc_fetch_row($rs)){
					
						echo "<tr>";
						
						$resultado = utf8_encode(odbc_result($rs,$indicadores[0]));
						echo "<td style='width:30%;'>$resultado</td>";
						
						for ($i = 1; $i < sizeof($indicadores); $i++) {
							$resultado = utf8_encode(odbc_result($rs,$indicadores[$i]));
											
							if($indicadores[$i] == 'abd'){
								echo "<td style='color:red;text-align:center'>$resultado</td>";
							}
							else{
								echo "<td style='text-align:center'>$resultado</td>";
							}
						}
						
						echo "</tr>";

					};
					
					echo "</tbody></table>";
					
					echo "</br>";
					
		};
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
		function tbl_2($titulo,$indicadores,$cores,$esp,$rs){
		
			cabecalho(1, $titulo);
			
			while(odbc_fetch_row($rs)){
			
				echo "<tr>";
				
				$cor = $cores[odbc_result($rs,$indicadores[0])-1];
				$pl = $esp[odbc_result($rs,$indicadores[0])-1];
				$resultado = utf8_encode(odbc_result($rs,$indicadores[1]));
				echo "<td style='width:30%;background-color:$cor;padding-left:$pl'>$resultado</td>";	
						
				for ($i = 2; $i < sizeof($indicadores); $i++) {
					$resultado = utf8_encode(odbc_result($rs,$indicadores[$i]));
					
					if($indicadores[$i] == "abd_perc"){
						$cortexto = abd_cor($resultado);
					}
					else{
						$cortexto = "";
					}
					
						echo "<td style='background-color:$cor;text-align:center;color:$cortexto;'>$resultado</td>";
				}
				
				echo "</tr>";

			};
			
			echo "</tbody></table>";
			echo "</br>";
					
		};
		
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
		function tbl_3($titulo,$indicadores,$cores,$esp,$rs,$fonte){
		
			cabecalho(1, $titulo);
			
			while(odbc_fetch_row($rs)){
			
				echo "<tr>";
				
				$cor = $cores[odbc_result($rs,$indicadores[0])-1];
				$pl = $esp[odbc_result($rs,$indicadores[0])-1];
				$letra = $fonte[odbc_result($rs,$indicadores[0])-1];
				
				$resultado = utf8_encode(odbc_result($rs,$indicadores[1]));
				
				echo "<td style='width:30%;background-color:$cor;padding-left:$pl;$letra;'>$resultado</td>";	
						
				for ($i = 2; $i < sizeof($indicadores); $i++) {
					$resultado = utf8_encode(odbc_result($rs,$indicadores[$i]));
					
					if($indicadores[$i] == "abd_perc"){
						$cortexto = abd_cor($resultado);
					}
					else{
						$cortexto = "";
					}
					
						echo "<td style='background-color:$cor;text-align:center;$letra;color:$cortexto;'>$resultado</td>";
				}
				
				echo "</tr>";

			};
			
			echo "</tbody></table>";
					
		};
		
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			function tbl_4($titulo,$indicadores,$cores,$esp,$rs,$fonte){
		
					cabecalho(2, $titulo);
					
					while(odbc_fetch_row($rs)){
					
						echo "<tr>";
						
										$cor = $cores[odbc_result($rs,$indicadores[0])-1];
				$pl = $esp[odbc_result($rs,$indicadores[0])-1];
				$letra = $fonte[odbc_result($rs,$indicadores[0])-1];

						$resultado = utf8_encode(odbc_result($rs,$indicadores[1]));
						echo "<td style='width:30%;background-color:$cor;padding-left:$pl;$letra;'>$resultado</td>";
						
						for ($i = 2; $i < sizeof($indicadores); $i++) {
							$resultado = utf8_encode(odbc_result($rs,$indicadores[$i]));
											
							if($indicadores[$i] == 'perc_abd'){
								$cortexto = abd_cor($resultado);
							}
							else{
								$cortexto = '';
							}

                                                        echo "<td style='background-color:$cor;text-align:center;$letra;color:$cortexto;''>$resultado</td>";
						}
						
						echo "</tr>";

					};
					
					echo "</tbody></table>";
					
					echo "</br>";
					
		};

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
		function tbl_rga($titulo,$indicadores,$cores,$esp,$rs){
		
		odbc_fetch_row($rs);

echo "<table class='fancyTable' id='myTable02' name='myTable02' ><thead><tr><td style='font-size: 16px;
																									font-weight: bold;
																									color: #215867;'>
					$titulo
					</td></tr></thead></table>";
					
					echo "<table class='fancyTable' id='myTable02' name='myTable02' style='font-size: 10px;word-break:break-all;width:100%'>
								<thead>
									<tr>";
										echo "	<th rowspan=2>ÁREA/FILA</th>
												<th style='width:10%'>".utf8_encode(odbc_result($rs,$indicadores[1]))."</th>
												<th style='width:10%'>".utf8_encode(odbc_result($rs,$indicadores[2]))."</th>
												<th style='width:10%'>".utf8_encode(odbc_result($rs,$indicadores[3]))."</th>
												<th style='width:10%'>".utf8_encode(odbc_result($rs,$indicadores[4]))."</th></tr><th colspan=4>%ABD</th>";
									
					echo"</thead><tbody>";
			
			while(odbc_fetch_row($rs)){
			
				echo "<tr>";
				
				$resultado = utf8_encode(odbc_result($rs,$indicadores[0]));
				echo "<td style='width:30%;'>$resultado</td>";	
						
				for ($i = 1; $i < sizeof($indicadores); $i++) {
					$resultado = utf8_encode(odbc_result($rs,$indicadores[$i]));
					
						$cortexto = abd_cor($resultado);
					
						echo "<td style='text-align:center;color:$cortexto;'>$resultado</td>";
				}
				
				echo "</tr>";

			};
			
			echo "</tbody></table>";
			echo "</br>";
					
		};
		
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
		$conn=odbc_connect('MISPG','','');

		// $sql = "INSERT INTO tbl_acessos_portal(pagina,acessos,data) 
		// VALUES ('boletim',0,CURRENT_DATE) 
		// WHERE NOT EXISTS(SELECT acessos FROM tbl_acessos_portal WHERE data = CURRENT_DATE and pagina = 'boletim')"
		// $rs = odbc_exec($conn, $sql);

		// $sql = "UPDATE tbl_acessos_portal 
		// 		SET acessos = (SELECT acessos FROM tbl_acessos_portal WHERE data = CURRENT_DATE and pagina = 'boletim') + 1 
		// 		WHERE pagina = 'boletim' and data = CURRENT_DATE";
		// $rs=odbc_exec($conn,$sql);

		$sql = "SELECT servidor, dac, max(data_atualiza) + interval '30 minutes' as max
			FROM avaya.tbl_avaya_extracao_info 
			WHERE relatorio = 2
			GROUP BY servidor, dac 
			ORDER BY servidor, dac ";
		$rs=odbc_exec($conn,$sql);
		
		$at_tempo = date('H:i:s',strtotime(odbc_result($rs,'max')));
		$hoje = date("d/m/y");
		$intervalo = min_volume_bm($at_tempo);
		
		$tab = "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp";
		$tab2 = "&nbsp&nbsp&nbsp&nbsp&nbsp";
	
	//Imprime titulo e legendaSSSS
			echo"<table class='fancyTable' id='myTable02' name='myTable02'><thead><tr><td style='font-size: 16px;
			font-weight: bold;
			color: #215867;'>
			Boletim - Torre de Monitoramento
			</td></tr></thead></table>";
			
			echo "
				</br>
				<div style='font-size:14px;'>
					<ul class='legenda'>
						<li><b style='padding-right: 10px;'>REC:</b>$tab LIGAÇÕES RECEBIDAS</li>
						<li><b style='padding-right: 10px;'>ATD:</b>$tab LIGAÇÕES ATENDIDAS</li>
						<li><b style='padding-right: 10px;'>ABD:</b>$tab LIGAÇÕES ABANDONADAS</li>
						<li><b style='padding-right: 10px;'>TMO:</b>$tab TEMPO MÉDIO DE OPERAÇÃO</li>
						<li><b style='padding-right: 15px;'>NS 60:</b>$tab2% DE LIGAÇÕES ATENDIDAS ATÉ 60''</li>
						<li><b style='padding-right: 10px;'>(%) ABD:</b> LIGAÇÕES ABANDONADAS MAIOR QUE 5 SEGUNDOS</li>
					</ul>
				</div>
		<!-- </div>    ### DIV DO MAL ### -->
		";
	
	//Consulta da tbl_1
	
		$sql = 'SELECT * FROM proc_boletim_tbl1('.$intervalo.','.date("Y").')';
		$rs=odbc_exec($conn,$sql);
		$indicadores = array('fila','rec','ns60','abd_ac','abd','tmax');
		$cores = array('#b3c8e2','#d8e3f0','','','');
		$esp = array('0%','2%','5%','7%','9%');
		
	//Ranking de abandono
		if(odbc_num_rows($rs) != 0){
			raining_blood("Telefone - Abandono maior que aceitável - $hoje - Atualizado até: $at_tempo",$indicadores,$cores,$esp,$rs);
		}
		else{
						echo "<table class='fancyTable' id='myTable02' name='myTable02' ><thead><tr><td style='font-size: 16px;
																									font-weight: bold;
																									color: #215867;'>
					Telefone - Abandono maior que aceitável - $hoje - Atualizado até: $at_tempo
					</td></tr></thead></table>";
					echo "<p style='color: #215867;'>Não existem filas com abandono maior que o aceitável.</p>";
		}
	//Consulta tbl_2
	
		$sql = "SELECT * FROM tbl_boletim_2";
		$rs=odbc_exec($conn,$sql);
		$indicadores = array('nivel','fila','rec','atd','abd','tmo','ns60','abd_perc');
		
	//Quadro resumo
	
		tbl_2("Quadro Resumo - Acumulado do dia $hoje  - Atualizado até: $at_tempo",$indicadores,$cores,$esp,$rs);

	//Consulta tbl_rga
	
		$sql = "SELECT * FROM proc_boletim_rga4(CURRENT_DATE,$intervalo)";
		$rs=odbc_exec($conn,$sql);
		$indicadores = array('fila','semana1','semana2','semana3','semana4');
		
	//Quadro resumo
	
		tbl_rga("Quadro Semanal - RGA",$indicadores,$cores,$esp,$rs);

	//Consulta tbl_3
	
		$sql = "SELECT * FROM tbl_boletim_3";
		$rs=odbc_exec($conn,$sql);
		$indicadores = array('nivel','diretoria','rec','atd','abd','tmo','ns60','abd_perc');
		
		$cores = array('#b3c8e2','#b3c8e2','#d8e3f0','','#ffffff');
		$fonte = array('','','','font-weight: bold;','');
		$esp = array('0%','0%','0%','2%','4%');
		
	//Quadro detalhado
	
		tbl_3("Quadro detalhado - Acumulado do dia $hoje  - Atualizado até: $at_tempo",$indicadores,$cores,$esp,$rs,$fonte);

	if(isset($conn)){
		odbc_close($conn);
	};
?>


		</div><!-- / fim boletim -->
	</div><!-- / fim inner -->
</div><!-- / fim content-bottom -->