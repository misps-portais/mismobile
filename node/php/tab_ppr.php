﻿<?php

$pagina = $_GET['pagina'];

switch ($pagina){
	case 'graf':
		echo "<table><tr><td>Em construção!</td></tr></table>";
		break;
	case 'regras':
			$usuario = $_GET['usuario'];
			$indicadores = array(array("Faltas","Qualidade", "Abandono","Abandono CHAT"),
						 array("Menor","Maior","Menor","Menor"),
						 array("Maior","Menor","Maior","Maior"));
			
			$conn=odbc_connect('MISPG','','');	
			$sql = "
				SELECT indicador, CAST((round(CAST (100*(peso) AS numeric),2)) as character varying)||'%' as peso, CASE WHEN indicador = 2 THEN CAST(round(CAST (((1-faixa[1])*referencia) AS numeric),2) as character varying) ELSE CAST((round(CAST (100*((1-faixa[1])*referencia) AS numeric),2)) as character varying)||'%' END as melhor , 
				CASE WHEN indicador = 2 THEN CAST(round(CAST (((1-faixa[2])*referencia) AS numeric),2) as character varying) ELSE CAST((round(CAST (100*((1-faixa[2])*referencia) AS numeric),2)) as character varying)||'%' END as pior
				FROM ppr_2013.tbl_regras
				WHERE ccusto = (SELECT centro_custo
					FROM tbl_ccm7_hierarquia
					WHERE cod_re_rh = '$usuario') and indicador <= 3 ORDER BY indicador;
			";
			$rs=odbc_exec($conn,$sql);
			
			$i = 0;
			echo "<p>";
			echo "<table>";
			echo "<tr>";
			//Metricas dos indicadores
			echo "<td>";
			while(odbc_fetch_row($rs)){
					$texto = $indicadores[0][$i];
					$peso = odbc_result($rs,"peso");
					$sinalm = $indicadores[1][$i];
					$sinalp = $indicadores[2][$i];
					$melhor = odbc_result($rs,"melhor");
					$pior = odbc_result($rs,"pior");
					
					echo "<b style='font:14px'>$texto </b>(Peso de $peso)";
					echo "<p style='margin-left:1em'>Resultado $sinalm ou igual que $melhor, seu ganho será de 100%</p>";
					echo "<p style='margin-left:1em'>Resultado $sinalp $melhor e $sinalm ou igual $pior, seu ganho será de 80%</p>";
					echo "<p style='margin-left:1em'>Resultado $sinalp que $pior, seu ganho será de 0%</p>";
					
					$i++;
				}
			echo "</td>";
			
			
			$sql = "SELECT tcc.premio_max as peso
					FROM ppr_2013.tbl_ccusto tcc
					WHERE tcc.ccusto = (SELECT th.centro_custo
								FROM tbl_ccm7_hierarquia th
								WHERE th.cod_re_rh = '$usuario')";
			
			$rs=odbc_exec($conn,$sql);
			
			$peso = odbc_result($rs,"peso");
			
			//Metrica do lucro liquido
			echo "<td valign='top'>";
			echo "<b style='font:14px'>Lucro líquido</b>(Peso de 20.00%)";
			echo "<p style='margin-left:1em'>Resultado de lucro líquido será apurado no fechamento do ano de 2013 pela Controladoria</p>";
			
			//Metrica do lucro liquido
			echo "<b style='font:14px'>Composição da PPR 2013:</b>";
				echo "<table align='center'>";
					echo "<thead>";
						echo "<tr>";
							echo "<th style='text-align:center'>Faltas</th>";
							echo "<th></th>";
							echo "<th style='text-align:center'>Qualidade</th>";
							echo "<th></th>";
							echo "<th style='text-align:center'>Abandono</th>";
							echo "<th></th>";						
							echo "<th style='text-align:center'>Lucro Liquido</th>";
							echo "<th></th>";
							echo "<th style='text-align:center'>Total</th>";
						echo "</tr>";
					echo "</thead>";
					echo "<tr>";
						echo "<td align='center'>35%</td>";
						echo "<td align='center'>+</td>";
						echo "<td align='center'>35%</td>";
						echo "<td align='center'>+</td>";
						echo "<td align='center'>10%</td>";
						echo "<td align='center'>+</td>";				
						echo "<td align='center'>20%</td>";
						echo "<td align='center'>=</td>";
						echo "<td align='center'>100% de $peso salários </td>";
					echo "</tr>";
				echo "</table>";
			echo "</td>";
			echo "</tr>";
			echo "</table>";
		break;
	case 'orientacoes':
		echo "<table><tr><td>Em construção!</td></tr></table>";
		break;
	case 'regras_ger':
	
			$usuario = $_GET['usuario'];
			$indicadores = array(array("Faltas","Qualidade", "Abandono","Abandono CHAT"),
						 array("Menor","Maior","Menor","Menor"),
						 array("Maior","Menor","Maior","Maior"));
						 
			$conn=odbc_connect('MISPG','','');
			
			$sql = "SELECT tc.ccusto, tc.grupo
					FROM ppr_2013.tbl_gerente tg
					INNER JOIN ppr_2013.tbl_ccusto tc
						ON tg.gerente = tc.gerente
					WHERE tg.matricula IN (
								SELECT gerente_matricula 
								FROM ppr_2013.tbl_gerente_coor as tbl_coor 
								WHERE tbl_coor.matricula = '$usuario'
								)
					ORDER BY tc.grupo";
			
			$rs=odbc_exec($conn,$sql);

			echo "<select id='ccusto' onchange='regras_gerencia(this.options[selectedIndex].value)'>";
				echo "<option value='%'>Selecione um centro de custo</option>";
					while(odbc_fetch_row($rs)){
						$ccusto = odbc_result($rs,"ccusto");
						$grupo = odbc_result($rs,"grupo");
						echo "<option value='$ccusto'>$grupo</option>";
					}
			echo "</select>";
			
			echo "<div style='width: 100%;
					height: 10px;
					border-bottom: 1px rgb(184, 184, 184);
					border-bottom-style: solid;
					border-bottom-width: thin;'></div>";
					
			echo "<div id='regras'></div>";
	break;
	case 'regras_ger_cons':
	
			$ccusto = $_GET['ccusto'];
			$indicadores = array(array("Faltas","Qualidade", "Abandono","Abandono CHAT"),
						 array("Menor","Maior","Menor","Menor"),
						 array("Maior","Menor","Maior","Maior"));
			
			$conn=odbc_connect('MISPG','','');	
			$sql = "
				SELECT indicador, CAST((round(CAST (100*(peso) AS numeric),2)) as character varying)||'%' as peso, CASE WHEN indicador = 2 THEN CAST(round(CAST (((1-faixa[1])*referencia) AS numeric),2) as character varying) ELSE CAST((round(CAST (100*((1-faixa[1])*referencia) AS numeric),2)) as character varying)||'%' END as melhor , 
				CASE WHEN indicador = 2 THEN CAST(round(CAST (((1-faixa[2])*referencia) AS numeric),2) as character varying) ELSE CAST((round(CAST (100*((1-faixa[2])*referencia) AS numeric),2)) as character varying)||'%' END as pior
				FROM ppr_2013.tbl_regras
				WHERE ccusto = '$ccusto' and indicador <= 3 ORDER BY indicador;
			";
			$rs=odbc_exec($conn,$sql);
			
			$i = 0;
			echo "<p>";
			echo "<table>";
			echo "<tr>";
			//Metricas dos indicadores
			echo "<td>";
			while(odbc_fetch_row($rs)){
					$texto = $indicadores[0][$i];
					$peso = odbc_result($rs,"peso");
					$sinalm = $indicadores[1][$i];
					$sinalp = $indicadores[2][$i];
					$melhor = odbc_result($rs,"melhor");
					$pior = odbc_result($rs,"pior");
					
					echo "<b style='font:14px'>$texto </b>(Peso de $peso)";
					echo "<p style='margin-left:1em'>Resultado $sinalm ou igual que $melhor, seu ganho será de 100%</p>";
					echo "<p style='margin-left:1em'>Resultado $sinalp $melhor e $sinalm ou igual $pior, seu ganho será de 80%</p>";
					echo "<p style='margin-left:1em'>Resultado $sinalp que $pior, seu ganho será de 0%</p>";
					
					$i++;
				}
			echo "</td>";
			
			
			$sql = "SELECT tcc.premio_max as peso
					FROM ppr_2013.tbl_ccusto tcc
					WHERE tcc.ccusto = '$ccusto'";
			
			$rs=odbc_exec($conn,$sql);
			
			$peso = odbc_result($rs,"peso");
			
			//Metrica do lucro liquido
			echo "<td valign='top'>";
			echo "<b style='font:14px'>Lucro líquido</b>(Peso de 20.00%)";
			echo "<p style='margin-left:1em'>Resultado de lucro líquido será apurado no fechamento do ano de 2013 pela Controladoria</p>";
			
			//Metrica do lucro liquido
			echo "<b style='font:14px'>Composição da PPR 2013:</b>";
				echo "<table align='center'>";
					echo "<thead>";
						echo "<tr>";
							echo "<th style='text-align:center'>Faltas</th>";
							echo "<th></th>";
							echo "<th style='text-align:center'>Qualidade</th>";
							echo "<th></th>";
							echo "<th style='text-align:center'>Abandono</th>";
							echo "<th></th>";						
							echo "<th style='text-align:center'>Lucro Liquido</th>";
							echo "<th></th>";
							echo "<th style='text-align:center'>Total</th>";
						echo "</tr>";
					echo "</thead>";
					echo "<tr>";
						echo "<td align='center'>35%</td>";
						echo "<td align='center'>+</td>";
						echo "<td align='center'>35%</td>";
						echo "<td align='center'>+</td>";
						echo "<td align='center'>10%</td>";
						echo "<td align='center'>+</td>";				
						echo "<td align='center'>20%</td>";
						echo "<td align='center'>=</td>";
						echo "<td align='center'>100% de $peso salários </td>";
					echo "</tr>";
				echo "</table>";
			echo "</td>";
			echo "</tr>";
			echo "</table>";
		break;
	break;
}
if(isset($conn)){
odbc_close($conn);};
?>