<?php

//
define('ENVIRONMENT', 'develop');
define('APP_PATH', 'C:\xampp\htdocs\mismobile');
define('REL_PATH', APP_PATH.'\node\php\relatorios');
ini_set('date.timezone','America/Sao_Paulo');

//
//require(REL_PATH.'\functions.php');
(empty($_GET['acao'])) ? $rota = 404 : $rota = $_GET['acao'];


//
$conn = odbc_connect('MISPG','','');


//
if(ENVIRONMENT == 'develop'){
    error_reporting(E_ALL);
    session_start();
    header('Content-Type: text/html; charset=utf-8');
    header("Last-Modified: ".date("D, d M Y H:i:s")." GMT");
}

if(ENVIRONMENT == 'test'){
    error_reporting(0);
    global $user;

}

if(ENVIRONMENT == 'production'){
    error_reporting(0);
    global $user;
}

/**
 *    Rotas para as páginas do sistema
 */
switch($rota){
    case 'todos':
        require(REL_PATH.'\todos.php');
        break;
    case 'boletim_torre':
        require (REL_PATH.'\boletim_torre.php');
        break;
    case 'especificos':
        require(REL_PATH.'\especificos.php');
        break;
	case 'performance':
        require (REL_PATH.'\performance.php');
        break;
	case 'produtividade':
        require (REL_PATH.'\produtividade.php');
        break;
    case 'produtividade':
        require (REL_PATH.'\produtividade.php');
        break;
    case 'sucursais':
        require (REL_PATH.'\sucursais.php');
        break;
    case 'volumetria':
        require (REL_PATH.'\volumetria.php');
        break;

	default:
        header("HTTP/1.0 404 Not Found");
        echo 'Erro 404: Página não encontrada';
        break;
}

/**
 *   Acaba com o banco!  #ohnooo
 */
odbc_close($conn);

?>