﻿<link href="node/css/myTheme.css" rel="stylesheet" media="screen">
<link rel="stylesheet" href="node/css/comunicacao.css" />

<style type="text/css">

.fancyTable td, .fancyTable th {
	/* appearance */
	border: 1px solid #e7eaed;
	
	/* size */
	padding: 3px;
	}

</style>

<?php
	$acao = $_GET['acao'];
	
	//Funcao que efetua a seguinte conversao MIS -> Mis
	function LUCase($string){
		return(ucwords(strtolower($string)));
	};
	
	switch ($acao){
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		case 'entrada':
			$usuario = $_GET['usuario'];
			$offset = $_GET['offset'];
			
			$conn=odbc_connect('MISPG','','');
			
			$sql = "
					SELECT COUNT(id) as contagem
						FROM comunicacao.tbl_conversa
						INNER JOIN 
							(SELECT DISTINCT destinatario,remetente,conversa 	
							FROM comunicacao.tbl_msg
							WHERE destinatario = '$usuario') as query1
						ON id = query1.conversa
						WHERE id = ANY((SELECT DISTINCT conversa
										FROM comunicacao.tbl_msg
										WHERE destinatario = '$usuario'
								)
								  ) and excluir_d is null
			";
			
			$rs=odbc_exec($conn,$sql);
			
			$nmsg = odbc_result($rs,'contagem');
			
			$entrada = chr(34). "entrada" . chr(34);
			$avanca = $offset + 15;
			$volta = $offset - 15;
			
			if($avanca < $nmsg){
				
				$esquerda = 'node/img/seta_d1.png';
				$funcaoe = "onclick='consulta($usuario,$entrada,$avanca);'";
			}
			else{
				$esquerda = 'node/img/seta_d0.png';
				$funcaoe = "";
			}
			if($volta < 0){
				$direita = 'node/img/seta_e0.png';
				$funcaod = "";
			}
			else{
				$direita = 'node/img/seta_e1.png';
				$funcaod = "onclick='consulta($usuario,$entrada,$volta);'";
			}
					
			$sql = "SELECT id,CASE WHEN 
								(SELECT DISTINCT nome_ronda 
								FROM tbl_ccm7_hierarquia 
								WHERE cod_re_rh = query1.remetente) IS NOT NULL
							THEN 
								(SELECT grupo FROM tbl_ccm7_hierarquia 
								INNER JOIN ppr_2013.tbl_ccusto as custo
								ON custo.ccusto = tbl_ccm7_hierarquia.centro_custo
								WHERE cod_re_rh = query1.remetente)
							ELSE 
								(CASE WHEN 
									(SELECT gerente 
									FROM ppr_2013.tbl_gerente 
									WHERE matricula = query1.remetente) IS NOT NULL
								THEN 
									'Gerentes'
								ELSE query1.remetente END) 
							END as ccusto,
							CASE WHEN 
								(SELECT DISTINCT nome_ronda 
								FROM tbl_ccm7_hierarquia 
								WHERE cod_re_rh = query1.remetente) IS NOT NULL
							THEN 
								(SELECT DISTINCT nome_ronda 
								FROM tbl_ccm7_hierarquia 
								WHERE cod_re_rh = query1.remetente)
							ELSE 
								(CASE WHEN 
									(SELECT gerente 
									FROM ppr_2013.tbl_gerente 
									WHERE matricula = query1.remetente) IS NOT NULL
								THEN 
									(SELECT gerente 
									FROM ppr_2013.tbl_gerente 
									WHERE matricula = query1.remetente)
								ELSE query1.remetente END) 
							END as remetente,
							CASE WHEN 
								(SELECT DISTINCT nome_ronda 
								FROM tbl_ccm7_hierarquia 
								WHERE cod_re_rh = query1.destinatario) IS NOT NULL
							THEN 
								(SELECT DISTINCT nome_ronda 
								FROM tbl_ccm7_hierarquia 
								WHERE cod_re_rh = query1.destinatario)
							ELSE 
								(CASE WHEN 
									(SELECT gerente 
									FROM ppr_2013.tbl_gerente 
									WHERE matricula = query1.destinatario) IS NOT NULL
								THEN 
									(SELECT gerente 
									FROM ppr_2013.tbl_gerente 
									WHERE matricula = query1.destinatario)
								ELSE query1.destinatario END) 
							END as destinatario, 
							data_envio, 
							data_ultima_atualizacao, 
							assunto, 
							subassunto, 
							titulo, 
							query1.destinatario as matrdest,
							query1.remetente as matrrem
						FROM comunicacao.tbl_conversa
						INNER JOIN 
							(SELECT DISTINCT destinatario,remetente,conversa 	
							FROM comunicacao.tbl_msg
							WHERE destinatario = '$usuario') as query1
						ON id = query1.conversa
						WHERE id = ANY((SELECT DISTINCT conversa
										FROM comunicacao.tbl_msg
										WHERE destinatario = '$usuario'
								)
								  ) and excluir_d is null
						ORDER BY data_ultima_atualizacao DESC
						LIMIT 15
						OFFSET $offset
			";
			
			$rs=odbc_exec($conn,$sql);
			
			$sup = $offset + odbc_num_rows($rs);
			if($sup <= 0){
				$offset = 0;
				$sup = 0;
			}
			else{
				$offset++;
			}
			echo "<table>
				<tbody>
					<tr>
						<th style='text-align:right;'>
							<b>$offset-$sup</b> de <b>$nmsg</b>
						</th>
						<th style='width: 30px;'>
							<img src='$direita' style='width: 25px;height: 25px;' $funcaod>
						</th>		
						<th style='width: 30px;'>
							<img src='$esquerda' style='width: 25px;height: 25px;' $funcaoe>
						</th>	
					</tr>
				</tbody>
			</table>";
			
			$leitura = chr(34). "leitura" . chr(34);
			echo "<center><div style='max-height:600px;overflow-y:auto;'><table class='fancyTable' id='myTable02' name='myTable02' style='font-size:10px;'>";

			echo "<thead><tr><td width='300' ><b>Quem</b>
						 </td><td width='300' ><b>Matrícula</b></td>
						 </td><td width='300'  ><b>Centro de custo</b></td>
			             </td><td width='300' ><b>Título</b></td>
						 <td width='300' ><b>Data</b></td></tr></thead>";
			
			while(odbc_fetch_row($rs)){
					echo "<tr>";
							
						$indicador = array("remetente","matrrem","ccusto","titulo","data_ultima_atualizacao");
						
						$id_conv = odbc_result($rs,"id");
						if(odbc_result($rs,'matrrem') == "$usuario"){
							$ind = 0;
						}
						else{
							$ind = 1;
						}
						
						$usuario = chr(34) . $_GET['usuario'] . chr(34);
						
					
						for($i = 0 ; $i < sizeof($indicador); $i++){
								if ($indicador[$i] == "data_ultima_atualizacao"){
								$resultado = date("d/m/Y H:i:s", strtotime(odbc_result($rs,"$indicador[$i]")));
								}
								else{
								$resultado = utf8_encode(LUCase(odbc_result($rs,"$indicador[$i]")));
								}
								echo "<td onclick='ler($usuario,$leitura,$id_conv,$ind);' style='text-align:center;cursor:pointer;'>$resultado</td>";
						}
					
					echo "</tr>";
			}
			
			echo "</table></div></center>";
		break;
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		case 'saida':
		
			$usuario =  $_GET['usuario'];
			$offset = $_GET['offset'];
			
			$conn= odbc_connect('MISPG','','');
			
			$sql = "
				SELECT Count(id) as contagem
						FROM comunicacao.tbl_conversa
						INNER JOIN 
							(SELECT DISTINCT destinatario,remetente,conversa 	
							FROM comunicacao.tbl_msg
							WHERE remetente = '$usuario') as query1
						ON id = query1.conversa
						WHERE id = ANY((SELECT DISTINCT conversa
										FROM comunicacao.tbl_msg
										WHERE remetente = '$usuario'
								)
								  ) and excluir_r is null
			";
			
			$rs=odbc_exec($conn,$sql);
			
			$nmsg = odbc_result($rs,'contagem');
			
			$saida = chr(34). "saida" . chr(34);
			$avanca = $offset + 15;
			$volta = $offset - 15;
			
			if($avanca < $nmsg){
				
				$esquerda = 'node/img/seta_d1.png';
				$funcaoe = "onclick='consulta($usuario,$saida,$avanca);'";
			}
			else{
				$esquerda = 'node/img/seta_d0.png';
				$funcaoe = "";
			}
			if($volta < 0){
				$direita = 'node/img/seta_e0.png';
				$funcaod = "";
			}
			else{
				$direita = 'node/img/seta_e1.png';
				$funcaod = "onclick='consulta($usuario,$saida,$volta);'";
			}
										
			$sql = "SELECT id,CASE WHEN 
								(SELECT DISTINCT nome_ronda 
								FROM tbl_ccm7_hierarquia 
								WHERE cod_re_rh = query1.destinatario) IS NOT NULL
							THEN 
								(SELECT grupo FROM tbl_ccm7_hierarquia 
								INNER JOIN ppr_2013.tbl_ccusto as custo
								ON custo.ccusto = tbl_ccm7_hierarquia.centro_custo
								WHERE cod_re_rh = query1.destinatario)
							ELSE 
								(CASE WHEN 
									(SELECT gerente 
									FROM ppr_2013.tbl_gerente 
									WHERE matricula = query1.destinatario) IS NOT NULL
								THEN 
									'Gerentes'
								ELSE query1.destinatario END) 
							END as ccusto,
							CASE WHEN 
								(SELECT DISTINCT nome_ronda 
								FROM tbl_ccm7_hierarquia 
								WHERE cod_re_rh = query1.remetente) IS NOT NULL
							THEN 
								(SELECT DISTINCT nome_ronda 
								FROM tbl_ccm7_hierarquia 
								WHERE cod_re_rh = query1.remetente)
							ELSE 
								(CASE WHEN 
									(SELECT gerente 
									FROM ppr_2013.tbl_gerente 
									WHERE matricula = query1.remetente) IS NOT NULL
								THEN 
									(SELECT gerente 
									FROM ppr_2013.tbl_gerente 
									WHERE matricula = query1.remetente)
								ELSE query1.remetente END) 
							END as remetente,
							CASE WHEN 
								(SELECT DISTINCT nome_ronda 
								FROM tbl_ccm7_hierarquia 
								WHERE cod_re_rh = query1.destinatario) IS NOT NULL
							THEN 
								(SELECT DISTINCT nome_ronda 
								FROM tbl_ccm7_hierarquia 
								WHERE cod_re_rh = query1.destinatario)
							ELSE 
								(CASE WHEN 
									(SELECT gerente 
									FROM ppr_2013.tbl_gerente 
									WHERE matricula = query1.destinatario) IS NOT NULL
								THEN 
									(SELECT gerente 
									FROM ppr_2013.tbl_gerente 
									WHERE matricula = query1.destinatario)
								ELSE query1.destinatario END) 
							END as destinatario, 
							data_envio, 
							data_ultima_atualizacao, 
							assunto, 
							subassunto, 
							titulo, 
							query1.destinatario as matrdest,
							query1.remetente as matrrem
						FROM comunicacao.tbl_conversa
						INNER JOIN 
							(SELECT DISTINCT destinatario,remetente,conversa 	
							FROM comunicacao.tbl_msg
							WHERE remetente = '$usuario') as query1
						ON id = query1.conversa
						WHERE id = ANY((SELECT DISTINCT conversa
										FROM comunicacao.tbl_msg
										WHERE remetente = '$usuario'
								)
								  ) and excluir_d is null
						ORDER BY data_ultima_atualizacao DESC
						LIMIT 15
						OFFSET $offset
			";
			

			$rs=odbc_exec($conn,$sql);
			
			$sup = $offset + odbc_num_rows($rs);
			if($sup <= 0){
				$offset = 0;
				$sup = 0;
			}
			else{
				$offset++;
			}

			echo "<table>
				<tbody>
					<tr>
						<th style='text-align:right;'>
							<b>$offset-$sup</b> de <b>$nmsg</b>
						</th>
						<th style='width: 30px;'>
							<img src='$direita' style='width: 25px;height: 25px;' $funcaod>
						</th>		
						<th style='width: 30px;'>
							<img src='$esquerda' style='width: 25px;height: 25px;' $funcaoe>
						</th>	
					</tr>
				</tbody>
			</table>";
				
			$leitura = chr(34). "leitura" . chr(34);
			echo "<center><div style='max-height:600px;overflow-y:auto;'><table class='fancyTable' id='myTable02' name='myTable02' style='font-size:10px;'>";
			
			echo "<thead><tr><td width='300'  ><b>Destinatário</b>
						</td><td width='300'  ><b>Matrícula</b></td>
						</td><td width='300'  ><b>Centro de custo</b></td>
						 </td><td width='300'  ><b>Título</b></td>
						 <td width='300' ><b>Data</b></td></tr></thead>";
			
			while(odbc_fetch_row($rs)){
					echo "<tr>";
							
						$indicador = array("destinatario","matrdest","ccusto","titulo","data_ultima_atualizacao");
						
						$id_conv = odbc_result($rs,"id");

						if(odbc_result($rs,'matrrem') == $usuario){
							$ind = 0;
						}
						else{
							$ind = 1;
						}
						
						$usuario = chr(34) . $_GET['usuario'] . chr(34); 
						for($i = 0 ; $i < sizeof($indicador); $i++){
								if ($indicador[$i] == "data_envio"){
								$resultado = date("d/m/Y H:i:s", strtotime(odbc_result($rs,"$indicador[$i]")));
								}
								else{
								$resultado = utf8_encode(LUCase(odbc_result($rs,"$indicador[$i]")));
								}
								echo "<td onclick='ler($usuario,$leitura,$id_conv,$ind);' style='text-align:center;cursor:pointer;'>$resultado</td>";
						}
					
					echo "</tr>";
				
			}
			echo "</table></div></center>";
		break;
		case 'nova':
			$usuario = $_GET['usuario'];
			$conn=odbc_connect('MISPG','','');

			$sql = "SELECT query1.matricula as matr_rem, 
						query1.gerente as remetente, 
						cod_re_rh as matr_dest, 
						nome_ronda as destinatario
					FROM (
						SELECT gerentes.gerente, matricula, ccustos.ccusto FROM ppr_2013.tbl_gerente as gerentes
						INNER JOIN ppr_2013.tbl_ccusto as ccustos
							ON ccustos.gerente = gerentes.gerente 
						WHERE gerentes.matricula = '$usuario'
					) as query1
					INNER JOIN tbl_ccm7_hierarquia as hierarquia
						ON hierarquia.centro_custo = query1.ccusto
					ORDER BY nome_ronda";
				
			$rs=odbc_exec($conn,$sql);
			
			if(odbc_result($rs,"matr_rem") != ''){
			
				
				$remetente= LUCase(odbc_result($rs,"remetente"));
				$matrrem = odbc_result($rs,"matr_rem");
				$destinatario= LUCase(utf8_encode(odbc_result($rs,"destinatario")));
				$matrdest = odbc_result($rs,"matr_dest");
				
				$campo_destinatario = "<label for='destinatario'>Destinatário</label><select id='destinatario' style='width:300px;'>";
				//$campo_destinatario = $campo_destinatario . "<option value='MIS'>Enviar mensagem para o MIS</option>";
				$campo_destinatario = $campo_destinatario . "<option value='todos'>Enviar mensagem para todos operadores</option>";
				while(odbc_fetch_row($rs)){
					$campo_destinatario = $campo_destinatario . "<option value='$matrdest'>$destinatario</option>";
					$destinatario= LUCase(utf8_encode(odbc_result($rs,"destinatario")));
					$matrdest = odbc_result($rs,"matr_dest");
				}
				
				$campo_destinatario = $campo_destinatario . "</select><br/>";
				$dest =  'document.getElementById("destinatario").options[document.getElementById("destinatario").selectedIndex].value';
			}
			else{
				$sql = "SELECT (SELECT DISTINCT nome
								FROM tbl_ronda_agentes_dia_2013
								WHERE matricula = '$usuario') AS remetente, 
						'$usuario' as matr_rem,
						gerente as destinatario,
								(SELECT matricula 
								 FROM ppr_2013.tbl_gerente as tbl_gerente 
								 WHERE tbl_gerente.gerente = ppr_2013.tbl_ccusto.gerente) 
						as matr_dest
						FROM ppr_2013.tbl_ccusto
						WHERE ccusto LIKE ANY (SELECT DISTINCT ccusto 
							FROM tbl_ronda_agentes_dia_2013
							WHERE matricula = '$usuario');";
				$rs=odbc_exec($conn,$sql);
				
				$destinatario= LUCase(odbc_result($rs,"destinatario"));
				$remetente= LUCase(odbc_result($rs,"remetente"));
				$matrdest = odbc_result($rs,"matr_dest");
				$matrrem = odbc_result($rs,"matr_rem");
				
				$campo_destinatario = "<label for='dest'>Destinatário</label><input id='dest' value='$destinatario' disabled='true' style='width:300px;'/><br/><a id='destinatario' style='display:none;'>$matrdest</a>";
				$dest =  'document.getElementById("destinatario").innerHTML';
			}
			
			echo "<center><div id='mensagem' style='width:600px;text-align:left;'>";
			##Nome
				echo "<label for='nome'>Remetente</label><input id='nome' value='$remetente' disabled='true' style='width:300px;'/><br/><a id ='remetente' style='display:none;'>$matrrem</a>"; 
			##Destinatário
				echo "$campo_destinatario"; 
			##Assunto
				echo "<label for='assunto' style='display:none'>Assunto</label><select id='assunto' style='width:300px;display:none'></select>"; 
			##SubAssunto
				echo "<label for='sub' style='display:none'>Sub-assunto</label><select id='sub' style='width:300px;display:none'></select>";
			##Titulo
				echo "<label for='titulo'>Título</label><input maxlength='42' id='titulo' type='text' class='button_text' style='width:300px;'/><br/>"; 
			##Mensagem
				echo "<label for='msg'>Mensagem</label><textarea maxlength='500' type='text' id='msg' style='width:600px;height:200px;'></textarea></br>";
			echo "</div>";
		
				$envio = chr(34). "envio" . chr(34);
				$titulo = 'document.getElementById("tn").value';
				$nome =  'document.getElementById("remetente").innerHTML';
				$titulo =  'document.getElementById("titulo").value';
				$mensagem =  'document.getElementById("msg").value';
				$assunto =  chr(34). "assunto" . chr(34);
				$sub =  chr(34). "sub" . chr(34);
				
			echo "<input id='envio'  class='button_text' type='submit' name='envio' value='Enviar' 
					onclick='enviar($nome, $dest, $assunto, $sub, $titulo, $mensagem,$envio);' style='cursor: pointer;font-size: 14px;'/>
				  <input id='remover' class='button_text' type='submit' name='limpar' value='Limpar'/>";
			echo "</center>";
		break;
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		case 'envio':
		
			$conn=odbc_connect('MISPG','','');
			
			$remetente = $_GET['usuario'];
			$destinatario = $_GET['dest'];
			$assunto = $_GET['assunto'];
			$sub = $_GET['sub'];
			$titulo = utf8_decode($_GET['titulo']);
			$mensagem = utf8_decode($_GET['mensagem']);

			$hora = date('Y-m-d H:i:s');
							
			if($destinatario!='todos'){
				$sql = "INSERT INTO comunicacao.tbl_conversa(
									id, 
									remetente, destinatario, 
									data_envio, data_ultima_atualizacao, 
									assunto, subassunto, titulo,leitura)
									VALUES (DEFAULT, 
										'$remetente', '$destinatario', 
										'$hora', '$hora',
										'$assunto', '$sub', '$titulo','0');";
				
				$rs=odbc_exec($conn,$sql);

				
				$sql = "INSERT INTO comunicacao.tbl_msg(
						id, conversa, mensagem, remetente, data_insert,destinatario)
						VALUES (DEFAULT, (SELECT id 
											FROM comunicacao.tbl_conversa 
											WHERE data_envio = '$hora' and titulo = '$titulo'), 
								'$mensagem', '$remetente', '$hora','$destinatario');
						";

				$rs=odbc_exec($conn,$sql);
			}
			else{
				$sql = "
						INSERT INTO comunicacao.tbl_conversa(
									remetente, destinatario, data_envio, data_ultima_atualizacao, 
									assunto, subassunto, titulo, leitura)
						SELECT 
							'$remetente', 
							hierarquia.cod_re_rh, 
							'$hora','$hora', 
							'$assunto' as assunto, 
							'$sub' as subassunto,
							'$titulo' as titulo,
							'false' as leitura
											FROM  (
												SELECT gerentes.gerente, matricula, ccustos.ccusto FROM ppr_2013.tbl_gerente as gerentes
												INNER JOIN ppr_2013.tbl_ccusto as ccustos
													ON ccustos.gerente = gerentes.gerente 
												WHERE gerentes.matricula = '$remetente'
											) as query1
											INNER JOIN tbl_ccm7_hierarquia as hierarquia
												ON hierarquia.centro_custo = query1.ccusto
				";
										
				$rs=odbc_exec($conn,$sql);
				
				$sql = "
						INSERT INTO comunicacao.tbl_msg(
							conversa, mensagem, remetente, data_insert, destinatario, 
							leitura)
						SELECT 
							(SELECT id FROM comunicacao.tbl_conversa
							WHERE remetente = '$remetente' 
								AND destinatario = hierarquia.cod_re_rh 
								AND data_envio = '$hora'), 
							'$mensagem',
							'$remetente',
							'$hora',
							hierarquia.cod_re_rh, 
							'false' as leitura
											FROM  (
												SELECT gerentes.gerente, matricula, ccustos.ccusto FROM ppr_2013.tbl_gerente as gerentes
												INNER JOIN ppr_2013.tbl_ccusto as ccustos
													ON ccustos.gerente = gerentes.gerente 
												WHERE gerentes.matricula = '$remetente'
											) as query1
											INNER JOIN tbl_ccm7_hierarquia as hierarquia
												ON hierarquia.centro_custo = query1.ccusto
				";
				
				$rs=odbc_exec($conn,$sql);
						
			}
			
			
			echo "Mensagem enviada com sucesso!";
		break;
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		case 'leitura':
			$usuario = $_GET['usuario'];
			$id = $_GET['id'];
			$tipo = $_GET['tipo'];
			
			$responder = chr(34). "responder" . chr(34);
			$temp = chr(34). "$usuario" . chr(34);
				echo "<table>";
					echo "<td onclick='ler($temp,$responder,$id)' style='cursor:pointer;'>Responder</td>";
			
			$conn=odbc_connect('MISPG','','');
			
			$sql = "SELECT id
					FROM ppr_2013.tbl_gerente
					WHERE matricula = '$usuario'";
					
			$rs=odbc_exec($conn,$sql);
			
					// if(odbc_result($rs,"id") != ""){
						// echo "<td style='cursor:pointer;'>Encaminhar para o MIS</td>";
					// }
					
					//echo "<td style='cursor:pointer;' onclick='excluir($id,$tipo);'>Excluir</td>";
				echo "</table>";
				
				
				
				$sql = "SELECT query1.titulo as titulo, CASE WHEN 
									(SELECT DISTINCT nome_ronda 
									FROM tbl_ccm7_hierarquia 
									WHERE cod_re_rh = comunicacao.tbl_msg.remetente) IS NOT NULL
								THEN 
									(SELECT DISTINCT nome_ronda 
									FROM tbl_ccm7_hierarquia 
									WHERE cod_re_rh = comunicacao.tbl_msg.remetente)
								ELSE 
									(CASE WHEN 
										(SELECT gerente 
										FROM ppr_2013.tbl_gerente 
										WHERE matricula = comunicacao.tbl_msg.remetente) IS NOT NULL
									THEN 
										(SELECT gerente 
										FROM ppr_2013.tbl_gerente 
										WHERE matricula = comunicacao.tbl_msg.remetente)
									ELSE comunicacao.tbl_msg.remetente END) 
								END as remetente,mensagem, data_insert
						FROM comunicacao.tbl_msg
												INNER JOIN 
													(SELECT DISTINCT titulo, destinatario,remetente,id 	
													FROM comunicacao.tbl_conversa
													) as query1
												ON comunicacao.tbl_msg.conversa = query1.id
						WHERE comunicacao.tbl_msg.conversa = $id
						ORDER BY data_insert DESC
				";
						 
				$rs=odbc_exec($conn,$sql);
				
				$titulo = odbc_result($rs,"titulo");
				
				echo "<h2>$titulo</h2>";
				
				echo "<table>";
					$data = date("d/m/Y H:i:s", strtotime(odbc_result($rs,"data_insert")));
					$mensagem = utf8_encode(odbc_result($rs,"mensagem"));
					$remetente = LUCase(odbc_result($rs,"remetente"));
					
					echo "<tr style='background-color:#eeeeee'>";

					echo "<td  style='text-align:left;'><b>$remetente</b></td>";
					echo "<td  style='text-align:right;'><b>$data</b></td>";
					
					echo "</tr>";
					echo "<tr>";
					echo "<td style='padding-left:50px;'>$mensagem</td>";
					echo "<td id='id_conversa' style='display:none'>$id</td>";
					echo "</tr>";
				while(odbc_fetch_row($rs)){
				
					$data = date("d/m/Y H:i:s", strtotime(odbc_result($rs,"data_insert")));
					$mensagem = utf8_encode(odbc_result($rs,"mensagem"));
					$remetente = LUCase(odbc_result($rs,"remetente"));
					
					echo "<tr style='background-color:#eeeeee'>";

					echo "<td  style='text-align:left;'><b>$remetente</b></td>";
					echo "<td  style='text-align:right;'><b>$data</b></td>";
					
					echo "</tr>";
					echo "<tr>";
					echo "<td style='padding-left:50px;'>$mensagem</td>";
					echo "<td id='id_conversa' style='display:none'>$id</td>";
					echo "</tr>";
				}
				echo "</table>";							
		break;
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		case 'responder':
			$usuario = $_GET['usuario'];
			$id = $_GET['id'];
			$conn=odbc_connect('MISPG','','');

			$sql = "SELECT DISTINCT remetente, destinatario
					FROM comunicacao.tbl_conversa
					WHERE id = $id
					";
						
			$rs=odbc_exec($conn,$sql);
			
			if($usuario == odbc_result($rs,"destinatario")){
				$matr_dest = odbc_result($rs,"remetente");
				$matr_reme = odbc_result($rs,"destinatario");
			
			}
			else{
				$matr_dest = odbc_result($rs,"destinatario");
				$matr_reme = odbc_result($rs,"remetente");			
			}
			
			$sql = "
					SELECT CASE WHEN (SELECT gerente 
								  FROM ppr_2013.tbl_gerente 
								  WHERE matricula = '$matr_dest') IS NOT NULL
								  THEN (SELECT gerente 
								  FROM ppr_2013.tbl_gerente 
								  WHERE matricula = '$matr_dest')
								  ELSE
								  (SELECT DISTINCT nome_ronda 
								  FROM tbl_ccm7_hierarquia 
								  WHERE cod_re_rh = '$matr_dest'
								  )
								  END AS nome_dest,
							CASE WHEN (SELECT gerente 
									  FROM ppr_2013.tbl_gerente 
									  WHERE matricula = '$matr_reme') IS NOT NULL
									  THEN (SELECT gerente 
									  FROM ppr_2013.tbl_gerente 
									  WHERE matricula = '$matr_reme')
									  ELSE
									  (SELECT DISTINCT nome_ronda 
									  FROM tbl_ccm7_hierarquia 
									  WHERE cod_re_rh = '$matr_reme'
									  )
									  END AS nome_reme
			";
			
			$rs=odbc_exec($conn,$sql);
			
			$nome_dest = LUCase(odbc_result($rs,"nome_dest"));
			$nome_reme = LUCase(odbc_result($rs,"nome_reme"));
			
			if($matr_reme == 'MIS'){
				$nome_reme = 'MIS';
			}
			else if($matr_dest == 'MIS'){
				$nome_dest = 'MIS';
			}
			
			echo "<a id='id' style='display:none;'>$id</a>";
			
			echo "<center><div id='mensagem' style='width:600px;text-align:left;'>";
			##Nome
				echo "<label for='nome'>Remetente</label><input id='nome' value='$nome_reme' disabled='true' style='width:300px;'/><br/><a id ='remetente' style='display:none;'>$matr_reme</a>"; 
			##Destinatário
				echo "<label for='dest'>Destinatário</label><input id='dest' value='$nome_dest' disabled='true' style='width:300px;'/><br/><a id='destinatario' style='display:none;'>$matr_dest</a>"; 
			##Mensagem
				echo "<label for='sub'>Mensagem</label><textarea maxlength='500' type='text' id='msg' style='width:600px;height:200px;'></textarea></br>";
			echo "</div>";
						echo "<input id='envio'  class='button_text' type='submit' name='envio' value='Enviar' 
					onclick='resp()' style='cursor: pointer;font-size: 14px;'/>
				  <input id='limpar' class='button_text' type='submit' name='limpar' value='Limpar'/>";
			echo "</center>";
		break;
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		case 'resposta':
			$remetente = $_GET['remetente'];
			$destinatario = $_GET['destinatario'];
			$id = $_GET['id'];
			$mensagem = utf8_decode($_GET['mensagem']);
			
			$hora = date('Y-m-d H:i:s');
					
			$conn=odbc_connect('MISPG','','');
			$sql = "INSERT INTO comunicacao.tbl_msg(
					id, conversa, mensagem, remetente, data_insert,destinatario)
					VALUES (DEFAULT, $id, 
					'$mensagem', '$remetente', '$hora','$destinatario');
			";
			odbc_exec($conn,$sql);

			$sql = "UPDATE comunicacao.tbl_conversa
					SET data_ultima_atualizacao= '$hora'
					WHERE id = $id;";
					
			odbc_exec($conn,$sql);

			echo "Mensagem enviada com sucesso!";
		break;
		// case 'excluir':
			// $tipo = $_GET['tipo'];
			// $id = $_GET['id'];
			
			// if($tipo == 1){
				// $excluir = "excluir_d";
			// }
			// else{
				// $excluir = "excluir_r";
			// }
			
			// $conn=odbc_connect('MISPG','','');
			
			// $sql = "UPDATE comunicacao.tbl_conversa
					// SET $excluir = 'true'
					// WHERE id = $id;";
			
			// odbc_exec($conn,$sql);
			
			// echo "Mensagem excluida com sucesso!";
			
		break;
	}
if(isset($conn)){
odbc_close($conn);
};	
?>